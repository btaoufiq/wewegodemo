<?php

namespace WEWEGO;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
    	'event_id',
    	'opinion',
    	'rate',
    	'rated_user_id',
    	'rating_user_id',
    	'comment'
    ];
}
