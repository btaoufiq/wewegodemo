<?php

namespace WEWEGO\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use WEWEGO\User;
use WEWEGO\Rating;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function welcome()
    {
        $user = Auth::user();
        $events = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('children','events.child','=','children.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id)
                    ->where('events_users.accepted','1');
            })
            ->leftjoin('ratings','events.id', '=', 'ratings.event_id')
            ->where('events.invalide','=','0')
            ->where(function($query) use ($user){
                $query->where('events_users.user_id','=',$user->id)->orWhere('events.owner','=', $user->id);
            })
            ->where('events.dhend', '<=', DB::raw('NOW()'))
            ->where('events.deal','1')
            ->where('ratings.rating_user_id','!=',$user->id)
            ->get(array('events.*','children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex','users.type as user_type', 'users.first_name', 'users.last_name', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));

        $myTeachers = [];
        foreach ($events as $e) {
            if($e->user_type == '1') $myTeachers[$e->id] = User::find($e->owner);
            else {
                $teacher = DB::table('events_users')->where('event_id',$e->id)->where('user_type','1')->where('accepted','1')->first();
                $myTeachers[$e->id] = User::find($teacher->user_id);
            }
        }

        //dd($myTeachers);
        
        if($events->count() > 0 && ($user->type==0 || $user->type==4)) return view('welcome', compact('events','myTeachers'));
        else return redirect('/');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $events = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('children','events.child','=','children.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id)
                    ->whereIn('events_users.accepted',[0,1]);
            })
            ->where('events.invalide','=','0')
                ->where(function($query) use ($user){
                    $query->where('events_users.user_id','=',$user->id)->orWhere('events.owner','=', $user->id);
                })
            ->get(array('events.*','children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex','users.type as user_type','events_users.id as enrolled','events_users.user_id as enrolled_user_id','events_users.accepted as enrolled_status', 'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $eventsUsers = DB::table('events_users')->where('accepted','=',1)->where('user_id','=',$user->id)->get()->pluck('event_id')->toArray();
        $notification_count = count(DB::table('notifications')->where('notifiable_id','=',$user->id)->where('readit','=',0)->get());
        $latestCreatedEvents = $this->getLatestCreatedEvents();
        $latestAskedEvents = $this->getLatestAskedEvents();
        $latestAsblEvents = $this->getLatestAsblEvents();
        $latestWeweclass = $this->getLatestWeweclass();
        $students = [];
        $teachers = [];
        $generaltext = [];
        foreach ($events as $event)
        {
            if($event->user_type == 1)
            {
                $teachers[$event->id] = 1 ;
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',['0','4'])->where('accepted','=',1)->get());
                $students[$event->id] = $stud_count;
            }
            else
            {
                if($event->user_type == 0 || $event->user_type == 4)
                {
                    $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',['0','4'])->where('accepted','=',1)->get());
                    $teach_count = count(DB::table('events_users')->where('event_id','=',$event->id)->where('user_type','=',1)->where('accepted','=',1)->get());
                    $students[$event->id] = $stud_count + 1;
                    $teachers[$event->id] = $teach_count ;
                }
                else
                {
                    $students[$event->id] = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',['0','4'])->where('accepted','=',1)->get());
                    $teachers[$event->id] = 0;
                }
            }
            $event_user = DB::table('events_users')
                ->where('event_id','=',$event->id)->where('accepted','=',1)
                ->where('range_start','!=',NULL)->get()->groupBy('range_start', 'range_end');
            $generaltexts = '';
            if(count($event_user))
            {

                foreach ($event_user as $ev)
                {
                    $r_start = new DateTime($ev->first()->range_start);
                    $r_end = new DateTime($ev->first()->range_end);
                    $generaltexts .= count($ev).' student is register for event from '.$r_start->format('Y-m-d H:i A').' till '.$r_end->format('Y-m-d H:i A')." , ";
                }
                $generaltext[$event->id] = $generaltexts;
            }
            else
            {
                $generaltext[$event->id] = '';
            }

        }
        $localities = DB::table('localities')->get();
        $levels = DB::table('levels')->get();
        $courses = DB::table('courses')->get();

        // if($user->type==0 || $user->type==1) $user_level = $user->getLevel();
        // else $user_level = $levels->toArray();
        // if($user->type==1) $user_course = $user->getCourse();

        return view('home', compact('user','localities','courses','levels','events','latestCreatedEvents','latestAskedEvents','latestAsblEvents','latestWeweclass','notification_count','eventsUsers','students','teachers','generaltext'));
    }

    function getInfoCard(int $id)
    {
        try {
            $u = User::find($id);
            $user = '';
            if($u->type=='1') {
                $user = DB::table('users')->where('users.id','=',$id)
                ->join('localities','users.localite','=','localities.code')
                ->join('user_level','users.id','=','user_level.user_id')
                ->join('user_course','users.id','=','user_course.user_id')
                ->select('users.id as user_id', 'users.first_name', 'users.last_name', 'users.sex', 'users.type as owner_type', DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'), 'users.email', 'user_level.*', 'user_course.*')
                ->first();
            }
            else if($u->type=='0') {
                $user = DB::table('users')->where('users.id','=',$id)
                ->join('localities','users.localite','=','localities.code')
                ->join('user_level','users.id','=','user_level.user_id')
                ->select('users.id as user_id', 'users.first_name', 'users.last_name', 'users.sex', 'users.type as owner_type', DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'), 'users.email', 'user_level.*')
                ->first();
            }
            else if($u->type=='4') $user = DB::table('users')->where('users.id','=',$id)
                ->join('localities','users.localite','=','localities.code')
                ->select('users.id as user_id', 'users.first_name', 'users.last_name', 'users.sex', 'users.type as owner_type', DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'), 'users.email')
                ->first();

            $levelToShow = '';
            $courseToShow = '';
            if($user->owner_type!=4){
                if($user->primaire == 1) $levelToShow .= 'Primaire<br/>';
                if($user->sec_inf == 1) $levelToShow .= 'Secondaire inférieur<br/>';
                if($user->sec_sup == 1) $levelToShow .= 'Secondaire supérieur<br/>';
            }
            if($user->owner_type==1){
                if($user->edm == 1) $courseToShow .= 'Etude du milieu<br/>';
                if($user->math == 1) $courseToShow .= 'Math<br/>';
                if($user->francais == 1) $courseToShow .= 'Français<br/>';
                if($user->neerlandais == 1) $courseToShow .= 'Néerlandais<br/>';
                if($user->anglais == 1) $courseToShow .= 'Anglais<br/>';
                if($user->chimie == 1) $courseToShow .= 'Chimie<br/>';
                if($user->biologie == 1) $courseToShow .= 'Biologie<br/>';
                if($user->physique == 1) $courseToShow .= 'Physique<br/>';
            }

            $countDeal = DB::table('events_users')->join('events','events_users.event_id','=','events.id')->where('events_users.user_id','=',$id)->where('events.deal','=','1')->count();

            $rating = '';
            if($user->owner_type==1) $rating = User::find($user->user_id)->userAverageRating();

            return ['status' => 'success', 'userInfo' => $user, 'levelToShow' => $levelToShow, 'courseToShow' => $courseToShow, 'countDeal' => $countDeal, 'rating' => $rating];
        }
        catch (Exception $e) {
            return['status' => 'failed','message' => $e->getMessage()];
        }
    }

    function getWeweview()
    {
        $user = Auth::user();
        $notification_count = count(DB::table('notifications')->where('notifiable_id','=',$user->id)->where('readit','=',0)->get());
        $latestCreatedEvents = $this->getLatestCreatedEvents();
        $latestAskedEvents = $this->getLatestAskedEvents();
        $latestAsblEvents = $this->getLatestAsblEvents();
        $latestWeweclass = $this->getLatestWeweclass();
        $localities = DB::table('localities')->get();
        $levels = DB::table('levels')->get();
        $courses = DB::table('courses')->get();
        return view('weweview', compact('user','localities','courses','levels','latestCreatedEvents','latestAskedEvents','latestAsblEvents','latestWeweclass','notification_count'));
    }

    function getEventsByCourse($idCourse, Request $request)
    {
        $user = Auth::user();
        $events = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->where([['events.invalide','=','0'],["events.dhstart", ">=", DB::raw('NOW()')]])
            ->orderBy('events.dhstart', 'asc');

        $courses = DB::table('courses')->pluck('id')->toArray();
        $allCourses = array_merge([0],$courses); // [0] is to get 'Primaire'
        if($idCourse=='All') $events = $events->whereIn('events.course_id',$allCourses);
        else $events = $events->where('events.course_id','=', $idCourse);
            
        if($request->has('filterType') && $request->get('filterType')!=null)
        {
            if($request->get('filterType')=='asbl') $events = $events->where('events.type','2');
            else if($request->get('filterType')=='weweclass') $events = $events->where('events.type','1');
            else if($request->get('filterType')=='0') $events = $events->whereIn('users.type',[0,4]);
            else $events = $events->where('users.type',$request->get('filterType'));
        }
        $filterLocalite = array_filter($request->get('filterLocalite',[]),'strlen');
        if(!empty($filterLocalite))
        {
            $events = $events->whereIn('users.localite',$filterLocalite);
        }
        if($request->has('filterDealChk'))
        {
            $events = $events->where('events.deal','=',1);
        }
        if($request->has('student_home'))
        {
            $events = $events->where('events.student_home','=',1);
        }
        if($request->has('teacher_home'))
        {
            $events = $events->where('events.teacher_home','=',1);
        }
        if($request->has('private'))
        {
            $events = $events->where('events.private','=',1);
        }
        if($request->has('duration'))
        {
            $events = $events->where('events.duration','=',$request->get('duration'));
        }
        $events = $events->get(array('events.*','users.type as user_type', 'users.localite', 'users.sex as user_sex', 'users.first_name', 'users.last_name','events_users.id as enrolled','events_users.accepted as enrolled_status', 'events_users.user_id as enrolled_user_id', 'events_users.mobility', 'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));

        $students = [];
        $teachers = [];
        foreach ($events as $event)
        {
            if($event->user_type == 1)
            {
                $teachers[$event->id] = 1;
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                $students[$event->id] = $stud_count;
            }
            else
            {
                if($event->user_type == 0 || $event->user_type == 4)
                {
                    $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                    $teach_count = count(DB::table('events_users')->where('event_id','=',$event->id)->where('user_type','=',1)->where('accepted','=',1)->get());
                    $students[$event->id] = $stud_count + 1;
                    $teachers[$event->id] = $teach_count ;
                }
                else
                {
                    $students[$event->id] = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                    $teachers[$event->id] = 0;
                }
            }
        }
        return ['events' => $events, 'students' => $students, 'teachers' => $teachers, 'req' => $request->toArray()];
        //return view('weweview', compact('events', 'students', 'teachers', 'req'));
    }

    function getLatestCreatedEvents()
    {
        $user = Auth::user();
        $result = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->where([['users.type', '=', '1'],['events.invalide', '=', '0'], ["events.dhstart", ">=", DB::raw('CURDATE()')]])
            ->orderBy('events.id','desc')->limit(5)
            ->get(array('events.*','users.type as user_type','events_users.id as enrolled', 'courses.color', 'events_users.accepted as enrolled_status', 'courses.label_fr as course_label', 'levels.label_fr as level_label', 'events_users.user_id as enrolled_user_id'));
        $students = [];
        $teachers = [];
        foreach ($result as $event)
        {
            if($event->user_type == 1)
            {
                $teachers[$event->id] = 1 ;
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                $students[$event->id] = $stud_count;
            }
            else
            {
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                $teach_count = count(DB::table('events_users')->where('event_id','=',$event->id)->where('user_type','=',1)->where('accepted','=',1)->get());
                $students[$event->id] = $stud_count + 1;
                $teachers[$event->id] = $teach_count ;
            }
        }
        return ['latestCreatedEvents' => $result, 'students' => $students, 'teachers' => $teachers];
    }

    function getLatestAskedEvents()
    {
        $user = Auth::user();
        $result = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->whereIn('users.type',['0','4'])
            ->where([['events.invalide', '=', '0'], ["events.dhstart", ">=", DB::raw('CURDATE()')]])
            ->orderBy('events.id','desc')->limit(5)
            ->get(array('events.*','users.type as user_type','events_users.id as enrolled', 'courses.color', 'events_users.accepted as enrolled_status', 'courses.label_fr as course_label', 'levels.label_fr as level_label', 'events_users.user_id as enrolled_user_id'));
        $students = [];
        $teachers = [];
        foreach ($result as $event)
        {
            if($event->user_type == 1)
            {
                $teachers[$event->id] = 1 ;
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                $students[$event->id] = $stud_count;
            }
            else
            {
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                $teach_count = count(DB::table('events_users')->where('event_id','=',$event->id)->where('user_type','=',1)->where('accepted','=',1)->get());
                $students[$event->id] = $stud_count + 1;
                $teachers[$event->id] = $teach_count ;
            }
        }
        return ['latestAskedEvents' => $result, 'students' => $students, 'teachers' => $teachers];
    }

    function getLatestAsblEvents()
    {
        $user = Auth::user();
        $result = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->where([['events.type', '=', '2'],['events.invalide', '=', '0'], ["events.dhstart", ">=", DB::raw('CURDATE()')]])
            ->orderBy('events.id','desc')->limit(5)
            ->get(array('events.*','users.type as user_type','events_users.id as enrolled', 'courses.color', 'events_users.accepted as enrolled_status', 'courses.label_fr as course_label', 'levels.label_fr as level_label', 'events_users.user_id as enrolled_user_id'));
        return $result;
    }

    function getLatestWeweclass()
    {
        $user = Auth::user();
        $result = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->where([['events.type', '=', '1'],['events.invalide', '=', '0'], ["events.dhstart", ">=", DB::raw('CURDATE()')]])
            ->orderBy('events.id','desc')->limit(5)
            ->get(array('events.*','users.type as user_type','events_users.id as enrolled', 'courses.color', 'events_users.accepted as enrolled_status', 'courses.label_fr as course_label', 'levels.label_fr as level_label', 'events_users.user_id as enrolled_user_id'));
        $students = [];
        foreach ($result as $event)
        {
            
            $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
            $students[$event->id] = $stud_count;
        }
        return ['latestWeweclass'=>$result, 'students'=>$students];
    }

    function filter(Request $request)
    {
        $user = Auth::user();
        $result = [];
        $filterLocalite = array_filter($request->get('filterLocalite',[]),'strlen');
        $event_query = DB::table('events')
            ->join('levels', 'events.level_id', '=', 'levels.id')
            ->join('users', 'events.owner', '=', 'users.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftJoin('events_users', function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id)
                    ->whereIn('events_users.accepted',[0,1]);
            });
        if ($request->has('filterCourse') && $request->get('filterCourse')!=null)
        {
            if($request->get('filterCourse')=='0') $event_query = $event_query->where('events.level_id','1');
            else $event_query = $event_query->where('events.course_id',$request->get('filterCourse'));
        }
        if($request->has('filterType') && $request->get('filterType')!=null)
        {
            if($request->get('filterType')=='asbl') $event_query = $event_query->where('events.type','2');
            else if($request->get('filterType')=='weweclass') $event_query = $event_query->where('events.type','1');
            else if($request->get('filterType')=='0') $event_query = $event_query->whereIn('users.type',[0,4]);
            else $event_query = $event_query->where('users.type',$request->get('filterType'));
        }
        if(!empty($filterLocalite))
        {
            $event_query = $event_query->whereIn('users.localite',$filterLocalite);
        }
        if($request->has('filterDealChk'))
        {
            $event_query = $event_query->where('events.deal','=',1);
        }
        if($request->has('student_home'))
        {
            $event_query = $event_query->where('events.student_home','=',1);
        }
        if($request->has('teacher_home'))
        {
            $event_query = $event_query->where('events.teacher_home','=',1);
        }
        if($request->has('private'))
        {
            $event_query = $event_query->where('events.private','=',1);
        }
        if($request->has('duration'))
        {
            $event_query = $event_query->where('events.duration','=',$request->get('duration'));
        }

        $events = $event_query->where('events.invalide','=','0')
            ->where(function($event_query) use ($user){
                $event_query->where('events_users.user_id','=',$user->id)->orWhere('events.owner','=', $user->id);
            })
            ->get(array('events.*','users.type as user_type','events_users.id as enrolled','events_users.accepted as enrolled_status', 'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'))
            ->toArray();

        foreach($events as $event) {
            $teachers = 0 ;
            $students = 0 ;
            $generaltext = '';
            if($event->user_type == 1)
            {
                $teachers = 1 ;
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                $students = $stud_count;
            }
            else
            {
                $stud_count = count(DB::table('events_users')->where('event_id','=',$event->id)->whereIn('user_type',[0,4])->where('accepted','=',1)->get());
                $teach_count = count(DB::table('events_users')->where('event_id','=',$event->id)->where('user_type','=',1)->where('accepted','=',1)->get());
                $students = $stud_count + 1;
                $teachers = $teach_count ;
            }
            $event_user = DB::table('events_users')->where('event_id','=',$event->id)->where('accepted','=',1)->where('range_start','!=',NULL)->first();
            if($event_user)
            {
                $generaltext = 'One student is register for event from '.$event_user->range_start .' till '.$event_user->range_end;
            }

            array_push($result,
                [
                    'id' => $event->id,
                    'title' => (($event->type==1) ? ' <i class="fa fa-star" aria-hidden="true"></i> ' : '').($event->level_id == '1' ? $event->level_label : $event->course_label).' '.(($students) ? ' <span class="fa-stack" style="font-size:8px"><i class="fa fa-circle-o thin fa-stack-2x"></i><strong class="fa-stack-1x">'.$students.'</strong></span>' : '').' '.(($teachers) ? ' <i class="fa fa-male" aria-hidden="true"></i>' : ''),
                    'start' => $event->dhstart,
                    'end' => $event->dhend,
                    'descr' => $event->descr,
                    'owner' => $event->owner,
                    'level' => $event->level_id,
                    'course' => $event->course_id,
                    'student_home' => $event->student_home,
                    'teacher_home' => $event->teacher_home,
                    'private' => $event->private,
                    'color' => $event->color,
                    'user_type' => $event->user_type,
                    'hours' => $event->duration,
                    'type' => $event->type,
                    'address' => $event->address,
                    'enrolled' => $event->enrolled,
                    'enrolled_status' => $event->enrolled_status,
                    'student_count'=> $students,
                    'generaltext' => $generaltext
                ]);
        }
        return $result;
    }
}
