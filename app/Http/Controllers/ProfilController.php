<?php

namespace WEWEGO\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use WEWEGO\Child;

class ProfilController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$user = Auth::user();
		$userLocality = DB::table('localities')->join('users','users.localite','=','localities.code')->where('users.email','=',$user['email'])->get();
        $notification_count = count(DB::table('notifications')->where('notifiable_id','=',$user->id)->where('readit','=',0)->get());
        return view('profil', compact('user', 'userLocality','notification_count'));
	}

	protected function update(Request $request)
	{
		$id = Auth::user()->id;
		
		DB::table('users')->where('id',$id)->update(['first_name'=>$request['first_name'], 'last_name'=>$request['last_name'], 'email'=>$request['email'], 'localite'=>$request['localite'], 'phone'=>$request['phone']]);
		
		// Faire un insert au cas où il n'y a pas de ligne existante à modifier
        $levels = $request['level'];
        $levelToInsert = array(0, 0, 0);
        foreach ($levels as $l) {
            if($l == 1) $levelToInsert[0] = 1;
            if($l == 2) $levelToInsert[1] = 1;
            if($l == 3) $levelToInsert[2] = 1;
        }
		$levelExist = DB::table('user_level')->where('user_id','=',$id)->get();
        if($levelExist->isEmpty()) DB::insert('insert into user_level (user_id, primaire, sec_inf, sec_sup) values (?, ?, ?, ?)', [$id, $levelToInsert[0], $levelToInsert[1], $levelToInsert[2]]);
        else DB::table('user_level')->where('user_id',$id)->update(['primaire'=>$levelToInsert[0], 'sec_inf'=>$levelToInsert[1], 'sec_sup'=>$levelToInsert[2]]);
		if(isset($request['course']))
		{
			$courses = $request['course'];
			$courseToInsert = array(0, 0, 0, 0, 0, 0, 0, 0);
			foreach ($courses as $c) {
				if($c == 1) $courseToInsert[0] = 1;
                if($c == 2) $courseToInsert[1] = 1;
                if($c == 3) $courseToInsert[2] = 1;
                if($c == 4) $courseToInsert[3] = 1;
                if($c == 5) $courseToInsert[4] = 1;
                if($c == 6) $courseToInsert[5] = 1;
                if($c == 7) $courseToInsert[6] = 1;
                if($c == 8) $courseToInsert[7] = 1;
			}
			$courseExist = DB::table('user_course')->where('user_id','=',$id)->get();
			if($courseExist->isEmpty()) DB::insert('insert into user_course (user_id, edm, math, francais, neerlandais, anglais, chimie, biologie, physique) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$id, $courseToInsert[0], $courseToInsert[1], $courseToInsert[2], $courseToInsert[3], $courseToInsert[4], $courseToInsert[5], $courseToInsert[6], $courseToInsert[7]]);
			else DB::table('user_course')->where('user_id',$id)->update(['edm'=>$courseToInsert[0], 'math'=>$courseToInsert[1], 'francais'=>$courseToInsert[2], 'neerlandais'=>$courseToInsert[3], 'anglais'=>$courseToInsert[4], 'chimie'=>$courseToInsert[5], 'biologie'=>$courseToInsert[6], 'physique'=>$courseToInsert[7]]);
		}
		
		return redirect('/');
	}

	protected function manageChildren()
	{
		$user = Auth::user();
		$notification_count = count(DB::table('notifications')->where('notifiable_id','=',$user->id)->where('readit','=',0)->get());
		$children = [];
		$children = Child::where('parent',$user->id)->get();
		$levels = DB::table('levels')->get();
		return view('children', compact('notification_count','children', 'levels'));
	}

	protected function updateChildren(Request $request)
	{
		if($request->has('child0')){
			$idChild1 = $request->get('child0');
			$child1 = Child::find($idChild1);
			$child1->level = $request->get('childLevel_'.$idChild1);
			$child1->save();
		}
		if($request->has('child1')){
			$idChild2 = $request->get('child1');
			$child2 = Child::find($idChild2);
			$child2->level = $request->get('childLevel_'.$idChild2);
			$child2->save();
		}
		if($request->has('child2')){
			$idChild3 = $request->get('child2');
			$child3 = Child::find($idChild3);
			$child3->level = $request->get('childLevel_'.$idChild3);
			$child3->save();
		}
		if($request->has('child3')){
			$idChild4 = $request->get('child3');
			$child4 = Child::find($idChild4);
			$child4->level = $request->get('childLevel_'.$idChild4);
			$child4->save();
		}
		return redirect()->back();
	}
}
