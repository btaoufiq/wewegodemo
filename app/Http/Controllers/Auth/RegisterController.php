<?php

namespace WEWEGO\Http\Controllers\Auth;

use WEWEGO\User;
use WEWEGO\Child;
use WEWEGO\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'last_name' => 'required|string|max:255',
            'first_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'type' => 'required|integer',
            'localite' => 'string|max:255',
            'phone' => 'string|max:15',
            'sex' => 'required|string|max:1',
            //'school_id' => 'required|integer',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \WEWEGO\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'last_name' => $data['last_name'],
            'first_name' => $data['first_name'],
            'email' => $data['email'],
            'type' => $data['type'],
            'localite' => $data['localite'],
            'phone' => $data['phone'],
            'sex' => $data['sex'],
            'school_id' => "0",
            'password' => bcrypt($data['password']),
        ]);
        $id= $user->id;
        if($data['type']==0)
        {
            $levelToInsert = array(0, 0, 0);
            if($data['level'] == 1) $levelToInsert[0] = 1;
            if($data['level'] == 2) $levelToInsert[1] = 1;
            if($data['level'] == 3) $levelToInsert[2] = 1;
            DB::insert('insert into user_level (user_id, primaire, sec_inf, sec_sup) values (?, ?, ?, ?)', [$id, $levelToInsert[0], $levelToInsert[1], $levelToInsert[2]]);
        }
        if($data['type']==1)
        {
            // Pour le(s) niveau(x) du prof
            if(isset($_POST['level']))
            {
                $levelToInsert = array(0, 0, 0);
                foreach ($_POST['level'] as $l) {
                    if($l == 1) $levelToInsert[0] = 1;
                    if($l == 2) $levelToInsert[1] = 1;
                    if($l == 3) $levelToInsert[2] = 1;
                }
                DB::insert('insert into user_level (user_id, primaire, sec_inf, sec_sup) values (?, ?, ?, ?)', [$id, $levelToInsert[0], $levelToInsert[1], $levelToInsert[2]]);
            }
            // Pour le(s) cours du prof
            if(isset($_POST['course']))
            {
                $courseToInsert = array(0, 0, 0, 0, 0, 0, 0, 0);
                foreach ($_POST['course'] as $c) {
                    if($c == 1) $courseToInsert[0] = 1;
                    if($c == 2) $courseToInsert[1] = 1;
                    if($c == 3) $courseToInsert[2] = 1;
                    if($c == 4) $courseToInsert[3] = 1;
                    if($c == 5) $courseToInsert[4] = 1;
                    if($c == 6) $courseToInsert[5] = 1;
                    if($c == 7) $courseToInsert[6] = 1;
                    if($c == 8) $courseToInsert[7] = 1;
                }
                DB::insert('insert into user_course (user_id, edm, math, francais, neerlandais, anglais, chimie, biologie, physique) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$id, $courseToInsert[0], $courseToInsert[1], $courseToInsert[2], $courseToInsert[3], $courseToInsert[4], $courseToInsert[5], $courseToInsert[6], $courseToInsert[7]]);
            }
            
        }
        return $user;
    }

    protected function createFamily(Request $data)
    {
        $user = User::create([
            'last_name' => $data['parent_last_name'],
            'first_name' => $data['parent_first_name'],
            'email' => $data['parent_email'],
            'type' => $data['parent_type'],
            'localite' => $data['parent_localite'],
            'phone' => $data['parent_phone'],
            'sex' => $data['parent_sex'],
            'school_id' => "0",
            'password' => bcrypt($data['parent_password']),
        ]);
        if($data->has('child1_last_name') && $data->get('child1_last_name')!=null){
            Child::create([
                'first_name' => $data->get('child1_first_name'),
                'last_name' => $data->get('child1_last_name'),
                'sex' => $data->get('child1_sex'),
                'school_id' => '0',
                'level' => $data->get('child1_level'),
                'parent' => $user->id
            ]);
        }
        if($data->has('child2_last_name') && $data->get('child2_last_name')!=null){
            Child::create([
                'first_name' => $data->get('child2_first_name'),
                'last_name' => $data->get('child2_last_name'),
                'sex' => $data->get('child2_sex'),
                'school_id' => '0',
                'level' => $data->get('child2_level'),
                'parent' => $user->id
            ]);
        }
        if($data->has('child3_last_name') && $data->get('child3_last_name')!=null){
            Child::create([
                'first_name' => $data->get('child3_first_name'),
                'last_name' => $data->get('child3_last_name'),
                'sex' => $data->get('child3_sex'),
                'school_id' => '0',
                'level' => $data->get('child3_level'),
                'parent' => $user->id
            ]);
        }
        if($data->has('child4_last_name') && $data->get('child4_last_name')!=null){
            Child::create([
                'first_name' => $data->get('child4_first_name'),
                'last_name' => $data->get('child4_last_name'),
                'sex' => $data->get('child4_sex'),
                'school_id' => '0',
                'level' => $data->get('child4_level'),
                'parent' => $user->id
            ]);
        }

        return redirect('/');
    }

}
