<?php

namespace WEWEGO\Http\Controllers;

use Carbon\Carbon;
use WEWEGO\Messages;
use WEWEGO\Notification;
use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use League\Flysystem\Exception;
use WEWEGO\User;
use WEWEGO\Event;
use WEWEGO\SuitMe;
use WEWEGO\Child;
use WEWEGO\Rating;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createEvent(Request $request)
    {
        $this->validate($request,
            [
                'dhstart' => 'required',
                'dhend' => 'required'
            ]);
        $id = Auth::user()->id;
        if($_POST['level'] == 1) $course = 0;
        else $course = $request['course'];
        $now = new DateTime();
        $dateStart = DateTime::createFromFormat('d/m/Y H', $request['dhstart']);
        if($now > $dateStart)
        {
            $errors = [
                'event_validation'  => 'Vous ne pouvez pas créer un cours dans le passé'
            ];
            return redirect('/')->withErrors($errors);
        }
        elseif(($now->format('Y-m-d') == $dateStart->format('Y-m-d')) && $now->diff($dateStart)->h < 3)
        {
            $errors = [
                'event_validation'  => 'Vous devez créer le cours au moins 3 heures à l\'avance'
            ];
            return redirect('/')->withErrors($errors);
        }
        elseif(!(isset($request['student_home'])) && !(isset($request['teacher_home'])))
        {
            $errors = [
                'event_validation'  => 'Vous devez choisir au moins un des deux champs de mobilité (Domicile élève / Domicile professeur)'
            ];
            return redirect('/')->withErrors($errors);
        }

        $child = (Auth::user()->type == '4') ? $request['children'] : null;
        
        $usableDateStart = $dateStart->format('Y-m-d H:i:s');
        $dateEnd = DateTime::createFromFormat('d/m/Y H', $request['dhend']);
        $usableDateEnd = $dateEnd->format('Y-m-d H:i:s');
        $hours = $dateStart->diff($dateEnd)->h;
        Event::create([
            'dhstart' => $usableDateStart,
            'dhend' => $usableDateEnd,
            'duration' => $hours,
            'level_id' => $request['level'],
            'course_id' => $course,
            'descr' => $request['descr'],
            'owner' => $id,
            'child' => $child,
            'student_home' => $request['student_home'],
            'teacher_home' => $request['teacher_home'],
            'private' => $request['private'],
            'deal' => 0,
            'invalide' => 0
        ]);
        return redirect('/');
    }

    public function addWeweclass(Request $request)
    {
        //dd($request);
        $dateStart = DateTime::createFromFormat('d/m/Y H', $request->get('wewedhstart'));
        $usableDateStart = $dateStart->format('Y-m-d H:i:s');
        $dateEnd = DateTime::createFromFormat('d/m/Y H', $request->get('wewedhend'));
        $usableDateEnd = $dateEnd->format('Y-m-d H:i:s');
        $weweclass = Event::create([
            'level_id' => $request->get('wewelevel'),
            'course_id' => $request->get('wewecourse'),
            'dhstart' => $usableDateStart,
            'dhend' => $usableDateEnd,
            'duration' => $request->get('weweaddHours'),
            'descr' => $request->get('wewedescr'),
            'address' => $request->get('weweaddress'),
            'owner' => Auth()->user()->id,
            'type' => 1,
            'invalide' => 0
        ]);
        return redirect('/');
    }

    public function editEvent(Request $request)
    {
        $dateStart = DateTime::createFromFormat('d/m/Y H', $request['editDhstart']);
        $usableDateStart = $dateStart->format('Y-m-d H:i:s');
        $dateEnd = DateTime::createFromFormat('d/m/Y H', $request['editDhEnd']);
        $usableDateEnd = $dateEnd->format('Y-m-d H:i:s');
        $hours = $dateStart->diff($dateEnd)->h;
        $event_update = Event::find($request['id']);
        $event_update->dhstart = $usableDateStart;
        $event_update->dhend = $usableDateEnd;
        $event_update->duration = $hours;
        $event_update->save();
        $users = DB::table('events_users')->where('event_id','=',$request['id'])->get()->pluck('user_id')->toArray();
        foreach ($users as $uid)
        {
            Notification::create([
                'notifiable_id' => $uid,
                'creator_id' => Auth()->user()->id,
                'event_id' => $request['id'],
                'description' => Auth()->user()->first_name . ' ' . Auth()->user()->last_name . ' a modifié ce cours',
                'readit' => 0,
                'showoption' => 0
            ]);
        }
        return redirect('/');
    }

    public function deleteEvent(Request $request)
    {
        DB::table('events')->where('id', '=', $request['elementIdTodelete'])->update(['invalide'=>'1']);
        return redirect()->back();
    }

    public function getList() // main method for myList.blade.php
    {
        $user = Auth::user();
        $userWeweclass = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftjoin('events_users',function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->where('events.owner', '=', $user->id)
            ->where('events.type','=','1')
            ->whereNotIn('events.invalide',['1','2'])
            ->get(array('events.*', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type',DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));

        //*** upcomming events
        $userEvents = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftJoin('children','events.child','=','children.id')
            ->whereNotIn('events.invalide',['1','2'])
            ->where('events.owner', '=', $user->id)
            ->where('events.type','=','0')
            ->where("events.dhstart", ">=", DB::raw('CURDATE()'))
            ->get(array('events.*', 'children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type',DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $appliedEventIds = DB::table('events_users')->where('user_id','=',$user->id)->get()->pluck('event_id')->toArray();
        $appliedEvents = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('events_users',function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->whereNotIn('events.invalide',['1','2'])
            ->where("events.dhstart", ">=", DB::raw('CURDATE()'))
            ->leftJoin('children','events_users.child','=','children.id')
            ->whereIn('events.id',$appliedEventIds)
            ->where('events_users.accepted','<>','2')
            ->get(array('events.*','events_users.accepted as accepted', 'children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type', DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $upcommingEvents = $userEvents->merge($appliedEvents);
        $upcommingEvents = array_values(array_sort($upcommingEvents, function($event){ //asc
            return $event->dhstart;
        }));
        $upcommingEventsIds = collect($upcommingEvents)->pluck('id')->toArray();
        if(!empty($upcommingEvents)){
            $countUpcommingEventsNotificationIds = count(Notification::where('notifiable_id',$user->id)->whereIn('event_id',$upcommingEventsIds)->where('readit','0')->get());
        }
        else $countUpcommingEventsNotificationIds = 0;

        //**** past events
        $pastUserEvents = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftJoin('children','events.child','=','children.id')
            ->whereNotIn('events.invalide',['1','2'])
            ->where('events.owner', '=', $user->id)
            ->where('events.type','=','0')
            ->where("events.dhstart", "<", DB::raw('CURDATE()'))
            ->get(array('events.*', 'children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type',DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $pastAppliedEvents = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftJoin('children','events.child','=','children.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('events_users',function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->whereNotIn('events.invalide',['1','2'])
            ->where("events.dhstart", "<", DB::raw('CURDATE()'))
            ->whereIn('events.id',$appliedEventIds)
            ->get(array('events.*','events_users.accepted as accepted', 'children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type', DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $pastEvents = $pastUserEvents->merge($pastAppliedEvents);
        $pastEvents = array_reverse(array_sort($pastEvents, function($event){ //desc
            return $event->dhstart;
        }));
        if(!empty($pastEvents)){
            $pastEventsIds = collect($pastEvents)->pluck('id')->toArray();
            $countPastEventsNotificationIds = count(Notification::where('notifiable_id',$user->id)->whereIn('event_id',$pastEventsIds)->where('readit','0')->get());
        }
        else $countPastEventsNotificationIds = 0;

        //*** declined events
        $declinedEvents = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('events_users',function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->whereNotIn('events.invalide',['1','2'])
            ->where("events.dhstart", ">=", DB::raw('CURDATE()'))
            ->leftJoin('children','events_users.child','=','children.id')
            ->whereIn('events.id',$appliedEventIds)
            ->where('events_users.accepted','=','2')
            ->get(array('events.*','events_users.accepted as accepted', 'children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type', DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        if(!empty($declinedEvents)){
            $declinedEventsIds = collect($declinedEvents)->pluck('id')->toArray();
            $countDeclinedEventsNotificationIds = count(Notification::where('notifiable_id',$user->id)->whereIn('event_id',$declinedEventsIds)->where('readit','0')->get());
        }
        else $countDeclinedEventsNotificationIds = 0;
        
        //*** invalid events
        $invalidUserEvents = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftJoin('children','events.child','=','children.id')
            ->where('events.invalide','2')
            ->where('events.owner', '=', $user->id)
            ->where('events.type','=','0')
            ->get(array('events.*', 'children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type',DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $invalidUserWeweclass = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftjoin('events_users',function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->where('events.invalide','2')
            ->where('events.owner', '=', $user->id)
            ->where('events.type','=','1')
            ->get(array('events.*', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type',DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $invalidAppliedEvents = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
            ->leftjoin('users','events.owner','=','users.id')
            ->leftjoin('localities','users.localite','=','localities.code')
            ->leftjoin('courses', 'events.course_id', '=', 'courses.id')
            ->leftjoin('events_users',function ($join) use ($user)
            {
                $join->on('events_users.event_id','=','events.id')
                    ->where('events_users.user_id','=',$user->id);
            })
            ->where('events.invalide','2')
            ->leftJoin('children','events_users.child','=','children.id')
            ->whereIn('events.id',$appliedEventIds)
            ->get(array('events.*','events_users.accepted as accepted', 'children.first_name as child_first_name', 'children.last_name as child_last_name', 'children.sex as child_sex', DB::raw('CONCAT(users.first_name," ",users.last_name) as owner_name'), 'users.type as owner_type', DB::raw('CONCAT(localities.code," ",localities.label_fr) as owner_locality'),'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $invalidEvents = $invalidUserEvents->merge($invalidUserWeweclass)->merge($invalidAppliedEvents);
        $invalidEvents = array_values(array_sort($invalidEvents, function($event){
            return $event->dhstart;
        }));
        if(!empty($invalidEvents)){
            $invalidEventsIds = collect($invalidEvents)->pluck('id')->toArray();
            $countInvalidEventsNotificationIds = count(Notification::where('notifiable_id',$user->id)->whereIn('event_id',$invalidEventsIds)->where('readit','0')->get());
        }
        else $countInvalidEventsNotificationIds = 0;

        $eventsUsers = DB::table('events_users')->whereIn('event_id',$upcommingEventsIds)->get()->toArray();
        $notification_query = Notification::where('notifiable_id',$user->id);
        $notifications = $notification_query->with('messages')->get();
        $notificationEventIds = $notifications->pluck('event_id')->toArray();
        $notification_count = count($notification_query->where('readit','=',0)->get());
        $acceptedEventUserIds = DB::table('events_users')
            ->where('user_id','!=',Auth()->user()->id)
            ->whereIn('event_id',collect($userEvents)->pluck('id')->toArray())
            ->where('accepted','=',1)
            ->get()->pluck('event_id','user_id')->toArray();
        $declinedEventUserIds = DB::table('events_users')->where('user_id','!=',Auth()->user()->id)
            ->whereIn('event_id',collect($userEvents)->pluck('id')->toArray())
            ->where('accepted','=',2)
            ->get()->pluck('event_id','user_id')->toArray();
        $acceptedTeachersIds = DB::table('events_users')
            ->where('user_id','!=',Auth()->user()->id)
            ->whereIn('event_id',collect($userEvents)->pluck('id')->toArray())
            ->where('user_type',1)
            ->where('accepted','=',1)
            ->get()->pluck('event_id','user_id')->toArray();
        $acceptedNotificationIds = DB::table('events_users')->join('notifications',function($join){
                $join->on('notifications.event_id','=','events_users.event_id');
                $join->on('notifications.creator_id','=','events_users.user_id');
            })
            ->whereIn('notifications.event_id',$acceptedEventUserIds)
            ->whereIn('notifications.creator_id',array_keys($acceptedEventUserIds))
            ->where('events_users.accepted',1)
            ->get()->pluck('id')->toArray();
        $declinedNotificationIds = DB::table('events_users')->join('notifications',function($join){
                $join->on('notifications.event_id','=','events_users.event_id');
                $join->on('notifications.creator_id','=','events_users.user_id');
            })
            ->whereIn('notifications.event_id',$declinedEventUserIds)
            ->whereIn('notifications.creator_id',array_keys($declinedEventUserIds))
            ->where('events_users.accepted',2)
            ->get()->pluck('id')->toArray();
        $acceptedTeachersNotificationIds = DB::table('events_users')->join('notifications',function($join){
                $join->on('notifications.event_id','=','events_users.event_id');
                $join->on('notifications.creator_id','=','events_users.user_id');
            })
            ->whereIn('notifications.event_id',$acceptedTeachersIds)
            ->whereIn('notifications.creator_id',array_keys($acceptedTeachersIds))
            ->where('events_users.accepted',1)
            ->get()->pluck('id','event_id')->toArray();
        $messages = Notification::where('creator_id',$user->id)->with('messages')->get();
        $mobility = DB::table('events_users')
            ->where('user_id','!=',Auth()->user()->id)
            ->whereIn('event_id',$upcommingEventsIds)
            ->get()->toArray();
        $localities = DB::table('localities')->get();
        $levels = DB::table('levels')->get();
        $courses = DB::table('courses')->get();

        //dump($userEvents);

        return view('myList', compact('localities','courses','levels','userWeweclass','notifications','notification_count','notificationEventIds','acceptedNotificationIds','declinedNotificationIds','acceptedTeachersNotificationIds','messages','eventsUsers','upcommingEvents','pastEvents','declinedEvents','invalidEvents','countUpcommingEventsNotificationIds','countPastEventsNotificationIds','countDeclinedEventsNotificationIds','countInvalidEventsNotificationIds'));
    }

    public function readNotification($id)
    {
        try{
            $notification = Notification::find($id);
            $notification->readit = 1;
            $notification->save();
            return ['status' => 'success', 'message'=>'Notification read successfully'];
        }
        catch(Exception $e)
        {
            return ['status' => 'failed', 'message' => $e->getMessage()];
        }
    }

    public function deleteMyEvent($id)
    {
        $event = Event::find($id);
        $course = DB::table('courses')->join('events','events.course_id','=','courses.id')->where('events.id','=',$event->id)->get(array('courses.label_fr'));
        if(empty($course)) $course = 'primaire';
        $users = DB::table('events_users')->where('event_id','=',$id)->get()->pluck('user_id')->toArray();
        foreach ($users as $uid)
        {
            //$description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' has deleted the event '.$event->descr.' on '.Carbon::now();
            $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' a supprimé ce cours';
            Notification::create([
                'notifiable_id' => $uid,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 0,
                'general' => 1
            ]);
        }
        $event->invalide = 1;
        $event->save();
        return redirect()->back();
    }

    public function confirmEvent($id,Request $request)
    {
        $event = DB::table('events')->leftJoin('users', 'events.owner', '=', 'users.id')->where('events.id', '=', $id)
            ->get(['events.*'])[0];
        $now = new DateTime();
        $startDate = new DateTime($event->dhstart);
        $event_query= DB::table('events_users')->where('event_id', '=', $id);
        $students = $event_query->where('accepted','=',1)->whereIn('user_type',[0,4])->get();
        $total = $event_query->get();

        if($event->private &&  count($students) == 1)
        {
            return ['status' => 'failed','message' => 'Ce cours particulier est déjà réservé pour un autre élève'];
        }
        if(count($students) == 3)
        {
            return ['status' => 'failed','message' => 'Ce cours a atteint le nombre maximum de participants (3 élèves)'];
        }

        if($now > $startDate)
        {
            return ['status' => 'failed','message' => 'Vous ne pouvez pas participer à un cours dans le passé'];
        }
        elseif(($now->format('Y-m-d') == $startDate->format('Y-m-d')) && $now->diff($startDate)->h < 2)
        {
            return ['status' => 'failed','message' => 'Le cours doit être confirmé au moins 2 heures à l\'avance'];
        }
        $exists = DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',0)->first();

        if($exists)
        {
            DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',0)->update(['accepted' => 0]);
        }
        else {
            $description = Auth()->user()->first_name . ' ' . Auth()->user()->last_name . ' souhaite participer à ce cours';
            $mobility = null;
            if($request->has('mobility')) $mobility = $request->get('mobility');
            else{
                if($event->student_home == 1) $mobility = 0;
                if($event->teacher_home == 1) $mobility = 1;
            }

            DB::insert('insert into events_users (event_id, user_id, user_type, mobility) values (?, ?, ?, ?)', [$event->id, Auth()->user()->id, 0, $mobility]);

            $notification = Notification::create([
                'notifiable_id' => $event->owner,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 1
            ]);
        }
        
        //add message
        if($request->has('message') && $request['message']!=null && $notification)
        {
            $msd_desc = $request->get('message');
            $message = new Messages();
            $message->notification_id = $notification->id;
            $message->message_from = Auth()->user()->id;
            $message->message_to = $event->owner;
            $message->message_type = 'participate';
            $message->description = $msd_desc;
            $message->save();
        }
        return ['status' => 'success'];
    }

    public function confirmChildEvent($id,Request $request)
    {
        $event = DB::table('events')->leftJoin('users', 'events.owner', '=', 'users.id')->where('events.id', '=', $id)->get(['events.*'])[0];
        $now = new DateTime();
        $startDate = new DateTime($event->dhstart);
        $event_query= DB::table('events_users')->where('event_id', '=', $id);
        $students = $event_query->where('accepted','=',1)->where('user_type',[0,4])->get();
        $total = $event_query->get();

        if($event->private &&  count($students) == 1)
        {
            return ['status' => 'failed','message' => 'Ce cours particulier est déjà réservé pour un autre élève'];
        }
        if(count($students) == 3)
        {
            return ['status' => 'failed','message' => 'Ce cours a atteint le nombre maximum de participants (3 élèves)'];
        }

        if($now > $startDate)
        {
            return ['status' => 'failed','message' => 'Vous ne pouvez pas participer à un cours dans le passé'];
        }
        elseif(($now->format('Y-m-d') == $startDate->format('Y-m-d')) && $now->diff($startDate)->h < 2)
        {
            return ['status' => 'failed','message' => 'Le cours doit être confirmé au moins 2 heures à l\'avance'];
        }
        $exists = DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',0)->where('child','=',$request['childToConfirm'])->first();

        if($exists)
        {
            DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',0)->where('child','=',$request->get('childToConfirm'))->update(['accepted' => 0]);
        }
        else {
            $child = Child::find($request->get('childToConfirm'));
            $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' souhaite inscrire son enfant '.$child->first_name.' '.$child->last_name.' à ce cours';
            $mobility = null;
            if($request->has('mobility')) $mobility = $request->get('mobility');
            else{
                if($event->student_home == 1) $mobility = 0;
                if($event->teacher_home == 1) $mobility = 1;
            }
            $child = null;
            if($request->has('childToConfirm')) $child = $request->get('childToConfirm');
            DB::insert('insert into events_users (event_id, user_id, user_type, mobility, child) values (?, ?, ?, ?, ?)', [$event->id, Auth()->user()->id, 4, $mobility, $child]);

            $notification = Notification::create([
                'notifiable_id' => $event->owner,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 1
            ]);
        }
        
        //add message
        if($request->has('message') && $request['message']!=null && $notification)
        {
            $msd_desc = $request->get('message');
            $message = new Messages();
            $message->notification_id = $notification->id;
            $message->message_from = Auth()->user()->id;
            $message->message_to = $event->owner;
            $message->message_type = 'participate';
            $message->description = $msd_desc;
            $message->save();
        }
        return ['status' => 'success'];
    }

    public function proposeEvent($id,Request $request)
    {
        $event = DB::table('events')->where('id', '=', $id)->get()[0];
        $now = new DateTime();
        $startDate = new DateTime($event->dhstart);
        $event_query= DB::table('events_users')->where('event_id', '=', $id);
        $teacher = $event_query->where('user_type','=',1)->where('accepted','=',1)->get();
        $total = $event_query->get();
        if(count($teacher))
        {
            return ['status' => 'failed','message' => 'Ce cours a déjà un professeur'];
        }
        if($now > $startDate)
        {
            return ['status' => 'failed','message' => 'Vous ne pouvez pas vous proposer à un cours passé'];
        }
        elseif(($now->format('Y-m-d') == $startDate->format('Y-m-d')) && $now->diff($startDate)->h < 2)
        {
            return ['status' => 'failed','message' => 'Le cours doit être confirmé au moins 2 heures à l\'avance'];
        }
        try
        {
            $exists = DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',1)->first();
            if($exists)
            {
                DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',1)->update(['accepted' => 0]);
            }
            else
            {
                $mobility = null;
                if($request->has('mobility')) $mobility = $request->get('mobility');
                else{
                    if($event->student_home == 1) $mobility = 0;
                    if($event->teacher_home == 1) $mobility = 1;
                }
                DB::insert('insert into events_users (event_id, user_id, user_type, mobility) values (?, ?, ?, ?)', [$event->id, Auth()->user()->id, 1, $mobility]);

                $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' s\'est proposé(e) pour ce cours';

                $notification = Notification::create([
                    'notifiable_id' => $event->owner,
                    'creator_id' => Auth()->user()->id,
                    'event_id' => $event->id,
                    'description' => $description,
                    'readit' => 0,
                    'showoption' => 1
                ]);
            }
            //add message
            if($request->has('message') && $request['message']!=null && $notification)
            {
                $msd_desc = $request->get('message');
                $message = new Messages();
                $message->notification_id = $notification->id;
                $message->message_from = Auth()->user()->id;
                $message->message_to = $event->owner;
                $message->message_type = 'propose';
                $message->description = $msd_desc;
                $message->save();
            }
            return ['status' => 'success'];
        }
        catch (Exception $e) {
            return['status' => 'failed','message' => $e->getMessage()];
        }
    }

    public function withdrawEvent($id)
    {
        $event = Event::find($id);
        $eventUserQuery = DB::table('events_users')->where('event_id','=',$id);
        DB::table('events_users')->where('event_id','=',$id)->where('user_id','=',Auth()->user()->id)->delete();
        DB::table('notifications')->where('event_id','=',$id)
            ->where('creator_id','=',Auth()->user()->id)
            ->where('notifiable_id','=',$event->owner)
            ->delete();
        $acceptedCount = count($eventUserQuery->where('accepted','=',1)->get());
        if(!$acceptedCount)
        {
            DB::table('events')->where('id','=',$id)->update(['deal' => 0]);
        }
        if($eventUserQuery->where('user_id','=',Auth()->user()->id)->first() && ($eventUserQuery->where('user_id','=',Auth()->user()->id)->first()->accepted == 1))
        {
            $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' s\'est retiré(e) de ce cours';
            Notification::create([
                'notifiable_id' => $event->owner,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 0
            ]);
        }

        return ['status' => 'success'];
    }

    public function unsubscribeEvent($id)
    {
        $event = Event::find($id);
        $eventUserQuery = DB::table('events_users')->where('event_id','=',$id);
        DB::table('notifications')->where('event_id','=',$id)
            ->where('notifiable_id','=',$event->owner)
            ->where('creator_id','=',Auth()->user()->id)
            ->delete();

        $acceptedCount = count(DB::table('events_users')->where('event_id','=',$id)->where('accepted','=',1)->get());
        if(!$acceptedCount)
        {
            DB::table('events')->where('id','=',$id)->update(['deal' => 0]);
        }
        if(DB::table('events_users')->where('event_id','=',$id)->where('user_id','=',Auth()->user()->id)->first() && (DB::table('events_users')->where('event_id','=',$id)->where('user_id','=',Auth()->user()->id)->first()->accepted == 1))
        {
            $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' s\'est désinscrit(e) de ce cours';
            Notification::create([
                'notifiable_id' => $event->owner,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 0
            ]);
        }
        $eventUserQuery->where('user_id','=',Auth()->user()->id)->delete();

        return ['status' => 'success'];
    }

    public function acceptEvent($id)
    {
        $notification = Notification::find($id);
        $participant = User::find($notification->creator_id);
        $event = Event::find($notification->event_id);
        $event_query = DB::table('events_users')->where('event_id','=',$event->id);
        $students = $event_query->whereIn('user_type',[0,4])->where('accepted',1)->get();
        $teacher = $event_query->where('user_type',1)->where('accepted',1)->get()->first();
        if($event->private &&  count($students) == 1)
        {
            return ['status' => 'failed','message' => 'Ce cours particulier est déjà réservé pour un autre élève'];
        }
        if(count($students) == 3)
        {
            return ['status' => 'failed','message' => 'Ce cours a atteint le nombre maximum de participants (3 élèves)'];
        }
        if(count($teacher) == 1)
        {
            return ['status' => 'failed','message' => 'Ce cours a déjà un professeur confirmé'];
        }
        try{
            $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' a confirmé votre participation à ce cours pour le '.date('d/m/Y', strtotime($event->dhstart)).' à '.date('H', strtotime($event->dhstart)).'h';
            $description_to_students = Auth()->user()->first_name.' '.Auth()->user()->last_name.' a confirmé la participation de '.$participant->first_name.' '.$participant->last_name.' à ce cours pour le '.date('d/m/Y', strtotime($event->dhstart)).' à '.date('H', strtotime($event->dhstart)).'h';

            DB::table('events_users')->where('event_id',$notification->event_id)->where('user_id',$notification->creator_id)
                ->update(['accepted' => '1']);

            Notification::create([
                'notifiable_id' => $participant->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 0
            ]);

            if(count($students))
            {
                foreach ($students as $student)
                {
                    if($student->user_id != $event->owner)
                    {
                        Notification::create([
                            'notifiable_id' => $student->user_id,
                            'creator_id' => Auth()->user()->id,
                            'event_id' => $event->id,
                            'description' => $description_to_students,
                            'readit' => 0,
                            'showoption' => 0
                        ]);
                    }
                }
            }
            if(count($teacher))
            {
                if($teacher->user_id != $event->owner)
                {
                    Notification::create([
                        'notifiable_id' => $teacher->user_id,
                        'creator_id' => Auth()->user()->id,
                        'event_id' => $event->id,
                        'description' => $description_to_students,
                        'readit' => 0,
                        'showoption' => 0
                    ]);
                }
                
            }
            DB::table('events')->where('id','=',$notification->event_id)->update(['deal' => 1]);
            return ['status' => 'success'];
        }
        catch(Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function declineEvent($id,Request $request)
    {
        $notification = Notification::find($id);
        $nstudent = User::find($notification->creator_id);
        $event = Event::find($notification->event_id);
        $event_query = DB::table('events_users')->where('event_id','=',$notification->event_id);

        try
        {
            $notification->showoption = 1;
            $notification->save();
            $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' a décliné votre demande';
            DB::table('events_users')->where('user_id','=',$notification->creator_id)
                ->where('event_id','=',$notification->event_id)->update(['accepted' => 2]);
            $newNotification = Notification::create([
                'notifiable_id' => $nstudent->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 0
            ]);
            //add message
            if($request->has('message') && $request['message']!=null && $notification && $newNotification)
            {
                // show to participant
                $msd_desc = $request->get('message');
                $message = new Messages();
                $message->notification_id = $notification->id;
                $message->message_from = Auth()->user()->id;
                $message->message_to = $nstudent->id;
                $message->message_type = 'decline';
                $message->description = $msd_desc;
                $message->save();
                // show to owner
                $message = new Messages();
                $message->notification_id = $newNotification->id;
                $message->message_from = Auth()->user()->id;
                $message->message_to = Auth()->user()->id;
                $message->message_type = 'decline';
                $message->description = $msd_desc;
                $message->save();
            }
            $description_to_students = Auth()->user()->first_name.' '.Auth()->user()->last_name.' a décliné la participation de '.$nstudent->first_name.' '.$nstudent->last_name.' à ce cours pour le '.date('d/m/Y', strtotime($event->dhstart)).' à '.date('H', strtotime($event->dhstart)).'h';
            $students = $event_query->whereIn('user_type',[0,4])->where('accepted','=',1)->get();
            $teacher = $event_query->where('user_type','=',1)->where('accepted','=',1)->get();

            if(count($students))
            {
                foreach ($students as $student)
                {
                    if($student->user_id != $event->owner)
                    {
                        Notification::create([
                            'notifiable_id' => $student->user_id,
                            'creator_id' => Auth()->user()->id,
                            'event_id' => $event->id,
                            'description' => $description_to_students,
                            'readit' => 0,
                            'showoption' => 0
                        ]);
                    }
                }
            }
            if(count($teacher))
            {
                if($teacher->user_id != $event->owner)
                {
                    Notification::create([
                        'notifiable_id' => $teacher->user_id,
                        'creator_id' => Auth()->user()->id,
                        'event_id' => $event->id,
                        'description' => $description_to_students,
                        'readit' => 0,
                        'showoption' => 0
                    ]);
                }
            }
            return ['status' => 'success'];
        }
        catch(Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function suitEvent(Request $request)
    {
        $dateStart = DateTime::createFromFormat('d/m/Y H', $request->get('suitDhstart'));
        $usableDateStart = $dateStart->format('Y-m-d H:i:s');
        $dateEnd = DateTime::createFromFormat('d/m/Y H', $request->get('suitDhEnd'));
        $usableDateEnd = $dateEnd->format('Y-m-d H:i:s');
        $event = Event::find($request->get('suitid'));
        $enrolledUsers = DB::table('events_users')->where('event_id',$event->id)->where('accepted','=',1)->get();
        $suitPrivate = 0;
        if($request->has('suitPrivate') && $request->get('suitPrivate')=='suitPrivate') $suitPrivate = 1;
        $descrPrivate = '';
        try{
            SuitMe::create([
                'event_id' => $event->id,
                'asked_by' => Auth()->user()->id,
                'dhstart' => $usableDateStart,
                'dhend' => $usableDateEnd,
                'private' => $suitPrivate,
                'validation_date' => null
            ]);
            
            if(Auth()->user()->id==$event->owner) $selfDescription = "Ce cours n'est plus valide. Vous l'avez déplacé au ".date('d/m/Y', strtotime($usableDateStart))." de ".date('H', strtotime($usableDateStart))."h à ".date('H', strtotime($usableDateEnd))."h";
            else{
                if($suitPrivate==1) $descrPrivate = ' et à en faire un cours particulier';
                $selfDescription = "Vous avez demandé à déplacer le cours".$descrPrivate.". Vous n'êtes désormais plus inscrit(e) à ce cours.";
            }
            $notification = Notification::create([
                'notifiable_id' => Auth()->user()->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $selfDescription,
                'readit' => 0,
                'showoption' => 0
            ]);

            if($event->owner == Auth()->user()->id)
            {
                $event->invalide = 2;
                $event->deal =  0;
                $event->save();

                $newEvent = Event::create([
                    'dhstart' => $usableDateStart,
                    'dhend' => $usableDateEnd,
                    'duration' => $request->get('suitHoursReq'),
                    'level_id' => $event->level_id,
                    'course_id'=> $event->course_id,
                    'descr' => $event->descr,
                    'owner' => $event->owner,
                    'student_home' => $event->student_home,
                    'teacher_home' => $event->teacher_home,
                    'private' => $event->private,
                    'deal' => 0,
                    'invalide' => 0
                ]);

                SuitMe::where('event_id',$event->id)->where('asked_by',Auth()->user()->id)->where('dhstart',$usableDateStart)->where('dhend',$usableDateEnd)->update(['validation_date' => Carbon::now()]);
                if(count($enrolledUsers)){
                    foreach ($enrolledUsers as $participant)
                    {
                        //decline all participants for the current event
                        DB::table('events_users')->where('event_id',$event->id)->where('user_id',$participant->user_id)->update(['accepted'=>2]);
                        if($request->has('suitGetConfirmed') && $request->get('suitGetConfirmed')=='suitGetConfirmed')
                        {
                            // pre-enroll all confirmed participants with a new status
                            // accepted == 3
                            // participant must reconfirm the new event
                            DB::table('events_users')->insert([
                                'event_id' => $newEvent->id,
                                'user_id' => $participant->user_id,
                                'user_type' => $participant->type,
                                'accepted' => 3,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now()
                            ]);
                            $notification = Notification::create([
                                'notifiable_id' => $participant->user_id,
                                'creator_id' => Auth()->user()->id,
                                'event_id' => $event->id,
                                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' a déplacé le cours au '.date('d/m/Y', strtotime($usableDateStart)).' de '.date('H', strtotime($usableDateStart)).'h à '.date('H', strtotime($usableDateEnd)).'h et a souhaité vous garder confirmé pour ce cours.',
                                'readit' => 0,
                                'showoption' => 0,
                                'duplicated_event_id' => $newEvent->id
                            ]);
                            Notification::create([
                                'notifiable_id' => $participant->user_id,
                                'creator_id' => Auth::user()->id,
                                'event_id' => $newEvent->id,
                                'description' => 'Cours déplacé par '.Auth()->user()->first_name.' '.Auth()->user()->last_name.'.',
                                'readit' => 0,
                                'showoption' => 4
                            ]);
                        }
                        else
                        {
                            $notification = Notification::create([
                                'notifiable_id' => $participant->user_id,
                                'creator_id' => Auth()->user()->id,
                                'event_id' => $event->id,
                                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' a déplacé le cours au '.date('d/m/Y', strtotime($usableDateStart)).' de '.date('H', strtotime($usableDateStart)).'h à '.date('H', strtotime($usableDateEnd)).'h',
                                'readit' => 0,
                                'showoption' => 3,
                                'duplicated_event_id' => $newEvent->id
                            ]);
                        }
                        
                        if($request->has('suitReason') && $request['suitReason']!=null && $notification)
                        {
                            $msd_desc = $request->get('suitReason');
                            $message = new Messages();
                            $message->notification_id = $notification->id;
                            $message->message_from = Auth()->user()->id;
                            $message->message_to = $participant->user_id;
                            $message->message_type = 'suit_me';
                            $message->description = $msd_desc;
                            $message->save();
                        }
                    }
                }
            }
            else{
                // current user is not enrolled anymore
                DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',Auth()->user()->id)->update(['accepted'=>2]);

                // notify the owner
                if($suitPrivate==1) $descrPrivate = ' et en faire un cours particulier';
                $notification = Notification::create([
                    'notifiable_id' => $event->owner,
                    'creator_id' => Auth()->user()->id,
                    'event_id' => $event->id,
                    'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' souhaite déplacer le cours au '.date('d/m/Y', strtotime($usableDateStart)).' de '.date('H', strtotime($usableDateStart)).'h à '.date('H', strtotime($usableDateEnd)).'h'.$descrPrivate,
                    'readit' => 0,
                    'showoption' => 2
                ]);
                
                if($request->has('suitReason') && $request['suitReason']!=null && $notification)
                {
                    $msd_desc = $request->get('suitReason');
                    $message = new Messages();
                    $message->notification_id = $notification->id;
                    $message->message_from = Auth()->user()->id;
                    $message->message_to = $event->owner;
                    $message->message_type = 'suit_me';
                    $message->description = $msd_desc;
                    $message->save();
                }

                // notify the other participants
                if(count($enrolledUsers)){
                    foreach ($enrolledUsers as $participant)
                    {
                        if($participant->user_id!=Auth()->user()->id){
                            $notification = Notification::create([
                                'notifiable_id' => $participant->user_id,
                                'creator_id' => Auth()->user()->id,
                                'event_id' => $event->id,
                                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' n\'est plus inscrit.',
                                'readit' => 0,
                                'showoption' => 0
                            ]);
                            
                            if($request->has('suitReason') && $request['suitReason']!=null && $notification)
                            {
                                $msd_desc = $request->get('suitReason');
                                $message = new Messages();
                                $message->notification_id = $notification->id;
                                $message->message_from = Auth()->user()->id;
                                $message->message_to = $participant->user_id;
                                $message->message_type = 'suit_me';
                                $message->description = $msd_desc;
                                $message->save();
                            }
                        }
                    }
                }
            }

            return redirect()->back();
        }
        catch(Exception $e)
        {
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    public function possibleEvent($id)
    {
        $notification = Notification::find($id)->first();
        $nstudent = User::find($notification->creator_id)->first();
        $event = Event::find($notification->event_id)->first();

        try
        {
            $myEvents = Event::where('owner',Auth()->user()->id)->first();
            $participatedEvents = DB::table('events_users')->where('events_users.user_id','=',Auth()->user()->id)
                ->where('events_users.accepted','=',1)
                ->leftJoin('events','events.id','=','events_users.event_id')
                ->get(['events.*']);
            $allEvents = $myEvents->merge($participatedEvents);

            $event_query = DB::table('events_users')->where('event_id','=',$notification->event_id);
            $event_user = $event_query->where('user_id','=',$notification->creator_id)->first();
            $students = $event_query->whereIn('user_type',[0,4])->where('accepted','=',1)->get();
            $startDate = $event_user->suit_me_start;
            $endDate = $event_user->suit_me_end;
            $chkStart = new DateTime($startDate);
            $chkEnd = new DateTime($endDate);
            foreach ($allEvents as $eachEvent)
            {
                $dhstart = new DateTime($eachEvent->dhstart);
                $dhend = new DateTime($eachEvent->dhend);
                if ($chkStart->getTimestamp() >= $dhstart->getTimestamp() &&
                    $chkStart->getTimestamp() <= $dhend->getTimestamp()){
                    return ['status' => 'failed','message' => 'Vous avez déjà un autre cours prévu au même moment'];
                }elseif($chkEnd->getTimestamp() >= $dhstart->getTimestamp() &&
                    $chkEnd->getTimestamp() <= $dhend->getTimestamp()){
                    return ['status' => 'failed','message' => 'Vous avez déjà un autre cours prévu au même moment'];
                }
            }
            $description = Auth()->user()->first_name.' '.Auth()->user()->last_name.' a déplacé le cours suite à votre demande au '.date('d/m/Y', strtotime($startDate)).' de '.date('H', strtotime($startDate)).'h à '.date('H', strtotime($endDate)).'h';
            $description_to_students = Auth()->user()->first_name.' '.Auth()->user()->last_name.' a déplacé le cours au '.date('d/m/Y', strtotime($startDate)).' de '.date('H', strtotime($startDate)).'h à '.date('H', strtotime($endDate)).'h';
            $duration = (new DateTime($endDate))->diff(new DateTime($startDate))->h;
            /*DB::table('events')->where('id','=',$notification->event_id)->update([
                'dhstart' => $startDate,
                'dhend' => $endDate,
                'duration' => $duration
            ]);*/

            $event->where('id','=',$notification->event_id)->update([
                'invalide' => 1
            ]);
            Event::create([
                'dhstart' => $startDate,
                'dhend' => $endDate,
                'duration' => $duration,
                'level_id' => $event->level_id,
                'course_id' => $event->course_id,
                'descr' => $event->descr,
                'owner' => $event->owner,
                'student_home' => $event->student_home,
                'teacher_home' => $event->teacher_home,
                'private' => $event->private,
                'deal' => 0,
                'invalide' => 0
            ]);
            /*DB::table('events_users')
                ->where('event_id','=',$notification->event_id)
                ->where('user_id','=',$notification->creator_id)
                ->update(['changed' => 1]);*/
            Notification::create([
                'notifiable_id' => $nstudent->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => $description,
                'readit' => 0,
                'showoption' => 0
            ]);
            if(count($students))
            {
                foreach ($students as $student)
                {
                    if($student->user_id != $event->owner)
                    {
                        Notification::create([
                            'notifiable_id' => $student->user_id,
                            'creator_id' => Auth()->user()->id,
                            'event_id' => $event->id,
                            'description' => $description_to_students,
                            'readit' => 0,
                            'showoption' => 0
                        ]);
                    }
                }
            }
            return ['status' => 'success'];
        }
        catch(Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function notPossibleEvent($id)
    {
        $notification = Notification::find($id);
        $creator = User::find($notification->creator_id);
        $event = Event::find($notification->event_id);
        try
        {
            DB::table('events_users')->where('user_id','=',$notification->creator_id)->where('event_id','=',$notification->event_id)->update(['changed' => 2]);
            Notification::create([
                'notifiable_id' => $creator->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' a décliné votre demande',
                'readit' => 0,
                'showoption' => 0
            ]);
            Notification::create([
                'notifiable_id' => Auth()->user()->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => 'Vous avez décliné la demande de '.$creator->first_name.' '.$creator->last_name,
                'readit' => 0,
                'showoption' => 0
            ]);
            return ['status' => 'success'];
        }
        catch(Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function duplicateEvent($id)
    {
        $notification = Notification::find($id);
        $creator = User::find($notification->creator_id);
        $event = Event::find($notification->event_id);
        $suit_me = SuitMe::where('event_id',$event->id)->where('asked_by',$creator->id)->get()->first();
        $current_Events_Users = DB::table('events_users')->where('event_id','=',$event->id)->where('user_id','=',$creator->id)->get()->first();

        // remove options for the current notification
        $notification->showoption = 0;
        $notification->save();
        
        // validate the demand
        $suit_me->update(['validation_date' => Carbon::now()]);

        // duplicate
        $newEvent = Event::create([
            'dhstart' => $suit_me->dhstart,
            'dhend' => $suit_me->dhend,
            'duration' => $event->duration,
            'level_id' => $event->level_id,
            'course_id' => $event->course_id,
            'descr' => $event->descr,
            'owner' => $event->owner,
            'student_home' => $event->student_home,
            'teacher_home' => $event->teacher_home,
            'private' => $suit_me->private,
            'deal' => 0,
            'invalide' => 0
        ]);

        // enrol the creator of the demand in the new event
        DB::table('events_users')->insert([
            'event_id' => $newEvent->id,
            'user_id' => $creator->id,
            'user_type' => $creator->type,
            'accepted' => 1,
            'mobility' => $current_Events_Users->mobility
        ]);

        // notify the creator of the demand
        $descrPrivate = '';
        if($suit_me->private == 1) $descrPrivate = ' particulier';
        Notification::create([
            'notifiable_id' => $creator->id,
            'creator_id' => Auth()->user()->id,
            'event_id' => $event->id,
            'description' => 'Suite à votre demande, '.Auth()->user()->first_name.' '.Auth()->user()->last_name.' a programmé un nouveau cours'.$descrPrivate.' au '.date('d/m/Y', strtotime($suit_me->dhstart)).' de '.date('H', strtotime($suit_me->dhstart)).'h à '.date('H', strtotime($suit_me->dhend)).'h.',
            'readit' => 0,
            'showoption' => 0
        ]);

        Notification::create([
            'notifiable_id' => Auth()->user()->id,
            'creator_id' => Auth()->user()->id,
            'event_id' => $event->id,
            'description' => 'Suite à la demande de '.$creator->first_name.' '.$creator->last_name.', vous avez programmé un nouveau cours'.$descrPrivate.' au '.date('d/m/Y', strtotime($suit_me->dhstart)).' de '.date('H', strtotime($suit_me->dhstart)).'h à '.date('H', strtotime($suit_me->dhend)).'h.',
            'readit' => 0,
            'showoption' => 0
        ]);

        Notification::create([
            'notifiable_id' => $creator->id,
            'creator_id' => Auth()->user()->id,
            'event_id' => $newEvent->id,
            'description' => 'Vous avez été confirmé(e) d\'office',
            'readit' => 0,
            'showoption' => 0
        ]);

        return ['status' => 'success'];
    }
    
    public function confirmNewEvent($id,Request $request)
    {
        try
        {
            $currNotification = Notification::find($id);
            $userToNotify = User::find($currNotification->creator_id);
            $event = Event::find($currNotification->event_id);
            $duplicatedEvent = Event::find($currNotification->duplicated_event_id);

            //update options of current notification
            $currNotification->showoption = 0;
            $currNotification->save();

            //update previous notification if the user wasn't accepted yet
            $notificationToUpdate = Notification::where('event_id','=',$event->id)->where('notifiable_id','=',$userToNotify->id)->where('creator_id','=',Auth()->user()->id)->where('showoption','=',1)->get()->first();
            if($notificationToUpdate){
                $notificationToUpdate->showoption = 0;
                $notificationToUpdate->save();
            }

            //checking if the user has already proposed to the new event
            $exists = DB::table('events_users')->where('event_id','=',$duplicatedEvent->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',0)->first();
            if($exists)
            {
                DB::table('events_users')->where('event_id','=',$duplicatedEvent->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',0)->update(['accepted' => 0]);
            }
            else
            {
                $mobility = null;
                if($request->has('mobility')) $mobility = $request->get('mobility');
                else{
                    if($duplicatedEvent->student_home == 1) $mobility = 0;
                    if($duplicatedEvent->teacher_home == 1) $mobility = 1;
                }

                DB::insert('insert into events_users (event_id, user_id, user_type, mobility) values (?, ?, ?, ?)', [$duplicatedEvent->id, Auth()->user()->id, 0, $mobility]);

                $notification = Notification::create([
                    'notifiable_id' => $duplicatedEvent->owner,
                    'creator_id' => Auth()->user()->id,
                    'event_id' => $duplicatedEvent->id,
                    'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' souhaite participer à ce cours',
                    'readit' => 0,
                    'showoption' => 1
                ]);
            }
            
            //add message
            if($request->has('message') && $request['message']!=null && $notification)
            {
                $msd_desc = $request->get('message');
                $message = new Messages();
                $message->notification_id = $notification->id;
                $message->message_from = Auth()->user()->id;
                $message->message_to = $duplicatedEvent->owner;
                $message->message_type = 'participate';
                $message->description = $msd_desc;
                $message->save();
            }

            return ['status' => 'success'];
        }
        catch(Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function proposeNewEvent($id, Request $request)
    {
        try
        {
            $currNotification = Notification::find($id);
            $userToNotify = User::find($currNotification->creator_id);
            $event = Event::find($currNotification->event_id);
            $duplicatedEvent = Event::find($currNotification->duplicated_event_id);

            //update options of current notification
            $currNotification->showoption = 0;
            $currNotification->save();
        
            //update previous notification if the user wasn't accepted yet
            $notificationToUpdate = Notification::where('event_id','=',$event->id)->where('notifiable_id','=',$userToNotify->id)->where('creator_id','=',Auth()->user()->id)->where('showoption','=',1)->get()->first();
            if($notificationToUpdate){
                $notificationToUpdate->showoption = 0;
                $notificationToUpdate->save();
            }

            //checking if the user has already proposed to the new event
            $exists = DB::table('events_users')->where('event_id','=',$duplicatedEvent->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',1)->first();
            if($exists)
            {
                DB::table('events_users')->where('event_id','=',$duplicatedEvent->id)->where('user_id','=',Auth()->user()->id)->where('user_type','=',1)->update(['accepted' => 0]);
            }
            else
            {
                $mobility = null;
                if($request->has('mobility')) $mobility = $request->get('mobility');
                else{
                    if($duplicatedEvent->student_home == 1) $mobility = 0;
                    if($duplicatedEvent->teacher_home == 1) $mobility = 1;
                }
                DB::insert('insert into events_users (event_id, user_id, user_type, mobility) values (?, ?, ?, ?)', [$duplicatedEvent->id, Auth()->user()->id, 1, $mobility]);

                $notification = Notification::create([
                    'notifiable_id' => $duplicatedEvent->owner,
                    'creator_id' => Auth()->user()->id,
                    'event_id' => $duplicatedEvent->id,
                    'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' s\'est proposé(e) pour ce cours',
                    'readit' => 0,
                    'showoption' => 1
                ]);
            }
            //add message
            if($request->has('message') && $request['message']!=null && $notification)
            {
                $msd_desc = $request->get('message');
                $message = new Messages();
                $message->notification_id = $notification->id;
                $message->message_from = Auth()->user()->id;
                $message->message_to = $event->owner;
                $message->message_type = 'propose';
                $message->description = $msd_desc;
                $message->save();
            }

            return ['status' => 'success'];
        }
        catch(Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function acceptNewEvent($id)
    {
        $currentNotification = Notification::find($id);
        $userToNotify = User::find($currentNotification->creator_id);
        $event = Event::find($currentNotification->event_id);
        $event_query = DB::table('events_users')->where('event_id','=',$currentNotification->event_id);

        try
        {
            $currentNotification->showoption = 0;
            $currentNotification->save();
            Notification::create([
                'notifiable_id' => Auth()->user()->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => 'Vous avez accepté de maintenir votre participation',
                'readit' => 0,
                'showoption' => 0
            ]);
            Notification::create([
                'notifiable_id' => $userToNotify->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' a accepté de maintenir sa participation',
                'readit' => 0,
                'showoption' => 0
            ]);

            return ['status' => 'success'];
        }
        catch (Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function declineNewEvent($id)
    {
        $currentNotification = Notification::find($id);
        $userToNotify = User::find($currentNotification->creator_id);
        $event = Event::find($currentNotification->event_id);
        $event_query = DB::table('events_users')->where('event_id','=',$currentNotification->event_id);

        try
        {
            $currentNotification->showoption = 0;
            $currentNotification->save();
            Notification::create([
                'notifiable_id' => Auth()->user()->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => 'Vous avez refusé de maintenir votre participation',
                'readit' => 0,
                'showoption' => 0
            ]);
            Notification::create([
                'notifiable_id' => $userToNotify->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' a refusé de maintenir sa participation',
                'readit' => 0,
                'showoption' => 0
            ]);
            $event_query->update(['accepted'=>2]);

            return ['status' => 'success'];
        }
        catch (Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function reconfirmNewEvent($id)
    {
        $currentNotification = Notification::find($id);
        $userToNotify = User::find($currentNotification->creator_id);
        $event = Event::find($currentNotification->event_id);
        $event_query = DB::table('events_users')->where('event_id','=',$currentNotification->event_id)->where('user_id','=',Auth::user()->id);
        try
        {
            $currentNotification->showoption = 0;
            $currentNotification->save();
            Notification::create([
                'notifiable_id' => Auth()->user()->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => 'Vous avez confirmé votre participation',
                'readit' => 0,
                'showoption' => 0
            ]);
            Notification::create([
                'notifiable_id' => $userToNotify->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' a confirmé sa participation',
                'readit' => 0,
                'showoption' => 0
            ]);
            $event_query->update(['accepted'=>1]);
            $event->update(['deal'=>1]);
            return ['status' => 'success'];
        }
        catch (Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function redeclineNewEvent($id)
    {
        $currentNotification = Notification::find($id);
        $userToNotify = User::find($currentNotification->creator_id);
        $event = Event::find($currentNotification->event_id);
        $event_query = DB::table('events_users')->where('event_id','=',$currentNotification->event_id)->where('user_id','=',Auth::user()->id);
        try
        {
            $currentNotification->showoption = 0;
            $currentNotification->save();
            Notification::create([
                'notifiable_id' => Auth()->user()->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => 'Vous avez décliné votre participation',
                'readit' => 0,
                'showoption' => 0
            ]);
            Notification::create([
                'notifiable_id' => $userToNotify->id,
                'creator_id' => Auth()->user()->id,
                'event_id' => $event->id,
                'description' => Auth()->user()->first_name.' '.Auth()->user()->last_name.' a décliné sa participation',
                'readit' => 0,
                'showoption' => 0
            ]);
            $event_query->update(['accepted'=>2]);

            return ['status' => 'success'];
        }
        catch (Exception $e)
        {
            return ['status' => 'failed','message' =>$e->getMessage()];
        }
    }

    public function moreInfo($id, Request $request)
    {
        $event = Event::find($id);
        try {
            $notification = Notification::create([
                'notifiable_id' => $event->owner,
                'creator_id' => Auth::user()->id,
                'event_id' => $event->id,
                'description' => Auth::user()->first_name.' '.Auth::user()->last_name.' souhaite avoir plus d\'informations.',
                'readit' => 0,
                'showoption' => 0
            ]);
            if($request->has('message') && $request['message']!=null && $notification)
            {
                $msd_desc = $request->get('message');
                $message = new Messages();
                $message->notification_id = $notification->id;
                $message->message_from = Auth::user()->id;
                $message->message_to = $event->owner;
                $message->message_type = 'moreInfo';
                $message->description = $msd_desc;
                $message->save();
            }
            return ['status' => 'success'];
        } catch (Exception $e) {
            return ['status' => 'error'];
        }
    }

    public function rating(Request $request)
    {
        try {
            Rating::create([
                'event_id' => $request->get('event_id'),
                'opinion' => $request->get('opinion'),
                'rate' => $request->get('rate'),
                'rated_user_id' => $request->get('teacher_id'),
                'rating_user_id' => $request->get('student_id'),
                'comment' => $request->get('comment')
            ]);

            return $request;
        } catch (Exception $e) {
            return ['status' => 'error'];
        }
    }
}