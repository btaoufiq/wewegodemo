<?php

namespace WEWEGO\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminController extends Controller
{
    public function __construct()
	{
		$this->middleware('admin');
	}

	public function index()
	{
		$levels = DB::table('levels')->get();
		$courses = DB::table('courses')->get();
		return view('admin', compact('levels', 'courses'));
	}
}
