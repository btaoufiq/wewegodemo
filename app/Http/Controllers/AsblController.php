<?php

namespace WEWEGO\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use WEWEGO\User;
use Auth;
use DB;
use DateTime;
use WEWEGO\Event;
use WEWEGO\Notification;
use WEWEGO\Messages;

class AsblController extends Controller
{
    public function __construct()
	{
		$this->middleware('asbl');
	}

	public function index()
	{
		$user = Auth::user();
		$events = DB::table('events')->join('levels', 'events.level_id', '=', 'levels.id')
			->leftjoin('courses', 'events.course_id', '=', 'courses.id')
			->leftjoin('users','events.owner','=','users.id')
			->leftjoin('localities','users.localite','=','localities.code')
			->where('events.invalide','!=','1')
			->where('events.owner', '=', $user->id)
			->where('events.type','=','2')
			->get(array('events.*', 'localities.label_fr', 'localities.code', 'courses.color', 'courses.label_fr as course_label', 'levels.label_fr as level_label'));
        $unreadNotifications = Notification::where('notifiable_id','=',$user->id)->where('readit','=',0);
		$unreadMessages = $unreadNotifications->with('messages')->get();
		$countNotifications = count($unreadNotifications);
		$allNotifications = Notification::where('notifiable_id','=',$user->id);
        $allMessages = $allNotifications->with('messages')->get();
        $levels = DB::table('levels')->get();
        $courses = DB::table('courses')->get();

		return view('asbl',compact('user','levels','courses','events','allMessages'));
	}

	protected function validator(array $data)
	{
		return Validator::make($data, [
			'last_name' => 'required|string|max:255',
			'first_name' => 'required|string|max:255',
			'email' => 'required|string|email|max:255|unique:users',
			'localite' => 'string|max:255',
			'phone' => 'string|max:50',
			'level' => 'required',
			'course' => 'required',
		]);
	}

	public function createTeacher(Request $request)
	{
		$user = User::create([
			'last_name' => $request['last_name'],
			'first_name' => $request['first_name'],
			'email' => $request['email'],
			'type' => '1',
			'localite' => $request['localite'],
			'phone' => $request['phone'],
			'password' => bcrypt($request['email']),
		]);

		$id= $user->id;
		if(isset($request['level']))
		{
			$levelToInsert = array(0, 0, 0);
			foreach ($request['level'] as $l) {
				if($l == 1) $levelToInsert[0] = 1;
				if($l == 2) $levelToInsert[1] = 1;
				if($l == 3) $levelToInsert[2] = 1;
			}
			DB::insert('insert into user_level (user_id, primaire, sec_inf, sec_sup) values (?, ?, ?, ?)', [$id, $levelToInsert[0], $levelToInsert[1], $levelToInsert[2]]);
		}
		if(isset($request['course']))
		{
			$courseToInsert = array(0, 0, 0, 0, 0, 0, 0, 0);
			foreach ($request['course'] as $c) {
				if($c == 1) $courseToInsert[0] = 1;
				if($c == 2) $courseToInsert[1] = 1;
				if($c == 3) $courseToInsert[2] = 1;
				if($c == 4) $courseToInsert[3] = 1;
				if($c == 5) $courseToInsert[4] = 1;
				if($c == 6) $courseToInsert[5] = 1;
				if($c == 7) $courseToInsert[6] = 1;
				if($c == 8) $courseToInsert[7] = 1;
			}
			DB::insert('insert into user_course (user_id, edm, math, francais, neerlandais, anglais, chimie, biologie, physique) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', [$id, $courseToInsert[0], $courseToInsert[1], $courseToInsert[2], $courseToInsert[3], $courseToInsert[4], $courseToInsert[5], $courseToInsert[6], $courseToInsert[7]]);
		}
		return back()->with('message', 'Un professeur a été créé !');
	}

	public function updateTeacher(Request $request)
	{
		$id = $request['teacherId'];
		DB::table('users')->where('id','=',$id)->update(['first_name'=>$request['teacherFirstName'], 'last_name'=>$request['teacherLastName'], 'email'=>$request['teacherEmail'], 'localite'=>$request['teacherLocalite'], 'phone'=>$request['teacherPhone']]);
		return back()->with('message', 'Le professeur a été modifié.');
	}

	public function createEvent(Request $request)
	{
		if($_POST['level'] == 1) $course = 0;
        else $course = $request['course'];
        $dateStart = DateTime::createFromFormat('d/m/Y H', $request['dhstart']);
        $usableDateStart = $dateStart->format('Y-m-d H:i:s');
        $dateEnd = DateTime::createFromFormat('d/m/Y H', $request['dhend']);
        $usableDateEnd = $dateEnd->format('Y-m-d H:i:s');
        $hours = $dateStart->diff($dateEnd)->h;
        Event::create([
            'dhstart' => $usableDateStart,
            'dhend' => $usableDateEnd,
            'duration' => $hours,
            'level_id' => $request['level'],
            'course_id' => $course,
            'descr' => $request['descr'],
            'owner' => Auth::user()->id,
            'address' => $request['address'],
            'type' => 2,
            'deal' => 0,
            'invalide' => 0
        ]);
        return redirect()->back();
	}

	public function getSingleEvent($id)
	{
		$event = Event::find($id);
		$course_label = DB::table('courses')->where('id','=',$event->course_id)->get(['label_fr'])->first();
		$level_label = DB::table('levels')->where('id','=',$event->level_id)->get(['label_fr'])->first();
		return ['event'=>$event,'course_label'=>$course_label,'level_label'=>$level_label];
	}
}
