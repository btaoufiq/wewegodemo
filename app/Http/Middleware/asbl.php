<?php

namespace WEWEGO\Http\Middleware;

use Closure;
use Auth;

class asbl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check() and Auth::user()->CheckRole()!='asbl') {
            return redirect('/');
        }

        return $next($request);
    }
}
