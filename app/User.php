<?php

namespace WEWEGO;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use WEWEGO\Rating;
use DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'last_name', 'first_name', 'email', 'type', 'localite', 'phone', 'password', 'sex', 'school_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function CheckRole()
    {
        $role = 'standard';
        if($this->type=='2') $role = 'asbl';
        if($this->type=='3') $role = 'admin';
        return $role;
    }

    public function getLevel()
    {
        $user_level = DB::table('user_level')->where('user_id',$this->id)->first();
        $array = [];
        if($user_level->primaire == 1) $array[1] = 'Primaire';
        if($user_level->sec_inf == 1) $array[2] = 'Secondaire inférieur';
        if($user_level->sec_sup == 1) $array[3] = 'Secondaire supérieur';
        
        return $array;
    }

    public function getCourse()
    {
        $user_course = DB::table('user_course')->where('user_id',$this->id)->first();
        $array = [];
        if($user_course->edm == 1) $array[1] = 'Etude du milieu';
        if($user_course->math == 1) $array[2] = 'Math';
        if($user_course->francais == 1) $array[3] = 'Français';
        if($user_course->neerlandais == 1) $array[4] = 'Néerlandais';
        if($user_course->anglais == 1) $array[5] = 'Anglais';
        if($user_course->chimie == 1) $array[6] = 'Chimie';
        if($user_course->biologie == 1) $array[7] = 'Biologie';
        if($user_course->physique == 1) $array[8] = 'Physique';

        return $array;
    }

    public function userAverageRating()    
    {
        return number_format(Rating::where('rated_user_id',$this->id)->avg('rate'),1);
    }

    public function userSumRating()    
    {
        return number_format(Rating::where('rated_user_id',$this->id)->sum('rate'),1);
    }

    public function userMaxRating()    
    {
        return number_format(Rating::where('rated_user_id',$this->id)->max('rate'),1);
    }

    public function userMinRating()    
    {
        return number_format(Rating::where('rated_user_id',$this->id)->min('rate'),1);
    }

    public function userCountRating()    
    {
        return Rating::where('rated_user_id',$this->id)->count();
    }

}
