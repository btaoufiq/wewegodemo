<?php

namespace WEWEGO;

use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'sex',
        'school_id',
        'level',
        'parent'
    ];
}
