<?php

namespace WEWEGO;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
    	'dhstart',
    	'dhend',
    	'duration',
    	'level_id',
    	'course_id',
    	'descr',
    	'owner',
        'child',
    	'student_home',
    	'teacher_home',
    	'private',
        'type',
    	'deal',
        'address',
    	'invalide'
    ];

}
