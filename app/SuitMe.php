<?php

namespace WEWEGO;

use Illuminate\Database\Eloquent\Model;

class SuitMe extends Model
{
	protected $table = 'suit_me';
    protected $fillable = [
    	'event_id',
    	'asked_by',
    	'dhstart',
    	'dhend',
    	'private',
    	'validation_date'
    ];
}
