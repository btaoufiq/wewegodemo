<?php

namespace WEWEGO;

use Illuminate\Database\Eloquent\Model;


class Messages extends Model
{

    protected  $table = 'messages';

    protected $fillable = [
    	'norification_id',
    	'message_from',
    	'message_to',
    	'message_type',
    	'description'
    ];


    public function sender()
    {
        return $this->hasOne(User::class,'id','message_from');
    }

    public function receiver()
    {
        return $this->hasOne(User::class,'id','message_to');
    }
}
