<?php

namespace WEWEGO;

use Illuminate\Database\Eloquent\Model;


class Notification extends Model
{

    //protected  $table = 'notifications';

    protected $fillable = [
    	'notifiable_id',
    	'creator_id',
    	'event_id',
    	'description',
    	'readit',
    	'showoption',
    	'general',
        'duplicated_event_id'
    ];

    public function messages()
    {
        return $this->hasMany(Messages::class,'notification_id');
    }
}
