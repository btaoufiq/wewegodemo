function editModal_actionButtonsDisplay(event_id, event_owner, event_user_type, event_enrolled, event_enrolled_user_id, 
    event_enrolled_status, event_private, arrayTeachers, arrayStudents, user_id, user_type, event_type, event_child)
{
    if(event_owner==user_id) // current user is the owner
    {
        $('#ModalEdit #editDatepicker').attr('disabled', false);
        $('#ModalEdit #editHours').attr('disabled',false);
        $('#ModalEdit #editTimepickerStart').attr('disabled',false);
        $('#ModalEdit #submitEditEvent').css("display","");
        $('#ModalEdit #deleteEditEvent').attr('data-id',event_id);
        $('#ModalEdit #deleteEditEvent').css("display","");
        $('#ModalEdit #messagetext').css("display","").text('Vous êtes le propriétaire de ce cours');
    }
    else{
        switch(user_type)
        {
            case 1 : // current user is a teacher
                if(event_user_type==1) // the event belongs to another teacher
                {
                    $('#ModalEdit #proposeEditEvent').css('display','').attr('disabled',true);
                    $('#ModalEdit #messagetext').css('display','').text('Vous ne pouvez pas vous proposer à ce cours');
                }
                else // the event belongs to a student
                {
                    if(event_type==1 || event_type==2){
                        $('#ModalEdit #proposeEditEvent').css('display','').attr('disabled',true);
                        $('#ModalEdit #messagetext').css('display','').text('Vous ne pouvez pas vous proposer à ce cours');
                    }
                    else if(event_enrolled!='' && event_enrolled_user_id==user_id){ // current teacher is enrolled
                        $('#ModalEdit #messagetext').css('display','').text('');
                        if(event_enrolled_status == '2'){ // current teacher has been declined
                            $('#ModalEdit #messagetext').text('Vous avez été décliné');
                            $('#ModalEdit #proposeEditEvent').css('display','').attr('data-id',event_id);
                        }
                        else{
                            $('#ModalEdit #withdrawEditEvent').css('display','').attr('data-id',event_id);
                            $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event_id).attr('disabled',false);
                            switch(event_enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                case '0' : 
                                    $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                    $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                    break;
                                default : $('#ModalEdit #messagetext').text(''); break;
                            }
                        }
                    }
                    else{
                        if(arrayTeachers==1){
                            $('#ModalEdit #proposeEditEvent').css('display','').attr('disabled',true);
                            $('#ModalEdit #messagetext').css('display','').text('Vous ne pouvez pas vous proposer à ce cours');
                        }
                        else $('#ModalEdit #proposeEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                    }
                }
                break;
            case 0 : // current user is a student
                if(event_type==2){ // asbl event
                    $('#ModalEdit #moreInfoEditEvent').css('display','').attr('data-id',event_id);
                }
                else{
                    if(event_user_type==1) // the event belongs to a teacher
                    {
                        switch(event_private)
                        {
                            case '1' : // private event : student_count = 1
                                if(event_enrolled!=''){ // event has enrolled student
                                    if(event_enrolled_user_id==user_id){ // current student is enrolled
                                        $('#ModalEdit #messagetext').css('display','').text('');
                                        if(event_enrolled_status == '2'){ // current student has been declined
                                            $('#ModalEdit #messagetext').text('Vous avez été décliné');
                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('data-id',event_id);
                                        }
                                        else{
                                            $('#ModalEdit #unsubscribeEditEvent').css('display','').attr('data-id',event_id);
                                            $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event_id).attr('disabled',false);
                                            switch(event_enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                                case '0' : 
                                                    $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                                    $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                                    break;
                                                default : $('#ModalEdit #messagetext').text(''); break;
                                            }
                                        }
                                    }
                                    else{
                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                        $('#ModalEdit #messagetext').text('Ce cours est complet');
                                    }
                                }
                                else{
                                    $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                                }
                                break;
                            case '' : // not a private event : student_count up to 3
                                if(event_enrolled!='' && event_enrolled_user_id==user_id){ // current student is enrolled
                                    $('#ModalEdit #messagetext').css('display','').text('');
                                    if(event_enrolled_status == '2'){ // current student has been declined
                                        $('#ModalEdit #messagetext').text('Vous avez été décliné');
                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('data-id',event_id);
                                    }
                                    else{
                                        $('#ModalEdit #unsubscribeEditEvent').css('display','').attr('data-id',event_id);
                                        $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event_id).attr('disabled',false);
                                        switch(event_enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                            case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                            case '0' : 
                                                $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                                $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                                break;
                                            default : $('#ModalEdit #messagetext').text(''); break;
                                        }
                                    }
                                }
                                else{
                                    if(arrayStudents < 3){
                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                                    }
                                    else{ // event has 3 students
                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                        $('#ModalEdit #messagetext').text('Ce cours est complet');
                                    }
                                }
                                break;
                        }
                    }
                    if(event_user_type==0 || event_user_type==4) // the event belongs to another student
                    {
                        switch(event_private)
                        {
                            case '1' : // private event : student_count = 1
                                $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                $('#ModalEdit #messagetext').css('display','').text('Ce cours est complet');
                                break;
                            case '' : // not a private event : student_count up to 3
                                if(event_enrolled!='' && event_enrolled_user_id==user_id){ // current student is enrolled
                                    $('#ModalEdit #messagetext').css('display','').text('');
                                    if(event_enrolled_status == '2'){ // current student has been declined
                                        $('#ModalEdit #messagetext').text('Vous avez été décliné');
                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('data-id',event_id);
                                    }
                                    else{
                                        $('#ModalEdit #unsubscribeEditEvent').css('display','').attr('data-id',event_id);
                                        $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event_id).attr('disabled',false);
                                        switch(event_enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                            case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                            case '0' : 
                                                $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                                $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                                break;
                                            default : $('#ModalEdit #messagetext').text(''); break;
                                        }
                                    }
                                }
                                else{
                                    if(arrayStudents < 3){
                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                                    }
                                    else{ // event has 3 students
                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                        $('#ModalEdit #messagetext').css('display','').text('Ce cours est complet');
                                    }
                                }
                                break;
                        }
                    }
                }
                break;
            case 4 : // current user is a parent
                if(event_user_type==1){ // the event belongs to a teacher
                    switch(event_private)
                    {
                        case '1' : // private event : student_count = 1
                            if(event_enrolled!=''){ // event has enrolled student
                                if(event_enrolled_user_id==user_id){ // current student is enrolled
                                    $('#messagetext').css('display','').text('');
                                    if(event_enrolled_status == '2'){ // current student has been declined
                                        $('#messagetext').text('Vous avez été décliné');
                                        $('#confirmChildEditEvent').css('display','').attr('data-id',event_id);
                                    }
                                    else{
                                        $('#unsubscribeEditEvent').css('display','').attr('data-id',event_id);
                                        $('#doesNotSuitEditEvent').css('display','').attr('data-id',event_id);
                                        switch(event_enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                            case '1' : $('#messagetext').text('Vous avez été confirmé'); break;
                                            case '0' : 
                                                $('#messagetext').text('Vous n\'avez pas encore été confirmé');
                                                $('#doesNotSuitEditEvent').attr('disabled',true);
                                                break;
                                            default : $('#messagetext').text(''); break;
                                        }
                                    }
                                }
                                else{
                                    $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                    $('#messagetext').css('display','').text('Ce cours est complet');
                                }
                            }
                            else{
                                if(arrayStudents==1){
                                    $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                    $('#messagetext').css('display','').text('Ce cours est complet');
                                }
                                else $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                            }
                            break;
                        case '' : // not a private event : student_count up to 3
                            if(event_enrolled!=''){ // a student is enrolled
                                if(event_enrolled_user_id==user_id){
                                    $('#messagetext').css('display','').text('');
                                    if(event_enrolled_status == '2'){ // current student has been declined
                                        $('#messagetext').text('Vous avez été décliné');
                                        $('#confirmChildEditEvent').css('display','').attr('data-id',event_id);
                                    }
                                    else{
                                        $('#unsubscribeEditEvent').css('display','').attr('data-id',event_id);
                                        $('#doesNotSuitEditEvent').css('display','').attr('data-id',event_id);
                                        switch(event_enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                            case '1' : $('#messagetext').text('Vous avez été confirmé'); break;
                                            case '0' : 
                                                $('#messagetext').text('Vous n\'avez pas encore été confirmé');
                                                $('#doesNotSuitEditEvent').attr('disabled',true);
                                                break;
                                            default : $('#messagetext').text(''); break;
                                        }
                                    }
                                }
                                else{
                                    if(arrayStudents < 3) $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                                }
                            }
                            else{;
                                if(arrayStudents < 3){
                                    $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                                }
                                else{ // event has 3 students
                                    $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                    $('#messagetext').text('Ce cours est complet');
                                }
                            }
                            break;
                    }
                }
                if(event_user_type==0 || event_user_type==4){ // the event belongs to another student
                    switch(event_private)
                    {
                        case '1' : // private event : student_count = 1
                            $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                            $('#messagetext').css('display','').text('Ce cours est complet');
                            break;
                        case '' : // not a private event : student_count up to 3
                            if(event_enrolled!='' && event_enrolled_user_id==user_id){ // current parent is enrolled
                                $('#messagetext').css('display','').text('');
                                if(event_enrolled_status == '2'){ // current student has been declined
                                    $('#messagetext').text('Vous avez été décliné');
                                    $('#confirmChildEditEvent').css('display','').attr('data-id',event_id);
                                }
                                else{
                                    $('#unsubscribeEditEvent').css('display','').attr('data-id',event_id);
                                    $('#doesNotSuitEditEvent').css('display','').attr('data-id',event_id);
                                    switch(event_enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                        case '1' : $('#messagetext').text('Vous avez été confirmé'); break;
                                        case '0' : 
                                            $('#messagetext').text('Vous n\'avez pas encore été confirmé');
                                            $('#doesNotSuitEditEvent').attr('disabled',true);
                                            break;
                                        default : $('#messagetext').text(''); break;
                                    }
                                }
                            }
                            else{
                                if(arrayStudents < 3){
                                    $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event_id);
                                }
                                else{ // event has 3 students
                                    $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event_id);
                                    $('#messagetext').css('display','').text('Ce cours est complet');
                                }
                            }
                            break;
                    }
                }
                break;
        }
    }
}
function initDoesNotSuitModal(owner, dhstart, hours, user_id)
{
    $('#doesNotSuitModal #typeSuitMe').val('all');
    $('#doesNotSuitModal #suitDatepicker').val(moment(dhstart).format('DD/MM/YYYY'));
    $('#doesNotSuitModal #suitTimepickerStart').val(moment(dhstart).format('H'));
    $('#doesNotSuitModal #suitHours').val(hours);
    $('#doesNotSuitModal #suitOwner').val(owner);
    if(owner != user_id){
        if($('#doesNotSuitModal').find('#messageinfo').length==0) $('#doesNotSuitModal').find('.modal-body').append('<div class="alert alert-danger" id="messageinfo"><strong>Attention !</strong> Vous ne serez plus inscrit(e) à ce cours.</div>');
    }
    else $('#doesNotSuitModal').find('#messageinfo').remove();
}
function editModal_initData(event_id, event_owner, event_user_type, event_descr, event_hours, event_start, event_end, event_level, event_course, event_student_home, event_teacher_home, event_private, event_type, event_address, event_child, user_type, user_id)
{
    $('#ModalEdit #id').val(event_id);
    $('#ModalEdit #owner').val(event_owner);
    $('#ModalEdit #editDescr').val(event_descr);
    $('#ModalEdit #editHours option[value="'+event_hours+'"]').prop('selected', true);
    $('#ModalEdit #editDatepicker').attr('disabled',true);
    $('#ModalEdit #editDatepicker').val(moment(event_start).format('DD/MM/YYYY'));
    $('#ModalEdit #editTimepickerStart').val(moment(event_start).format('HH'));
    $('#ModalEdit #editTimepickerEnd').val(moment(event_end).format('HH'));
    $('#ModalEdit #editLevelSelect').val(event_level);
    $('#ModalEdit #editChildren').val(event_child);
    //Read Only
    $('#ModalEdit #editLevelSelect').attr('disabled',true);
    $('#ModalEdit #editHours').attr('disabled',true);
    $('#ModalEdit #editTimepickerStart').attr('disabled',true);
    $('#ModalEdit #editDescr').attr('disabled',true);
    $('#ModalEdit #editAddress').attr('disabled',true);
    $('#ModalEdit #editStudent_home').attr('disabled',true);
    $('#ModalEdit #editTeacher_home').attr('disabled',true);
    $('#ModalEdit #editPrivate').attr('disabled',true);
    $('#ModalEdit #editChildren').attr('disabled',true);
    if(event_child != '' && event_user_type == 4 && user_type == 4 && event_owner==user_id) $('#ModalEdit #divEditChildren').css('display','');
    else $('#ModalEdit #divEditChildren').css('display','none');
    if(event_course == '0') $('#ModalEdit #editDivSelectCourse').hide();
    else{
        $('#ModalEdit #editDivSelectCourse').show();
        $('#ModalEdit #editCourseSelect').val(event_course);
        $('#ModalEdit #editCourseSelect').attr('disabled',true)
    }
    if(event_student_home == '1') $('#ModalEdit #editStudent_home').prop('checked', true);
    else $('#ModalEdit #editStudent_home').prop('checked', false);
    if(event_teacher_home == '1') $('#ModalEdit #editTeacher_home').prop('checked', true);
    else $('#ModalEdit #editTeacher_home').prop('checked', false);
    if(event_private=='1') $('#ModalEdit #editPrivate').prop('checked', true);
    else $('#ModalEdit #editPrivate').prop('checked', false);
    $('#ModalEdit #delete').prop('checked', false);
    if(event_type=='1' || event_type=='2'){
        $('#ModalEdit #editAddress').val(event_address);
        $('#ModalEdit #mobilityNotWeweclass').css('display','none');
        $('#ModalEdit #mobilityWeweclass').css('display','');
    }
    else{
        $('#ModalEdit #mobilityNotWeweclass').css('display','');
        $('#ModalEdit #mobilityWeweclass').css('display','none');
    }
}
function editModal_disableFooter()
{
	$('#ModalEdit #confirmEditEvent').attr('disabled',true);
    $('#ModalEdit #deleteEditEvent').attr('disabled',true);
    $('#ModalEdit #proposeEditEvent').attr('disabled',true);
    $('#ModalEdit #unsubscribeEditEvent').attr('disabled',true);
    $('#ModalEdit #withdrawEditEvent').attr('disabled',true);
    $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
    $('#ModalEdit #submitEditEvent').attr('disabled',true);
    $('#ModalEdit #messagetext').css("display","none");
}
function editModal_enableFooter()
{
    $('#ModalEdit #confirmEditEvent').attr('disabled',false);
    $('#ModalEdit #deleteEditEvent').attr('disabled',false);
    $('#ModalEdit #proposeEditEvent').attr('disabled',false);
    $('#ModalEdit #unsubscribeEditEvent').attr('disabled',false);
    $('#ModalEdit #withdrawEditEvent').attr('disabled',false);
    $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',false);
    $('#ModalEdit #submitEditEvent').attr('disabled',false);
}
function editModal_initFooter()
{
	$('#ModalEdit #deleteEditEvent').attr('data-id',0);
    $('#ModalEdit #confirmEditEvent').attr('data-id',0);
    $('#ModalEdit #proposeEditEvent').attr('data-id',0);
    $('#ModalEdit #unsubscribeEditEvent').attr('data-id',0);
    $('#ModalEdit #withdrawEditEvent').attr('data-id',0);
    $('#ModalEdit #doesNotSuitEditEvent').attr('data-id',0);
    $('#ModalEdit #submitEditEvent').css("display","none");
    $('#ModalEdit #deleteEditEvent').css("display","none");
    $('#ModalEdit #confirmEditEvent').css("display","none");
    $('#ModalEdit #proposeEditEvent').css("display","none");
    $('#ModalEdit #confirmChildEditEvent').css('display',"none");
    $('#ModalEdit #unsubscribeEditEvent').css("display","none");
    $('#ModalEdit #withdrawEditEvent').css("display","none");
    $('#ModalEdit #doesNotSuitEditEvent').css("display","none");
    $('#ModalEdit #messagetext').css("display","none");
    $('#ModalEdit #generalnote').css("display","none");
}
function editModal_pastEvent()
{
    $('#ModalEdit #editDatepicker').attr('disabled', true);
    $('#ModalEdit #editTimepickerStart').attr('disabled',true);
    $('#ModalEdit #editHours').attr('disabled',true);
    editModal_disableFooter();
}
function showInfoCardModal(res)
{
    var genderToShow = (res.userInfo['sex']) ? 'Monsieur' : 'Madame';
    $('#ModalShowInfoCard #infoCardSex').html(genderToShow);
    $('#ModalShowInfoCard #infoCardLastName').html(res.userInfo['last_name']);
    $('#ModalShowInfoCard #infoCardFirstName').html(res.userInfo['first_name']);
    if(res.userInfo['owner_type']==0 || res.userInfo['owner_type']==1){
        $('#ModalShowInfoCard #divInfoCardLevel').removeClass('hideDiv');
        $('#ModalShowInfoCard #infoCardLevel').html(res.levelToShow);
    }
    else $('#ModalShowInfoCard #divInfoCardLevel').addClass('hideDiv');
    if(res.userInfo['owner_type']==1){
        $('#ModalShowInfoCard #divInfoCardCourse').removeClass('hideDiv');
        $('#ModalShowInfoCard #infoCardCourse').html(res.courseToShow);
    }
    else $('#ModalShowInfoCard #divInfoCardCourse').addClass('hideDiv');
    $('#ModalShowInfoCard #infoCardLocality').html('Habitant à '+res.userInfo['owner_locality']);
    $('#ModalShowInfoCard #infoCardEmail').html(res.userInfo['email']);
    //$('#ModalShowInfoCard #infoCardPhone').html(res.userInfo['phone']);
    $('#ModalShowInfoCard #infoCardCount').html(res.countDeal);
    if(res.userInfo['owner_type']==1){
        $('#ModalShowInfoCard #divInfoCardRate').removeClass('hideDiv');
        $('#ModalShowInfoCard #infoCardRate').html(res.rating);
    }
    else $('#ModalShowInfoCard #divInfoCardRate').addClass('hideDiv');
}
function getPricing(type, level, level_label, duration, nbrStudents, mobility, event_user_type){
    var html = '';
    if(type != 1){
        var sum = 0;
        var th = 0; // tarif horaire -- pricing per hour

        switch(level){
            case 1: th = 11; break;
            case 2: th = 13; break;
            case 3: th = 15; break;
        }
        if(nbrStudents==2) th -= 1;
        if(nbrStudents==3) th -= 2;
        html = '<span style="font-size:8pt">T.H. '+th+' €/h</span><br>';

        sum = th * duration * nbrStudents;
        /*if(mobility!=null){
            if((mobility==1 && event_user_type==1) || (mobility==null && (event_user_type==0 || event_user_type==4))) sum += 2;
        }*/
        
        html += sum+' €<br>';

        var detail = '<span class="showInfoCard" style="font-size:8pt" id="" data-toggle="modal" data-target="#ModalDetailedPricing"><u>Détail</u></span>';
        html += detail;
        $('#ModalDetailedPricing #infoCardLevel').html(level_label);
        $('#ModalDetailedPricing #infoCardPricing').html(th+' €/h');
        $('#ModalDetailedPricing #infoCardNbrStudents').html(nbrStudents);
        $('#ModalDetailedPricing #infoCardDuration').html(duration+'h');
        //var mobilityToShow = (((mobility==1 && event_user_type==1) || (mobility==null && (event_user_type==0 || event_user_type==4))) && mobility!=null) ? 'Oui, +2€' : 'Non, 0 €';
        $('#ModalDetailedPricing #infoCardMobility').html('<i>En développement</i>');
        $('#ModalDetailedPricing #infoCardTotal').html(sum+' €');
    }
    else html = '<span class="small">Prix symbolique à définir</span>';

    return html;
}
