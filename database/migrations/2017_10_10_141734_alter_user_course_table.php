<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_course', function(Blueprint $table) {
            $table->dropColumn('course_id');
            $table->integer('edm')->nullable();
            $table->integer('math')->nullable();
            $table->integer('francais')->nullable();
            $table->integer('neerlandais')->nullable();
            $table->integer('anglais')->nullable();
            $table->integer('chimie')->nullable();
            $table->integer('biologie')->nullable();
            $table->integer('physique')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_course', function(Blueprint $table) {
            $table->integer('course_id');
            $table->dropColumn('edm');
            $table->dropColumn('math');
            $table->dropColumn('francais');
            $table->dropColumn('neerlandais');
            $table->dropColumn('anglais');
            $table->dropColumn('chimie');
            $table->dropColumn('biologie');
            $table->dropColumn('physique');
        });
    }
}
