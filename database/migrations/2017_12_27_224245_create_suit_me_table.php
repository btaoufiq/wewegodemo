<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuitMeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suit_me', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('asked_by');
            $table->dateTime('dhstart');
            $table->dateTime('dhend');
            $table->tinyInteger('validated')->default(0);
            $table->dateTime('validation_date')->nullable()->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suit_me');
    }
}
