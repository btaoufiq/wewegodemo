<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descr')->nullable();
            $table->dateTime('dhstart');
            $table->dateTime('dhend');
            $table->boolean('at_home')->nullable(); // à domicile
            $table->boolean('away')->nullable(); // déplacement
            $table->integer('owner'); // user_id
            $table->integer('level_id');
            $table->integer('course_id');
            $table->boolean('private')->nullable();
            $table->integer('duration')->default(1);
            $table->tinyInteger('deal')->default(0);
            $table->tinyInteger('invalide')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
