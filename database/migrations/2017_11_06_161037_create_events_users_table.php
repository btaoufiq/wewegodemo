<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id');
            $table->integer('user_id');
            $table->tinyInteger('user_type');
            $table->tinyInteger('accepted')->default(0);
            $table->boolean('mobility')->nullable()->default(null);
            $table->tinyInteger('changed')->default(0);
            $table->dateTime('suit_me_start')->nullable()->default(NULL);
            $table->dateTime('suit_me_end')->nullable()->default(NULL);
            $table->string('reason')->nullable();
            $table->dateTime('range_end')->default(NULL)->nullable();
            $table->dateTime('range_start')->default(NULL)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events_users');
    }
}
