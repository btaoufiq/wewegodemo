<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_level', function(Blueprint $table) {
            $table->dropColumn('level_id');
            $table->integer('primaire')->nullable();
            $table->integer('sec_inf')->nullable();
            $table->integer('sec_sup')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_level', function(Blueprint $table) {
            $table->integer('level_id');
            $table->dropColumn('primaire');
            $table->dropColumn('sec_inf');
            $table->dropColumn('sec_sup');
        });
    }
}
