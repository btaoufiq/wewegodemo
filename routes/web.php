<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::post('register/createFamily', 'Auth\RegisterController@createFamily');
Route::get('changepassword', function() {
	$notification_count = count(DB::table('notifications')->where('notifiable_id','=',Auth::user()->id)->where('readit','=',0)->get());
	return view('auth/passwords/changepassword', compact('notification_count')); 
});
Route::post('changepassword', 'Auth\UpdatePasswordController@update');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('conditions', ['as'=>'conditions', function (){
	return view('conditions');
}]);

Route::get('/welcome', 'HomeController@welcome');
Route::get('/calendar', 'HomeController@index');
Route::post('/filterCalendar', 'HomeController@filter');
Route::get('/eventByCourse/{id}', 'HomeController@getEventsByCourse');
Route::get('/', 'HomeController@getWeweview');
Route::post('/getInfoCard/{id}','HomeController@getInfoCard');

Route::post('/addWeweclass', 'EventController@addWeweclass');
Route::post('/addEvent', 'EventController@createEvent');
Route::post('/editEvent', 'EventController@editEvent');
Route::post('/suitEvent', 'EventController@suitEvent');
Route::post('/readNotification/{id}', 'EventController@readNotification');
Route::delete('/deleteMyEvent/{id}','EventController@deleteMyEvent');
Route::post('/confirmEvent/{id}','EventController@confirmEvent');
Route::post('/confirmChildEvent/{id}','EventController@confirmChildEvent');
Route::post('/proposeEvent/{id}','EventController@proposeEvent');
Route::post('/unsubscribeEvent/{id}','EventController@unsubscribeEvent');
Route::post('/withdrawEvent/{id}','EventController@withdrawEvent');
Route::post('/acceptEvent/{id}','EventController@acceptEvent');
Route::post('/declineEvent/{id}','EventController@declineEvent');
Route::post('/possibleEvent/{id}','EventController@possibleEvent');
Route::post('/notPossibleEvent/{id}','EventController@notPossibleEvent');
Route::post('/duplicateEvent/{id}','EventController@duplicateEvent');
Route::post('/confirmNewEvent/{id}','EventController@confirmNewEvent');
Route::post('/declineNewEvent/{id}','EventController@declineNewEvent');
Route::post('/proposeNewEvent/{id}','EventController@proposeNewEvent');
Route::post('/acceptNewEvent/{id}','EventController@acceptNewEvent');
Route::post('/reconfirmNewEvent/{id}','EventController@reconfirmNewEvent');
Route::post('/redeclineNewEvent/{id}','EventController@redeclineNewEvent');
Route::post('/moreInfo/{id}','EventController@moreInfo');
Route::post('/rating','EventController@rating');

Route::get('/profil', 'ProfilController@index');
Route::post('/profil', 'ProfilController@update');
Route::get('/children', 'ProfilController@manageChildren');
Route::post('/children', 'ProfilController@updateChildren');

Route::get('/mylist', 'EventController@getList');
Route::post('/mylist', 'EventController@deleteEvent');

Route::get('/asbl', 'asblController@index');
Route::post('/asbl/createEvent', 'asblController@createEvent');
Route::post('/asbl/createTeacher', 'asblController@createTeacher');
Route::post('/asbl/updateTeacher', 'asblController@updateTeacher');
Route::post('/asbl/getSingleEvent/{id}', 'asblController@getSingleEvent');

Route::get('/admin', 'adminController@index');