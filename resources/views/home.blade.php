@extends('layouts.app')
<style>
    #mainContent {
        width: 60%;
        height: 100%;
        display: inline-block;
        text-align: center;
        font-size:13px;
    }
    .hideDiv{
        display: none;
    }
</style>
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    @if($errors->any())
        <div class="alert alert-danger" id="errorMessage">
            @foreach ($errors->all() as $error)
                <span> {{ $error }}</span>
            @endforeach
        </div>
    @endif
    @include('layouts.sideNavLeft')
    @include('layouts.sideNavRight')


    <div id="mainContent" class="container">
        <div id="calendar" class="col-centered" style="margin-top: 15pt;"></div>
    </div>

    <script>
        $('.list-group button').on('click',function (e) {
            e.preventDefault();
            $(this).toggleClass("active");
        });
        $(document).ready(function () {
            $('#app').fadeIn(500);
            $('#app').css('display', '');
            $('.dropdown-toggle').dropdown();
            setTimeout(function() {
                $('#errorMessage').fadeOut('fast');
            }, 4000);
        });

        $(document).ready(function() {
            $('#getWewevueLink').css('color','');
            $('#listNavbarLink').css('color','');
            $('#getCalendarLink').css('color','#ff5a00');
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var nevents = [
                @foreach($events as $event)
                {
                    id: '{{ $event->id }}',
                    title : '{!!($event->type==1) ? ' <i class="fa fa-star" aria-hidden="true"></i> ' : ''!!}{!!($event->type==2) ? ' <i class="fa fa-home" aria-hidden="true"></i> ' : ''!!}{{ ($event->level_id == '1' ? $event->level_label : $event->course_label) }}{!! ($students[$event->id] && $event->type!=2) ? ' <span class="fa-stack" style="font-size:8px"><i class="fa fa-circle-o thin fa-stack-2x"></i><strong class="fa-stack-1x">'.$students[$event->id].'</strong></span>' : '' !!}{!!($teachers[$event->id]) ?  ' <i class="fa fa-male" aria-hidden="true"></i>' : ''!!}',
                    start: '{{ $event->dhstart }}',
                    end: '{{ $event->dhend }}',
                    descr: '{{ $event->descr }}',
                    owner: '{{ $event->owner }}',
                    child: '{{ $event->child }}',
                    level: '{{ $event->level_id }}',
                    course : '{{ $event->course_id }}',
                    student_home : '{{ $event->student_home }}',
                    teacher_home : '{{ $event->teacher_home }}',
                    private : '{{ $event->private }}',
                    color: '{{ $event->color }}',
                    user_type:'{{$event->user_type}}',
                    enrolled:'{{$event->enrolled}}',
                    enrolled_user_id: '{{$event->enrolled_user_id}}',
                    enrolled_status:'{{$event->enrolled_status}}',
                    hours :'{{$event->duration}}',
                    address : '{{$event->address}}',
                    student_count: '{{$students[$event->id]}}',
                    generaltext:'{{$generaltext[$event->id]}}',
                    type: '{{$event->type}}',
                    deal: '{{$event->deal}}'
                },
                @endforeach
            ];

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                buttonText: {
                    today: 'Aujourd\'hui',
                    month: 'Mois',
                    week: 'Semaine',
                    day: 'Jour',
                    list: 'Planning'
                },
                defaultView: 'month',
                fixedWeekCount: false, // afficher un nombre de semaine variable (entre 4 et 6 semaines) / true = 6semaines
                monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'AoÃ»t', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
                monthNamesShort: ['Jan', 'Fév', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Déc'],
                dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
                dayNamesShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
                defaultDate: date,
                firstDay: 1, //start with monday
                editable: true,
                eventLimit: true, // allow "more" link when too many events
                selectable: true,
                selectHelper: true,
                displayEventTime: false,
                eventStartEditable: false,
                dayRender: function( selecteddate, cell ) {
                    if(moment(moment(selecteddate).format('YYYY-MM-DD')).isBefore(moment(date).format('YYYY-MM-DD'),'day')) cell.css('background-color','GhostWhite');
                },
                select: function(start, end) {
                    if(moment(moment(start).format('YYYY-MM-DD')).isSameOrAfter(moment(date).format('YYYY-MM-DD'),'day'))
                    {
                        $('#ModalAdd #title').val('');
                        $('#ModalAdd #level').val('');
                        $('#ModalAdd #course').val('');
                        $('#ModalAdd #divSelectCourse').hide();
                        $('#ModalAdd #datepicker').val(moment(start).format('DD/MM/YYYY'));
                        $('#ModalAdd #datepicker').attr('disabled',true);
                        $('#ModalAdd #student_home').prop('checked', false);
                        $('#ModalAdd #teacher_home').prop('checked', false);
                        $('#ModalAdd #private').prop('checked', false);
                        $('#ModalAdd').modal('show');
                    }
                },
                eventRender: function(event, element) {
                    if(event.type==1) element.css('border-left','2px solid #ff5a00');
                    if(new Date(event.start).getTime() < new Date().getTime()) element.css('background-color','#bfbebe').css('opacity','0.7').css('border-color','#bfbebe');
                    if(event.enrolled_status==0 && {{Auth::user()->id}}!=event.owner) element.css('opacity','0.7');
                    element.css('cursor','pointer');
                    element.find('span.fc-title').html(element.find('span.fc-title').text());
                    element.bind('click', function() {
                        var elementTitle = element.find('span.fc-title').text();
                        var hasDigit = (/\d/).test(elementTitle);
                        var modalTitle = (hasDigit) ? elementTitle.substr(0,elementTitle.indexOf(elementTitle.match(/\d/))) : elementTitle;
                        $('#ModalEdit #myModalLabel').html(modalTitle);
                        editModal_initFooter();
                        //editModal_enableFooter();

                    // Init modal data
                        editModal_initData(event.id, event.owner, event.user_type, event.descr, event.hours, event.start, event.end, event.level, event.course, event.student_home, event.teacher_home, event.private, event.type, event.address, event.child, {{Auth::user()->type}}, {{Auth::user()->id}});

                        initDoesNotSuitModal(event.owner, event.dhstart, event.hours, {{Auth::user()->id}});

                    // Buttons display
 /*                       {{--editModal_actionButtonsDisplay(event.id, event.owner, event.user_type, event.enrolled, event.enrolled_status, event.private, {{$teachers[$event->id]}}, {{$students[$event->id]}}, {{Auth::user()->id}}, {{Auth::user()->type}}, event.type, event.child); --}}
 // this method is working only if there are events to display in the calendar
*/
                        if(event.owner=={{Auth::user()->id}}) // current user is the owner
                        {
                            $('#ModalEdit #editDatepicker').attr('disabled', false);
                            $('#ModalEdit #editTimepickerStart').attr('disabled',false);
                            $('#ModalEdit #submitEditEvent').css("display","");
                            $('#ModalEdit #deleteEditEvent').attr('data-id',event.id);
                            $('#ModalEdit #deleteEditEvent').css("display","");
                            $('#ModalEdit #messagetext').css("display","").text('Vous êtes le propriétaire de ce cours');
                            editModal_enableFooter();
                        }
                        else{
                            switch({{Auth::user()->type}})
                            {
                                case 1 : // current user is a teacher
                                    if(event.user_type=='1') // the event belongs to another teacher
                                    {
                                        $('#ModalEdit #proposeEditEvent').css('display','').attr('disabled',true);
                                        $('#ModalEdit #messagetext').text('Vous ne pouvez pas vous proposer à ce cours');
                                    }
                                    else // the event belongs to a student
                                    {
                                        
                                        if(event.type==1 || event.type==2){
                                            $('#ModalEdit #proposeEditEvent').css('display','').attr('disabled',true);
                                            $('#ModalEdit #messagetext').css('display','').text('Vous ne pouvez pas vous proposer à ce cours');
                                        }
                                        else if(event.enrolled!='' && event.enrolled_user_id=={{Auth::user()->id}}){ // current teacher is enrolled
                                            $('#ModalEdit #messagetext').css('display','').text('');
                                            if(event.enrolled_status == '2'){ // current teacher has been declined
                                                $('#ModalEdit #messagetext').text('Vous avez été décliné');
                                                $('#ModalEdit #proposeEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                            }
                                            else{
                                                $('#ModalEdit #withdrawEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                switch(event.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                    case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                                    case '0' : 
                                                        $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                                        $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                                        break;
                                                    default : $('#ModalEdit #messagetext').text(''); break;
                                                }
                                            }
                                        }
                                        else{
                                            $('#ModalEdit #proposeEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                        }
                                    }
                                    break;
                                case 0 : // current user is a student
                                    if(event.type==2){ //asbl event
                                        $('#ModalEdit #moreInfoEditEvent').css('display','').attr('data-id',event.id);
                                    }
                                    else{
                                        if(event.user_type=='1') // the event belongs to a teacher
                                        {
                                            switch(event.private)
                                            {
                                                case '1' : // private event : student_count = 1
                                                    if(event.enrolled!=''){ // event has enrolled student
                                                        if(event.enrolled_user_id=={{Auth::user()->id}}){ // current student is enrolled
                                                            $('#ModalEdit #messagetext').css('display','').text('');
                                                            if(event.enrolled_status == '2'){ // current student has been declined
                                                                $('#ModalEdit #messagetext').text('Vous avez été décliné');
                                                                $('#ModalEdit #confirmEditEvent').css('display','').attr('data-id',event.id);
                                                            }
                                                            else{
                                                                $('#ModalEdit #unsubscribeEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                                $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                                switch(event.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                                    case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                                                    case '0' : 
                                                                        $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                                                        $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                                                        break;
                                                                    default : $('#ModalEdit #messagetext').text(''); break;
                                                                }
                                                            }
                                                        }
                                                        else{
                                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                            $('#ModalEdit #messagetext').text('Ce cours est complet');
                                                        }
                                                    }
                                                    else{
                                                        $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                                    }
                                                    break;
                                                case '' : // not a private event : student_count up to 3
                                                    if(event.enrolled!='' && event.enrolled_user_id=={{Auth::user()->id}}){ // current student is enrolled
                                                        $('#ModalEdit #messagetext').css('display','').text('');
                                                        if(event.enrolled_status == '2'){ // current student has been declined
                                                            $('#ModalEdit #messagetext').text('Vous avez été décliné');
                                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('data-id',event.id);
                                                        }
                                                        else{
                                                            $('#ModalEdit #unsubscribeEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                            $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                            switch(event.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                                case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                                                case '0' : 
                                                                    $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                                                    $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                                                    break;
                                                                default : $('#ModalEdit #messagetext').text(''); break;
                                                            }
                                                        }
                                                    }
                                                    else{
                                                        if(event.student_count < 3){
                                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                                        }
                                                        else{ // event has 3 students
                                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                            $('#ModalEdit #messagetext').text('Ce cours est complet');
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                        if(event.user_type=='0' || event.user_type=='4') // the event belongs to another student
                                        {
                                            switch(event.private)
                                            {
                                                case '1' : // private event : student_count = 1
                                                    $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                    $('#ModalEdit #messagetext').css('display','').text('Ce cours est complet');
                                                    break;
                                                case '' : // not a private event : student_count up to 3
                                                    if(event.enrolled!='' && event.enrolled_user_id=={{Auth::user()->id}}){ // current student is enrolled
                                                        $('#ModalEdit #messagetext').css('display','').text('');
                                                        if(event.enrolled_status == '2'){ // current student has been declined
                                                            $('#ModalEdit #messagetext').text('Vous avez été décliné');
                                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('data-id',event.id);
                                                        }
                                                        else{
                                                            $('#ModalEdit #unsubscribeEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                            $('#ModalEdit #doesNotSuitEditEvent').css('display','').attr('data-id',event.id).attr('disabled',false);
                                                            switch(event.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                                case '1' : $('#ModalEdit #messagetext').text('Vous avez été confirmé'); break;
                                                                case '0' : 
                                                                    $('#ModalEdit #messagetext').text('Vous n\'avez pas encore été confirmé');
                                                                    $('#ModalEdit #doesNotSuitEditEvent').attr('disabled',true);
                                                                    break;
                                                                default : $('#ModalEdit #messagetext').text(''); break;
                                                            }
                                                        }
                                                    }
                                                    else{
                                                        if(event.student_count < 3){
                                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                                        }
                                                        else{ // event has 3 students
                                                            $('#ModalEdit #confirmEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                            $('#ModalEdit #messagetext').css('display','').text('Ce cours est complet');
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    break;
                                case 4 :
                                    if(event.user_type==1){ // the event belongs to a teacher
                                        switch(event.private)
                                        {
                                            case '1' : // private event : student_count = 1
                                                if(event.enrolled!=''){ // event has enrolled student
                                                    if(event.enrolled_user_id==user_id){ // current student is enrolled
                                                        $('#messagetext').css('display','').text('');
                                                        if(event.enrolled_status == 2){ // current student has been declined
                                                            $('#messagetext').text('Vous avez été décliné');
                                                            $('#confirmChildEditEvent').css('display','').attr('data-id',event.id);
                                                        }
                                                        else{
                                                            $('#unsubscribeEditEvent').css('display','').attr('data-id',event.id);
                                                            $('#doesNotSuitEditEvent').css('display','').attr('data-id',event.id);
                                                            switch(event.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                                case 1 : $('#messagetext').text('Vous avez été confirmé'); break;
                                                                case 0 : 
                                                                    $('#messagetext').text('Vous n\'avez pas encore été confirmé');
                                                                    $('#doesNotSuitEditEvent').attr('disabled',true);
                                                                    break;
                                                                default : $('#messagetext').text(''); break;
                                                            }
                                                        }
                                                    }
                                                    else{
                                                        $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                        $('#messagetext').css('display','').text('Ce cours est complet');
                                                    }
                                                }
                                                else{
                                                    if(event.student_count==1){
                                                        $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                        $('#messagetext').css('display','').text('Ce cours est complet');
                                                    }
                                                    else $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                                }
                                                break;
                                            case '' : // not a private event : student_count up to 3
                                                if(event.enrolled!=''){ // a student is enrolled
                                                    if(event.enrolled_user_id=={{Auth::user()->id}}){
                                                        $('#messagetext').css('display','').text('');
                                                        if(event.enrolled_status == '2'){ // current student has been declined
                                                            $('#messagetext').text('Vous avez été décliné');
                                                            $('#confirmChildEditEvent').css('display','').attr('data-id',event.id);
                                                        }
                                                        else{
                                                            $('#unsubscribeEditEvent').css('display','').attr('data-id',event.id);
                                                            $('#doesNotSuitEditEvent').css('display','').attr('data-id',event.id);
                                                            switch(event.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                                case '1' : $('#messagetext').text('Vous avez été confirmé'); break;
                                                                case '0' : 
                                                                    $('#messagetext').text('Vous n\'avez pas encore été confirmé');
                                                                    $('#doesNotSuitEditEvent').attr('disabled',true);
                                                                    break;
                                                                default : $('#messagetext').text(''); break;
                                                            }
                                                        }
                                                    }
                                                    else{
                                                        if(event.student_count < 3) $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                                    }
                                                }
                                                else{;
                                                    if(event.student_count < 3){
                                                        $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                                    }
                                                    else{ // event has 3 students
                                                        $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                        $('#messagetext').text('Ce cours est complet');
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                    if(event.user_type==0){ // the event belongs to another student
                                        switch(event.private)
                                        {
                                            case '1' : // private event : student_count = 1
                                                $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                $('#messagetext').css('display','').text('Ce cours est complet');
                                                break;
                                            case '' : // not a private event : student_count up to 3
                                                if(event.enrolled!='' && event.enrolled_user_id=={{Auth::user()->id}}){ // current parent is enrolled
                                                    $('#messagetext').css('display','').text('');
                                                    if(event.enrolled_status == '2'){ // current student has been declined
                                                        $('#messagetext').text('Vous avez été décliné');
                                                        $('#confirmChildEditEvent').css('display','').attr('data-id',event.id);
                                                    }
                                                    else{
                                                        $('#unsubscribeEditEvent').css('display','').attr('data-id',event.id);
                                                        $('#doesNotSuitEditEvent').css('display','').attr('data-id',event.id);
                                                        switch(event.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
                                                            case '1' : $('#messagetext').text('Vous avez été confirmé'); break;
                                                            case '0' : 
                                                                $('#messagetext').text('Vous n\'avez pas encore été confirmé');
                                                                $('#doesNotSuitEditEvent').attr('disabled',true);
                                                                break;
                                                            default : $('#messagetext').text(''); break;
                                                        }
                                                    }
                                                }
                                                else{
                                                    if(event.student_count < 3){
                                                        $('#confirmChildEditEvent').css('display','').attr('disabled',false).attr('data-id',event.id);
                                                    }
                                                    else{ // event has 3 students
                                                        $('#confirmChildEditEvent').css('display','').attr('disabled',true).attr('data-id',event.id);
                                                        $('#messagetext').css('display','').text('Ce cours est complet');
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                    break;
                            }
                        }

                        if(new Date(event.start).getTime() < new Date().getTime()) editModal_pastEvent();
                        
                        $('#ModalEdit').modal('show');
                    });
                },
                eventSources: [{events:nevents}]

            });
        });
    </script>
@endsection