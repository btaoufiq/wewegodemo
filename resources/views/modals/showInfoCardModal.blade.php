<div class="modal fade" id="ModalShowInfoCard" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
        		<div class="form-group">
        			<label class="control-label">
        				<span id="infoCardSex" name="infoCardSex"></span>
	        			<span id="infoCardFirstName" name="infoCardFirstName"></span>
	        			<span id="infoCardLastName" name="infoCardLastName"></span>
	        		</label>
	    			<div id="infoCardLocality" name="infoCardLocality"></div>
        		</div>
	            <div class="form-group hideDiv" id="divInfoCardLevel">
	            	<label class="control-label">Niveau(x)</label>
	    			<div id="infoCardLevel" name="infoCardLevel"></div>
	    		</div>
            	<div class="form-group hideDiv" id="divInfoCardCourse">
	            	<label class="control-label">Matière(s)</label>
	    			<div id="infoCardCourse" name="infoCardCourse"></div>
	    		</div>
	            <div class="form-group">
	            	<label class="control-label">Nombre de cours confirmés</label> &#160;
	    			<span id="infoCardCount" name="infoCardCount" style="font-size: 16px;"></span>
	    		</div>
	    		<div class="form-group hideDiv" id="divInfoCardRate">
	            	<label class="control-label">Note moyenne</label> &#160;
	    			<span id="infoCardRate" name="infoCardRate" style="font-size: 16px;"></span>
	    		</div>
	            <div class="form-group">
	            	<label class="control-label">Contact</label>
	            	<div><i class="fa fa-envelope-o"></i>&#160; <span id="infoCardEmail" name="infoCardEmail"></span></div>
	            	<!--<div><i class="fa fa-phone"></i>&#160; <span id="infoCardPhone" name="infoCardPhone"></span></div>-->
	            </div>
	            <div class="text-right">
	            	<button class="btn btn-default btn-sm" data-dismiss="modal">Fermer</button>
	            </div>
            </div>
        </div>
    </div>
</div>