<div class="modal fade" id="ModalDetailedPricing" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
            	<div class="form-group text-center" style="padding: 10pt;font-size: 14pt;">
        			<label class="control-label" id="infoCardLevel"></label>
        		</div>
            	<div class="form-group">
        			<label class="control-label">Tarif horaire : </label>
	    			<span id="infoCardPricing" name="infoCardPricing"></span>
        		</div>
        		<div class="form-group">
        			<label class="control-label">Nombre d'élèves : </label>
	    			<span id="infoCardNbrStudents" name="infoCardNbrStudents"></span>
        		</div>
        		<div class="form-group">
        			<label class="control-label">Durée du cours : </label>
	    			<span id="infoCardDuration" name="infoCardDuration"></span>
        		</div>
        		<div class="form-group">
        			<label class="control-label">Déplacement du professeur : </label>
	    			<span id="infoCardMobility" name="infoCardMobility"></span>
        		</div>
        		<div class="form-group text-center" style="padding-top: 20pt;padding-bottom: 10pt;font-size: 14pt;">
        			<label class="control-label">Somme totale : 
	    			<span id="infoCardTotal" name="infoCardTotal"></span></label>
        		</div>
            	<div class="text-right">
	            	<button class="btn btn-default btn-sm" data-dismiss="modal">Fermer</button>
	            </div>
            </div>
        </div>
    </div>
</div>