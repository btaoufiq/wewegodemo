<!-- Modal -->
<div class="modal fade" id="ModalEdit" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="{{ url('editEvent') }}" id="editEventForm">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div style="display: inline-block;"><h4 class="modal-title" id="myModalLabel">Editer</h4></div>
                    @if(Auth::user()->type==1)
                        <div style="display: inline-block;margin-left:5%;"><button id="editSeeRatingBtn" class="btn btn-success btn-xs" style="display: none;" data-id="0">Voir ma note</button></div>
                    @endif
                </div>
                <div class="modal-body">
                    <div class="form-group" id="divEditChildren" style="display: none;">
                        <label for="editChildren" class="col-sm-2 control-label">Concerne</label>
                        <div class="col-sm-10">
                            <?php $mychildren = DB::table('children')->where('parent','=',Auth::user()->id)->get(); ?>
                            <select class="form-control" id="editChildren" name="editChildren">
                                <option></option>
                                @foreach($mychildren as $child)
                                    <option value="{{$child->id}}">{{$child->first_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editLevelSelect" class="col-sm-2 control-label">Niveau</label>
                        <div class="col-sm-10">
                            <select name="editLevelSelect" class="form-control" id="editLevelSelect">
                                @foreach($levels as $l)
                                    <option value="{{$l->id}}">{{$l->label_fr}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group" id="editDivSelectCourse" style="display: none;">
                        <label for="editCourseSelect" class="col-sm-2 control-label">Cours</label>
                        <div class="col-sm-10">
                            <select name="editCourseSelect" class="form-control" id="editCourseSelect">
                                @foreach($courses as $c)
                                    <option value="{{$c->id}}">{{$c->label_fr}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editDhstart" class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="editDatepicker">
                        </div>
                    </div>
                    <div id="course2_div">
                        <div class="form-group">
                            <label for="editDhstart" class="col-sm-2 control-label">Début</label>
                            <!--<div class="col-sm-2">
                                <input type="text" class="form-control" id="editTimepickerStart">
                            </div>-->
                            <div class="col-sm-3">
                                <select class="form-control" id="editTimepickerStart">
                                    <option value="08">8h</option>
                                    <option value="09">9h</option>
                                    <option value="10">10h</option>
                                    <option value="11">11h</option>
                                    <option value="12">12h</option>
                                    <option value="13">13h</option>
                                    <option value="14">14h</option>
                                    <option value="15">15h</option>
                                    <option value="16">16h</option>
                                    <option value="17">17h</option>
                                    <option value="18">18h</option>
                                    <option value="19">19h</option>
                                </select>
                            </div>
                            <label for="editHours" class="col-sm-2 control-label">Durée</label>
                            <div class="col-sm-3">
                                <select id="editHours" class="form-control">
                                    <option value="1">1h</option>
                                    <option value="2" >2h</option>
                                    <option value="3" >3h</option>
                                </select>
                            </div>
                            <input type="hidden" name="editDhstart" id="editDhstart">
                        </div>
                        <div class="form-group">
                            <label for="editDhEnd" class="col-sm-2 control-label">Fin</label>

                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="editTimepickerEnd" disabled>
                            </div>
                            <input type="hidden" name="editDhEnd" id="editDhEnd">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editDescr" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <input type="text" name="editDescr" class="form-control" id="editDescr" placeholder="Description">
                        </div>
                    </div>

                    <div class="form-group" id="mobilityNotWeweclass">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-10">
						<span style="float: left; display: inline-block;">
							<input type="checkbox" id="editStudent_home" name="editStudent_home" value="1"> 
							<label for="editStudent_home" class="control-label">Domicile élève</label>
                            &emsp;
                            <input type="checkbox" id="editTeacher_home" name="editTeacher_home" value="1">
							<label for="editTeacher_home" class="control-label">Domicile professeur</label>
						</span>
                            <span style="float: right; display: inline-block; margin-right: 10px">
							<input type="checkbox" id="editPrivate" name="editPrivate" value="1"> 
							<label for="editPrivate" class="control-label">Cours particulier</label>
						</span>
                        </div>
                    </div>
                    <div class="form-group" id="mobilityWeweclass" style="display: none;">
                        <label for="editAddress" class="col-sm-2 control-label">Adresse</label>
                        <div class="col-sm-10">
                            <input type="text" name="editAddress" class="form-control" id="editAddress">
                        </div>
                    </div>
                    <input type="hidden" name="id" class="form-control" id="id">
                    <input type="hidden" name="owner" class="form-control" id="owner">
                </div>
                <div class="modal-footer">
                    <p id="messagetext" class="pull-left" style="display: none;"></p>
                    <p id="generalnote" class="pull-left" style="display: none;"></p>
                    <button type="submit" id="submitEditEvent" class="btn btn-primary btn-sm">Enregistrer</button>
                    <button id="confirmEditEvent" data-id="0" class="btn btn-success btn-sm" style="display: none;">Participer</button>
                    <button id="confirmChildEditEvent" data-id="0" class="btn btn-success btn-sm" style="display: none;">Inscrire mon enfant</button>
                    <button id="proposeEditEvent" data-id="0" class="btn btn-success btn-sm" style="display: none;">Se proposer</button>
                    <button id="unsubscribeEditEvent" data-id="0" class="btn btn-warning btn-sm" style="display: none;">Se désinscrire</button>
                    <button id="withdrawEditEvent" data-id="0" class="btn btn-warning btn-sm" style="display: none;">Se désinscrire</button>
                    <button id="doesNotSuitEditEvent" data-id="0" class="btn btn-info btn-sm" style="display: none;">Déplacer le cours</button>
                    <button id="moreInfoEditEvent" data-id="0" class="btn btn-primary btn-sm" style="display: none;">Plus d'info</button>
                    <button id="deleteEditEvent" data-id="0" class="btn btn-danger btn-sm">Supprimer</button>
                    <button class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>
@include('modals.doesNotSuitModal')
@include('modals.infoAsblModal')
<script type="text/javascript">

    $(document).ready(function () {
        $.datepicker.setDefaults($.datepicker.regional[""]);
        $("#editDatepicker").datepicker($.datepicker.regional["fr"]);

        var text_max = 350;
        $('#count_message').html(text_max + ' caractères restants');
        $('#text').keydown(function() {
            var text_length = $('#text').val().length;
            var text_remaining = text_max - text_length;

            $('#count_message').html(text_remaining + ' caractères restants');
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#editTimepickerStart').timepicker({
            'timeFormat': 'HH', 'minTime': '8:00 am', 'maxTime': '8:00 pm', onSelect: function () {
                $('#editHours').css('display','');
                $('#editHours').val(1);
                $('#editHours option').attr('disabled',false);
                if($('#editTimepickerStart').val() == 18)
                {
                    $('#editHours option[value="3"]').attr('disabled',true);
                }
                else if($('#editTimepickerStart').val() == 19)
                {
                    $('#editHours option[value="2"]').attr('disabled',true);
                    $('#editHours option[value="3"]').attr('disabled',true);
                }
                changeEndTime();
            }
        });
        function changeEndTime()
        {
            $('#editTimepickerEnd').val(parseInt($('#editTimepickerStart').val()) + parseInt($('#editHours').val()))
        }

        $('#editHours').on('change', function(e)
        {
            e.preventDefault();
            changeEndTime();
        });
    });
    $('#editLevelSelect').on('change', function ()
    {
        if($(this).val() != 1) $('#editDivSelectCourse').show();
        else $('#editDivSelectCourse').hide();
    });
    $("#submitEditEvent").on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        swal({
            title: 'Êtes-vous sûr ?',
            text: "Tous les participants seront avertis de cette modification",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Modifier',
            cancelButtonText: 'Annuler',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        },function(confirm) {
            if (confirm) {
                var t = $('#editDatepicker').val() + ' ' + $('#editTimepickerStart').val();
                $("#editDhstart").val(t);
                var t = $('#editDatepicker').val() + ' ' + $('#editTimepickerEnd').val();
                $("#editDhEnd").val(t);
                $('#editEventForm').submit();
            }
        });
    });
    $('#deleteEditEvent').on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        var id = $this.attr('data-id');
        swal({
            title: 'Êtes-vous sûr ?',
            text: "Tous les participants seront avertis de cette suppression",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Supprimer',
            cancelButtonText: 'Annuler',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        },function(confirm) {
            if (confirm) {
                $.ajax({
                    'url': '/deleteMyEvent/' + id,
                    'type': 'POST',
                    'data': {_method: 'DELETE'},
                    'success': function (res) {
                        $('#calendar').fullCalendar('removeEvents', id);
                        $('#ModalEdit').modal('hide');
                    }
                });
            }
        });
    });
    $('#moreInfoEditEvent').on('click', function(e)
    {
        e.preventDefault();
        var info_course_label = $('#editCourseSelect :selected').text();
        var info_level_label = $('#editLevelSelect :selected').text();
        var info_startDate = $('#editDatepicker').val();
        var info_startHour = $('#editTimepickerStart :selected').val();
        var info_endHour = $('#editTimepickerEnd').val();
        var info_address = $('#editAddress').val();
        var info_descr = $('#editDescr').val();
        var info_id = $('#id').val();
        $('#ModalInfoAsbl #infoAsblTitle').html(info_course_label+'&#160; &#8212; &#160;'+info_level_label);
        $('#ModalInfoAsbl #infoAsblDate').text(info_startDate);
        $('#ModalInfoAsbl #infoAsblTime').text(info_startHour+'h - '+info_endHour+'h');
        $('#ModalInfoAsbl #infoAsblAddress').text(info_address);
        $('#ModalInfoAsbl #infoAsblDescription').text(info_descr);
        $('#ModalInfoAsbl #event_id').val(info_id);
        $('#ModalInfoAsbl').modal('show');
    });
    $('#confirmEditEvent').on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        var id = $this.attr('data-id');
        var mobility = '';
        if($('#editStudent_home').is(":checked") && $('#editTeacher_home').is(":checked"))
        {
            mobility = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Définir le lieu</label><div class="btn-group col-sm-10" data-toggle="buttons"><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
        }
        swal({
            title: "Demande de participation",
            text: mobility+' <textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
            html: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText:'Participer',
            cancelButtonText:'Annuler'
        }, function(inputValue) {
            if(inputValue) {
                var data = {};
                var arr = [];
                if (inputValue != "") {
                    var value = $('#text').val();
                    data['message'] = value;

                }
                if (mobility != '') {
                    data['mobility'] = $("input[name='mobility']:checked").val();
                }
                arr.push(data);
                $.ajax({
                    'url': '/confirmEvent/' + id,
                    'type': 'POST',
                    'data': arr[0],
                    'success': function (res) {
                        $('#ModalEdit').modal('hide');
                        if (res.status == 'failed') {
                            swal('Oops', res.message, 'error');
                        }
                        else if (res.status == 'success') {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });
    $('#confirmChildEditEvent').on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        var id = $this.attr('data-id');
        var mobility = '';
        if($('#editStudent_home').is(":checked") && $('#editTeacher_home').is(":checked"))
        {
            mobility = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Définir le lieu</label><div class="btn-group col-sm-10" data-toggle="buttons"><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
        }
        var selectChild = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Concerne</label><div class="col-sm-10"><select class="form-control" id="childToConfirm" name="childToConfirm">';
        @foreach($mychildren as $child)
            selectChild += '<option value="{{$child->id}}">{{$child->first_name}}</option>';
        @endforeach
        selectChild += '</select></div></div><br/>';
        swal({
            title: "Demande de participation",
            text: selectChild+mobility+' <textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
            html: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText:'Participer',
            cancelButtonText:'Annuler'
        }, function(inputValue) {
            if(inputValue) {
                var data = {};
                var arr = [];
                if (inputValue != "") {
                    var value = $('#text').val();
                    data['message'] = value;
                }
                if (mobility != '') {
                    data['mobility'] = $("input[name='mobility']:checked").val();
                }
                data['childToConfirm'] = $("#childToConfirm").val();
                arr.push(data);
                $.ajax({
                    'url': '/confirmChildEvent/' + id,
                    'type': 'POST',
                    'data': arr[0],
                    'success': function (res) {
                        $('#ModalEdit').modal('hide');
                        if (res.status == 'failed') {
                            swal('Oops', res.message, 'error');
                        }
                        else if (res.status == 'success') {
                            window.location.reload();
                        }
                    }
                });
            }
        });
    });
    $('#proposeEditEvent').on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        var id = $this.attr('data-id');
        var mobility = '';
        if($('#editStudent_home').is(":checked") && $('#editTeacher_home').is(":checked"))
        {
            mobility = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Définir le lieu</label><div class="btn-group col-sm-10" data-toggle="buttons"><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
        }
        swal({
            title: "Proposer de donner le cours",
            text: mobility+'<textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
            html: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText:'Se proposer',
            cancelButtonText:'Annuler'
        }, function(inputValue) {
            if(inputValue)
            {
                var data = {};
                if(inputValue != "")
                {
                    var value = $('#text').val();
                    data = {'message': value};
                }
                $.ajax({
                    'url':'/proposeEvent/'+id,
                    'type':'POST',
                    'data':data,
                    'success': function(res)
                    {
                        $('#ModalEdit').modal('hide');
                        if(res.status == 'failed')
                        {
                            swal('Oops',res.message,'error');
                        }
                        else if(res.status == 'success')
                        {
                            window.location.reload();
                        }
                    }
                });
            }

        });
    });
    $('#unsubscribeEditEvent').on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        var id = $this.attr('data-id');
        $.ajax({
            'url':'/unsubscribeEvent/'+id,
            'type':'POST',
            'success': function(res)
            {
                $('#ModalEdit').modal('hide');
                if(res.status == 'success')
                {
                    window.location.reload();
                }
            }
        });
    });
    $('#withdrawEditEvent').on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        var id = $this.attr('data-id');
        $.ajax({
            'url':'/withdrawEvent/'+id,
            'type':'POST',
            'success': function(res)
            {
                $('#ModalEdit').modal('hide');
                if(res.status == 'success')
                {
                    window.location.reload();
                }
            }
        });
    });
    $('#doesNotSuitEditEvent').on('click', function(e)
    {
        e.preventDefault();
        $this = $(this);
        var id = $this.attr('data-id');
        $('#doesNotSuitModal').find('#suitid').val(id);
        $('#doesNotSuitModal').modal('show');
    });
</script>