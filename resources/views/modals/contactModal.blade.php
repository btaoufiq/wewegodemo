    <!-- Modal -->
    <div class="modal fade" id="modalContact" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{$contact}}</h4>
                </div>
                <div class="modal-body">
                    <?php
                        if($user['type'] == 1) $sqlResult = DB::table('users')->where('type','=','0')->get();
                        else $sqlResult = DB::table('users')->where('type','=','1')->get();
                        foreach ($sqlResult as $row) {
                            echo $row->first_name . ' ' . $row->last_name . '<br/>';
                        }
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>

        </div>
    </div>