<div class="modal fade" id="modalEditTeacher" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editer le profil du professeur</h4>
			</div>
			<div class="modal-body">
				<form method="POST" action="{{ url('asbl/updateTeacher') }}">
					{{ csrf_field() }}
					<fieldset>
						<div class="form-group">
							<label class="control-label">Nom</label>
							<input type="text" id="teacherLastName" name="teacherLastName" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label">Prénom</label>
							<input type="text" id="teacherFirstName" name="teacherFirstName" class="form-control">
						</div>
						<div class="form-group">
							<label class="control-label">Email</label>
							<input type="text" id="teacherEmail" name="teacherEmail" class="form-control">
						</div>
						<?php $localities = DB::table('localities')->get(); ?>
						<div class="form-group">
							<label for="teacherLocalite" class="control-label col-sm-2">Localité</label>
							<select class="form-control" id="teacherLocalite" name="teacherLocalite">
								@foreach($localities as $l)
									<option value="{{$l->code}}">
										{{$l->code}} &#8211; {{$l->label_fr}}
										@if(!empty($l->label_nl))
											/ {{$l->label_nl}}
										@endif
									</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label class="control-label">Téléphone</label>
							<input type="text" id="teacherPhone" name="teacherPhone" class="form-control">
						</div>
						<input type="hidden" name="teacherId" class="form-control" id="teacherId">
						<div class="modal-footer">
							<button type="submit" id="" class="btn btn-primary">Enregistrer</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function ()
	{
		$('#modalEditTeacher').on('show.bs.modal', function(e) {
			$(e.currentTarget).find('#teacherId').val($(e.relatedTarget).data('teacher_id'));
			$(e.currentTarget).find('#teacherFirstName').val($(e.relatedTarget).data('teacher_first_name'));
			$(e.currentTarget).find('#teacherLastName').val($(e.relatedTarget).data('teacher_last_name'));
			$(e.currentTarget).find('#teacherEmail').val($(e.relatedTarget).data('teacher_email'));
			$(e.currentTarget).find('#teacherLocalite option[value="'+$(e.relatedTarget).data('teacher_locality')+'"]').prop('selected', true);
			$(e.currentTarget).find('#teacherPhone').val($(e.relatedTarget).data('teacher_phone'));
		});
	});
</script>