<div class="modal fade" id="ModalAddWeweclass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form class="form-horizontal" method="POST" action="{{ url('addWeweclass') }}">
				{{ csrf_field() }}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
						Créer une Weweclasse
					</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="wewelevel" class="col-sm-2 control-label">Niveau</label>
						<div class="col-sm-10">
							<select name="wewelevel" class="form-control" id="wewelevel" onchange="showCourse();">
								@foreach($levels as $l)
									<option value="{{$l->id}}">{{$l->label_fr}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group" id="wewedivSelectCourse" style="display: none;">
						<label for="wewecourse" class="col-sm-2 control-label">Cours</label>
						<div class="col-sm-10">
							<select name="wewecourse" class="form-control" id="wewecourse">
								<option value="0"></option>
								@foreach($courses as $c)
									<option value="{{$c->id}}">{{$c->label_fr}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="wewedatepicker" class="col-sm-2 control-label">Date</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="wewedatepicker" name="wewedatepicker">
						</div>
					</div>
					<div id="wewecourse_div">
						<div class="form-group">
							<label for="wewetimepickerStart" class="col-sm-2 control-label">Début</label>
							<div class="col-sm-3">
								<select class="form-control" id="wewetimepickerStart" id="wewetimepickerStart">
									<option></option>
									<option value="8">8h</option>
									<option value="9">9h</option>
									<option value="10">10h</option>
									<option value="11">11h</option>
									<option value="12">12h</option>
									<option value="13">13h</option>
									<option value="14">14h</option>
									<option value="15">15h</option>
									<option value="16">16h</option>
									<option value="17">17h</option>
									<option value="18">18h</option>
									<option value="19">19h</option>
								</select>
							</div>
							<div id="wewedivAddHours" style="display: none">
								<label for="weweaddHours" class="col-sm-2 control-label">Durée</label>
								<div class="col-sm-2">
									<select id="weweaddHours" class="form-control" name="weweaddHours">
										<option value="1" selected>1h</option>
										<option value="2" >2h</option>
										<option value="3" >3h</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="wewedhstart" id="wewedhstart">
						</div>
						<div class="form-group">
							<label for="wewedhend" class="col-sm-2 control-label">Fin</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="wewetimepickerEnd" name="wewetimepickerEnd" disabled>
							</div>
							<input type="hidden" name="wewedhend" id="wewedhend">
						</div>
					</div>
					<div class="form-group">
						<label for="wewedescr" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10">
							<input type="text" name="wewedescr" class="form-control" id="wewedescr" placeholder="Description">
						</div>
					</div>
					<div class="form-group">
						<label for="weweaddress" class="col-sm-2 control-label">Adresse</label>
						<div class="col-sm-10">
							<input type="text" name="weweaddress" class="form-control" id="weweaddress" placeholder="Adresse">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="wewesubmitCreateEvent" class="btn btn-primary btn-sm">Créer</button>
					<button class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function (){
		$.datepicker.setDefaults($.datepicker.regional[""]);
        $("#wewedatepicker").datepicker($.datepicker.regional["fr"]);
		$('#wewetimepickerStart').on('change',function(e){
        	e.preventDefault();
        	$('#wewedivAddHours').css('display','');
            $('#weweaddHours').val(1);
            $('#weweaddHours option').attr('disabled',false);
            if($('#wewetimepickerStart').val() == '18')
            {
                $('#weweaddHours option[value="3"]').attr('disabled',true);
            }
            else if($('#wewetimepickerStart').val() == '19')
            {
                $('#weweaddHours option[value="2"]').attr('disabled',true);
                $('#weweaddHours option[value="3"]').attr('disabled',true);
            }
            changeEndTime();
        });
            
        
        //}});
        function changeEndTime()
        {
            $('#wewetimepickerEnd').val(parseInt($('#wewetimepickerStart').val()) + parseInt($('#weweaddHours').val()))
        }

        $('#weweaddHours').on('change', function(e)
        {
            e.preventDefault();
            changeEndTime();
        });
	});
	$('#wewesubmitCreateEvent').on('click', function(e){
		var t = $('#wewedatepicker').val() +' '+ $('#wewetimepickerStart').val();
            $("#wewedhstart").val(t);
            var t = $('#wewedatepicker').val() +' '+ $('#wewetimepickerEnd').val();
            $("#wewedhend").val(t);
	});
    function showCourse()
    {
        if($('#wewelevel').val() != 1)
            $('#wewedivSelectCourse').show();
        else $('#wewedivSelectCourse').hide();
    }
</script>