<div class="modal fade" id="ModalInfoAsbl" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="infoAsblTitle"></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label">Date et heure</label>
					<div id="infoAsblDate" name="infoAsblDate"></div>
					<div id="infoAsblTime" name="infoAsblTime"></div>
				</div>
				<div class="form-group">
					<label class="control-label">Adresse</label>
					<div id="infoAsblAddress" name="infoAsblAddress"></div>
				</div>
				<div class="form-group">
					<label class="control-label">Description</label>
					<div id="infoAsblDescription" name="infoAsblDescription"></div>
				</div>
				<input type="hidden" name="event_id" class="form-control" id="event_id">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-sm" id="infoAsblContactBtn">Demander plus d'informations</button>
				<button class="btn btn-default btn-sm" data-dismiss="modal">Fermer</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('#infoAsblContactBtn').on('click', function(e){
		swal({
            title: "Demande plus d'information",
            text: '<textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
            html: true,
            showCancelButton: true,
            closeOnConfirm: true,
            closeOnCancel: true,
            confirmButtonText:'Envoyer',
            cancelButtonText:'Annuler'
        }, function(inputValue) {
            if(inputValue) {
                var data = {};
                var arr = [];
                if (inputValue != "") {
                    var value = $('#text').val();
                    data['message'] = value;

                }
                arr.push(data);
                $.ajax({
                    'url': '/moreInfo/' + $('#event_id').val(),
                    'type': 'POST',
                    'data': arr[0],
                    'success': function (res) {
                        $('#ModalInfoAsbl').modal('hide');
                        console.log(res);
                    }
                });
            }
        });
	});
</script>