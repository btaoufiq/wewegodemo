<!-- Modal -->
<div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form class="form-horizontal" method="POST" action="{{ url('addEvent') }}">
				{{ csrf_field() }}
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">
						@if(Auth::user()->type==1) Proposer un cours
						@elseif(Auth::user()->type==0 || Auth::user()->type==4) Demander un cours
						@endif
					</h4>
				</div>
				<div class="modal-body">
					@if(Auth::user()->type==4)
					<div class="form-group">
						<label for="children" class="col-sm-2 control-label">Concerne</label>
						<div class="col-sm-10">
							<?php $mychildren = DB::table('children')->where('parent','=',Auth::user()->id)->get(); ?>
							<select class="form-control" id="children" name="children">
								<option></option>
								@foreach($mychildren as $child)
									<option value="{{$child->id}}">{{$child->first_name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					@endif
					<div class="form-group">
						<label for="course" class="col-sm-2 control-label">Niveau</label>
						<div class="col-sm-10">
							<select name="level" class="form-control" id="level">
								@foreach($levels as $l)
									<option value="{{$l->id}}">{{$l->label_fr}}</option>
								@endforeach
								{{--@foreach($user_level as $key => $level)
									<option value="{{$key}}">{{$level}}</option>
								@endforeach--}}
							</select>
						</div>
					</div>
					<div class="form-group" id="divSelectCourse" style="display: none;">
						<label for="course" class="col-sm-2 control-label">Cours</label>
						<div class="col-sm-10">
							<select name="course" class="form-control" id="course">
								@foreach($courses as $c)
									<option value="{{$c->id}}">{{$c->label_fr}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="dhstart" class="col-sm-2 control-label">Date</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="datepicker">
						</div>
					</div>
					<div id="course_div">
						<div class="form-group">
							<label for="dhstart" class="col-sm-2 control-label">Début</label>
							<div class="col-sm-3">
								<select class="form-control" id="timepickerStart">
									<option></option>
									<option value="8">8h</option>
									<option value="9">9h</option>
									<option value="10">10h</option>
									<option value="11">11h</option>
									<option value="12">12h</option>
									<option value="13">13h</option>
									<option value="14">14h</option>
									<option value="15">15h</option>
									<option value="16">16h</option>
									<option value="17">17h</option>
									<option value="18">18h</option>
									<option value="19">19h</option>
								</select>
							</div>
							<div id="divAddHours" style="display: none">
								<label for="addHours" class="col-sm-2 control-label">Durée</label>
								<div class="col-sm-2">
									<select id="addHours" class="form-control">
										<option value="1" selected>1h</option>
										<option value="2" >2h</option>
										<option value="3" >3h</option>
									</select>
								</div>
							</div>
							<input type="hidden" name="dhstart" id="dhstart">
						</div>
						<div class="form-group">
							<label for="dhend" class="col-sm-2 control-label">Fin</label>
							<div class="col-sm-2">
								<input type="text" class="form-control" id="timepickerEnd" disabled>
							</div>
							<input type="hidden" name="dhend" id="dhend">
						</div>
					</div>
					<div class="form-group">
						<label for="descr" class="col-sm-2 control-label">Description</label>
						<div class="col-sm-10">
							<input type="text" name="descr" class="form-control" id="descr" placeholder="Description">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-10">
						<span style="float: left; display: inline-block;">
							<input type="checkbox" id="student_home" name="student_home" value="1"> 
							<label for="student_home" class="control-label">Domicile élève</label>
							&emsp;
							<input type="checkbox" id="teacher_home" name="teacher_home" value="1"> 
							<label for="teacher_home" class="control-label">Domicile professeur</label>
						</span>
							<span style="float: right; display: inline-block; margin-right: 10px">
							<input type="checkbox" id="private" name="private" value="1"> 
							<label for="private" class="control-label">Cours particulier</label>
						</span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="submitCreateEvent" class="btn btn-primary btn-sm">Créer</button>
					<button class="btn btn-default btn-sm" data-dismiss="modal">Annuler</button>
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">
    $(document).ready(function ()
    {
        $.datepicker.setDefaults($.datepicker.regional[""]);
        $("#datepicker").datepicker($.datepicker.regional["fr"]);
        $('#timepickerStart').on('change',function(e){
        	e.preventDefault();
        	$('#divAddHours').css('display','');
            $('#addHours').val(1);
            $('#addHours option').attr('disabled',false);
            if($('#timepickerStart').val() == '18')
            {
                $('#addHours option[value="3"]').attr('disabled',true);
            }
            else if($('#timepickerStart').val() == '19')
            {
                $('#addHours option[value="2"]').attr('disabled',true);
                $('#addHours option[value="3"]').attr('disabled',true);
            }
            changeEndTime();
        });
        function changeEndTime()
        {
            $('#timepickerEnd').val(parseInt($('#timepickerStart').val()) + parseInt($('#addHours').val()))
        }

        $('#addHours').on('change', function(e)
        {
            e.preventDefault();
            changeEndTime();
        });

    });
    $("#submitCreateEvent").on('click', function(){
        var t = $('#datepicker').val() +' '+ $('#timepickerStart').val();
        $("#dhstart").val(t);
        var t = $('#datepicker').val() +' '+ $('#timepickerEnd').val();
        $("#dhend").val(t);
    });
    $('#level').on('change',function(e){
    	console.log('change level');
        if($('#level').val() != 1)
        	$('#divSelectCourse').show();
        else $('#divSelectCourse').hide();
    });
</script>