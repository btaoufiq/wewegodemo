<!-- Modal -->
<div class="modal fade" id="doesNotSuitModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form class="form-horizontal" method="POST" action="{{ url('/suitEvent') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Définir un nouveau rendez-vous</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group" id="firstDiv">
                        <label for="suitDhstart" class="col-sm-2 control-label">Date</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="suitDatepicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="suitDhstart" class="col-sm-2 control-label">Début</label>
                        <div class="col-sm-3">
                            <select class="form-control" id="suitTimepickerStart">
                                <option></option>
                                <option value="8">8h</option>
                                <option value="9">9h</option>
                                <option value="10">10h</option>
                                <option value="11">11h</option>
                                <option value="12">12h</option>
                                <option value="13">13h</option>
                                <option value="14">14h</option>
                                <option value="15">15h</option>
                                <option value="16">16h</option>
                                <option value="17">17h</option>
                                <option value="18">18h</option>
                                <option value="19">19h</option>
                            </select>
                        </div>
                        <div>
                            <label for="suitHours" class="col-sm-2 control-label">Durée</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" name="suitHours" id="suitHours" disabled>
                            </div>
                        </div>
                        <input type="hidden" name="suitDhstart" id="suitDhstart">
                    </div>
                    <div class="form-group">
                        <label for="suitDhEnd" class="col-sm-2 control-label">Fin</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" id="suitTimepickerEnd" disabled>
                        </div>
                        <input type="hidden" name="suitDhEnd" id="suitDhEnd">
                    </div>
                    <div class="form-group">
                        <label for="suitReason" class="col-sm-2 control-label">Message</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="suitReason" name="suitReason" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>
                        </div>
                    </div>
                    <div class="form-group hideDiv" id="divSuitPrivate">
                        <label for="suitPrivate" class="col-sm-2 control-label"></label>
                        <div class="col-sm-9">
                            &emsp;<input type="checkbox" id="suitPrivate" name="suitPrivate" value="suitPrivate">
                            &emsp;<label for="suitPrivate" class="control-label">Rendre le cours privé</label>
                        </div>
                    </div>
                    <div class="form-group hideDiv" id="divSuitGetConfirmed">
                        <label for="suitGetConfirmed" class="col-sm-2 control-label"></label>
                        <div class="col-sm-9">
                            &emsp;<input type="checkbox" id="suitGetConfirmed" name="suitGetConfirmed" value="suitGetConfirmed">
                            &emsp;<label for="suitGetConfirmed" class="control-label">Reprendre les participants confirmés</label>
                        </div>
                    </div>
                    <input type="hidden" name="suitid" class="form-control" id="suitid">
                    <input type="hidden" name="suitOwner" class="form-control" id="suitOwner">
                    <input type="hidden" name="suitHoursReq" class="form-control" id="suitHoursReq">
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitSuitEvent" class="btn btn-primary">Soumettre</button>
                    <button class="btn btn-default" data-dismiss="modal">Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var text_max = $('#suitReason').attr('maxlength');
        $('#count_message').html(text_max + ' caractères restants');
        $('#suitReason').keydown(function() {
            var text_length = $('#suitReason').val().length;
            var text_remaining = text_max - text_length;

            $('#count_message').html(text_remaining + ' caractères restants');
        });
        $.datepicker.setDefaults($.datepicker.regional[""]);
        $("#suitDatepicker").datepicker($.datepicker.regional["fr"]);
        $('#suitTimepickerStart').timepicker({
            'timeFormat': 'HH', 'minTime': '8:00 am', 'maxTime': '8:00 pm', onSelect: function () {
                $('#suitHours').css('display','');
                $('#suitHours').val(1);
                $('#suitHours option').attr('disabled',false);
                if($('#suitTimepickerStart').val() == 18)
                {
                    $('#suitHours option[value="3"]').attr('disabled',true);
                }
                else if($('#suitTimepickerStart').val() == 19)
                {
                    $('#suitHours option[value="2"]').attr('disabled',true);
                    $('#suitHours option[value="3"]').attr('disabled',true);
                }
                changeEndTime();
            }
        });
        function changeEndTime()
        {
            $('#suitTimepickerEnd').val(parseInt($('#suitTimepickerStart').val()) + parseInt($('#suitHours').val()))
        }

        $('#suitTimepickerStart').on('change', function(e)
        {
            e.preventDefault();
            changeEndTime();
        });
    });

    $("#submitSuitEvent").on('click', function(){
        var t = $('#suitDatepicker').val() +' '+ $('#suitTimepickerStart').val();
        $("#suitDhstart").val(t);
        var t = $('#suitDatepicker').val() +' '+ $('#suitTimepickerEnd').val();
        $("#suitDhEnd").val(t);
        $('#suitHoursReq').val($('#suitHours').val());
    });

</script>