<div id="divCreateTeacher" class="showingDiv hideDiv">
	<span class="closeDivIcon" style="float: right; cursor: pointer;font-size: 14pt;" title="Fermer"><i class="fa fa-times" aria-hidden="true"></i></span>
	<legend class="text-center">Inscrire un professeur</legend>
	@if(session()->has('message'))
		<div id="successMessage" class="alert alert-success">
			{{ session()->get('message') }}
		</div>
	@endif
	<form class="form-horizontal" method="POST" action="{{ url('asbl/createTeacher') }}" id="formCreateTeacher">
		{{ csrf_field() }}
		<fieldset>
			<div class="form-group">
				<label for="last_name" class="col-md-3 control-label">Nom</label>
				<div class="col-md-8">
					<input id="last_name" type="text" class="form-control" name="last_name" required>
				</div>
			</div>
			<div class="form-group">
				<label for="first_name" class="col-md-3 control-label">Prénom</label>
				<div class="col-md-8">
					<input id="first_name" type="text" class="form-control" name="first_name" required>
				</div>
			</div>

			<div class="form-group">
				<label for="email" class="col-md-3 control-label">Addresse email</label>
				<div class="col-md-8">
					<input id="email" type="email" class="form-control" name="email" required>
				</div>
			</div>

			<div class="form-group">
				<label for="level" class="col-md-3 control-label">Niveau</label>
				<div class="col-md-8" style="line-height:75%;">
					<input type="checkbox" id="primaireChk" name="level[]" value="1">
					<label for="primaireChk" style="font-weight:normal">Primaire</label>
					&emsp;
					<input type="checkbox" id="secInfChk" name="level[]" value="2">
					<label for="secInfChk" style="font-weight:normal">Secondaire inférieur</label>
					&emsp;
					<input type="checkbox" id="secSupChk" name="level[]" value="3">
					<label for="secSupChk" style="font-weight:normal">Secondaire supérieur</label>
				</div>
			</div>
			<div class="form-group">
				<label for="course" class="col-md-3 control-label">Matière(s)</label>
				<div class="col-md-8" style="line-height:75%;">
					<input type="checkbox" id="edmChk" name="course[]" value="1">
					<label for="emdChk" style="font-weight:normal">EDM</label>
					<br/>
					<input type="checkbox" id="mathChk" name="course[]" value="2">
					<label for="mathChk" style="font-weight:normal">Math</label>&emsp;
					<input type="checkbox" id="francaisChk" name="course[]" value="3">
					<label for="francaisChk" style="font-weight:normal">Français</label>
					<br/>
					<input type="checkbox" id="neerlandaisChk" name="course[]" value="4">
					<label for="neerlandaisChk" style="font-weight:normal">Néerlandais</label>&emsp;
					<input type="checkbox" id="anglaisChk" name="course[]" value="5">
					<label for="anglaisChk" style="font-weight:normal">Anglais</label>
					<br/>
					<input type="checkbox" id="chimieChk" name="course[]" value="6">
					<label for="chimieChk" style="font-weight:normal">Chimie</label>&emsp;
					<input type="checkbox" id="biologieChk" name="course[]" value="7">
					<label for="biologieChk" style="font-weight:normal">Biologie</label>&emsp;
					<input type="checkbox" id="physiqueChk" name="course[]" value="8">
					<label for="physiqueChk" style="font-weight:normal">Physique</label>
				</div>
			</div>

			<?php $localities = DB::table('localities')->get(); ?>
			<div class="form-group">
				<label for="localite" class="col-md-3 control-label">Localité</label>
				<div class="col-md-8">
					<select class="form-control" id="localite" name="localite">
						@foreach($localities as $l)
							<option value="{{$l->code}}">
								{{$l->code}} &#8211; {{$l->label_fr}}
								@if(!empty($l->label_nl))
									/ {{$l->label_nl}}
								@endif
							</option>
						@endforeach
					</select>
				</div>
			</div>

			<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
				<label for="phone" class="col-md-3 control-label">Téléphone</label>
				<div class="col-md-8">
					<input id="phone" type="text" class="form-control" name="phone">
				</div>
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-3">
					<button type="submit" class="btn btn-primary">
						Inscrire le professeur
					</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function() {
		  $('#successMessage').fadeOut('fast');
		}, 3000);
	});
</script>