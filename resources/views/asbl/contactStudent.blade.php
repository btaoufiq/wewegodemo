<div id="divContactStudent" class="showingDiv hideDiv">
	<span class="closeDivIcon" style="float: right; cursor: pointer;font-size: 14pt;" title="Fermer"><i class="fa fa-times" aria-hidden="true"></i></span>
	<legend class="text-center">Suivi élèves</legend>
	<table class="table table-hover table-bordered table-list">
		@foreach($allMessages as $notification)
			@foreach($notification->messages as $message)
				<tr>
					<td class="col-md-9">
						<div class="notificationDateTime">{{ date('d/m/Y', strtotime($notification->created_at)) }}, {{ date('H:i', strtotime($notification->created_at)) }}</div>
						<p>
							<i class="fa fa-comments-o"></i>
							{{ $message->sender->first_name.' '.$message->sender->last_name.' : ' }}
							<b>{{ $message->description }}</b>
						</p>
					</td>
					<td style="vertical-align: middle;text-align: center;" class="col-md-2">
						<button class="btn btn-primary btn-xs btnVoirFiche" id="btnVoirFiche{{$notification->id}}" data-event_id="{{$notification->event_id}}" data-notification_id="{{$notification->id}}">Détails du cours</button>
					</td>
					<td style="vertical-align: middle;text-align: center;" class="col-md-1">
						<button class="btn btn-success btn-xs btnReply" data-id="{{$message->id}}" title="Pas encore disponible">Répondre</button>
					</td>
				</tr>
				<tr class="hideDiv" id="divInfoEvent{{$notification->id}}"></tr>
			@endforeach
		@endforeach
	</table>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
		$('.btnVoirFiche').on('click', function(e){
			e.preventDefault();
			var event_id = $(this).data('event_id');
			var notification_id = $(this).data('notification_id');
			$('#divInfoEvent'+notification_id).fadeToggle(500);
			$('#divInfoEvent'+notification_id).toggleClass('hideDiv');
			if(!$('#divInfoEvent'+notification_id).hasClass('hideDiv')){
				$(this).text('Masquer');
				$.ajax({
	                'url': 'asbl/getSingleEvent/' + event_id,
	                'method': 'POST',
	                'success': function (res) {
	                    $('#divInfoEvent'+notification_id).html("<td colspan='3' style='padding-left:15pt;padding-bottom:0pt;font-size:11pt;color:#245580'><p>"+res['course_label'].label_fr+" &#8211; "+res['level_label'].label_fr+"</p><p>Le "+moment(res['event'].dhstart).format('DD/MM/YYYY')+" de "+moment(res['event'].dhstart).format('H')+"h à "+moment(res['event'].dhend).format('H')+"h. À l'adresse : "+res['event'].address+"</p><p><i>"+res['event'].descr+"</i></p></td>");
	                }
	            });
			}
			else $(this).text('Détails du cours');
		});
		$('.btnReply').on('click',function(e){
			e.preventDefault();
			var messageId = $(this).data('id');
			swal({
                title: 'Répondre',
                text: '<textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
                html: true,
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true,
                confirmButtonText:'Répondre',
                cancelButtonText:'Annuler'
            }, function(inputValue) {
                if(inputValue) {
                    var value = $('#text').val();
                    data = {'message': value};
                    /**** Ajax to do ****/
                    /*$.ajax({
                        'url': '/.../' + messageId,
                        'type': 'POST',
                        'data': data,
                        'success': function (res) {
                            
                        }
                    });*/
                }
            });
		});
	});
</script>