<div id="divCreateEvent" class="showingDiv hideDiv">
	{{--
		permanences : prossibilté de créer des events récurents avec exclusion de dates
		exemple: lundi:math&francais, mardi:science, ...
		dans le calendrier: option dispo : demander info -> mail/message/notif à l'asbl
	--}}
	<span class="closeDivIcon" style="float: right; cursor: pointer;font-size: 14pt;" title="Fermer"><i class="fa fa-times" aria-hidden="true"></i></span>
	<legend class="text-center">Permanences de l'ASBL</legend>
	<form class="form-horizontal" method="POST" action="{{ url('asbl/createEvent') }}">
		{{ csrf_field() }}
		<fieldset>
			<div class="form-group">
				<label for="course" class="col-xs-2 control-label">Niveau</label>
				<div class="col-xs-3">
					<?php $levels = DB::table('levels')->get(); ?>
					<select name="level" class="form-control" id="level" onchange="showCourse()">
						@foreach($levels as $l)
							<option value="{{$l->id}}">{{$l->label_fr}}</option>
						@endforeach
					</select>
				</div>
				<div id="divSelectCourse">
					<label for="course" class="col-xs-2 control-label">Cours</label>
					<div class="col-xs-3">
						<?php $courses = DB::table('courses')->get(); ?>
						<select name="course" class="form-control" id="course" disabled="disabled">
							<option></option>
							@foreach($courses as $c)
								<option value="{{$c->id}}">{{$c->label_fr}}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div id="divNoRecurr">
				<div class="form-group">
					<label for="dhstart" class="col-xs-2 control-label">Date</label>
					<div class="col-xs-2">
						<input type="text" class="form-control" id="datepicker">
					</div>
				</div>
				<div class="form-group">
					<label for="dhstart" class="col-xs-2 control-label">Début</label>
					<div class="col-xs-2">
						<select class="form-control" id="timepickerStart">
							<option></option>
							<option value="8">8h</option>
							<option value="9">9h</option>
							<option value="10">10h</option>
							<option value="11">11h</option>
							<option value="12">12h</option>
							<option value="13">13h</option>
							<option value="14">14h</option>
							<option value="15">15h</option>
							<option value="16">16h</option>
							<option value="17">17h</option>
							<option value="18">18h</option>
							<option value="19">19h</option>
						</select>
					</div>
					<label for="dhend" class="col-xs-1 control-label">Fin</label>
					<div class="col-xs-1">
						<input type="text" class="form-control" id="timepickerEnd"  disabled>
					</div>
					<div id="divAddHours" style="display: none">
						<label for="addHours" class="col-xs-2 control-label">Durée</label>
						<div class="col-xs-2">
							<select id="addHours" class="form-control">
								<option value="1" selected>1h</option>
								<option value="2" >2h</option>
								<option value="3" >3h</option>
							</select>
						</div>
					</div>
					<input type="hidden" name="dhstart" id="dhstart">
					<input type="hidden" name="dhend" id="dhend">
				</div>
			</div>
			<div class="form-group">
				<label for="descr" class="col-xs-2 control-label">Description</label>
				<div class="col-xs-8">
					<textarea type="text" name="descr" class="form-control" id="descr" placeholder="Description / Information / Commentaire"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="address" class="col-xs-2 control-label">Adresse</label>
				<div class="col-xs-8">
					<input type="text" name="address" class="form-control" id="address">
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-6 col-md-offset-3">
					<button type="submit" class="btn btn-primary" id="submitAsblCreateEvent">
						Créer le cours
					</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-ui-timepicker-addon@1.6.3/dist/jquery-ui-timepicker-addon.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-ui-timepicker-addon@1.6.3/dist/i18n/jquery-ui-timepicker-fr.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-ui-timepicker-addon@1.6.3/dist/jquery-ui-timepicker-addon.css">
    <script src="{{ asset('js/datepicker-fr.js') }}"></script>
    <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function ()
	{
		$.datepicker.setDefaults($.datepicker.regional[""]);
		$("#datepicker").datepicker($.datepicker.regional["fr"]);
		/*$('#timepickerStart').timepicker({'timeFormat':'HH','minTime':'8:00 am','maxTime':'8:00 pm',onSelect:function()
		{
			$('#addHours').css('display','');
			$('#addHours').val(1);
			$('#addHours option').attr('disabled',false);
			if($('#timepickerStart').val() == 18)
			{
				$('#addHours option[value="3"]').attr('disabled',true);
			}
			else if($('#timepickerStart').val() == 19)
			{
				$('#addHours option[value="2"]').attr('disabled',true);
				$('#addHours option[value="3"]').attr('disabled',true);
			}
			changeEndTime();
		}});*/
		$('#timepickerStart').on('change',function(e){
        	e.preventDefault();
        	$('#divAddHours').css('display','');
            $('#addHours').val(1);
            $('#addHours option').attr('disabled',false);
            if($('#timepickerStart').val() == '18')
            {
                $('#addHours option[value="3"]').attr('disabled',true);
            }
            else if($('#timepickerStart').val() == '19')
            {
                $('#addHours option[value="2"]').attr('disabled',true);
                $('#addHours option[value="3"]').attr('disabled',true);
            }
            changeEndTime();
        });
		function changeEndTime()
		{
			$('#timepickerEnd').val(parseInt($('#timepickerStart').val()) + parseInt($('#addHours').val()))
		}

		$('#addHours').on('change', function(e)
		{
			e.preventDefault();
			changeEndTime();
		});

	});
	$("#submitAsblCreateEvent").on('click', function(){
		var t = $('#datepicker').val() +' '+ $('#timepickerStart').val();
		$("#dhstart").val(t);
		var t = $('#datepicker').val() +' '+ $('#timepickerEnd').val();
		$("#dhend").val(t);
	});
	function showCourse()
	{
		/*if($('#level').val() != 1)
			$('#divSelectCourse').show();
		else $('#divSelectCourse').hide();*/
		if($('#level').val() != 1) $('#course').attr('disabled',false);
		else  $('#course').attr('disabled',true);
	}
	function showDiv(){
        if($("input[name=recurrEvent]:checked").val()=='0')
        {
        	$('#divNoRecurr').removeClass('hideDiv');
        	$('#divRecurr').addClass('hideDiv');
        }
        else
        {
        	$('#divRecurr').removeClass('hideDiv');
        	$('#divNoRecurr').addClass('hideDiv');
        }
    }
</script>