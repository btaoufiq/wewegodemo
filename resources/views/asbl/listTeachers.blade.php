<style type="text/css">
	.boldText{
		font-weight: bold;
		font-size: large;
	}
</style>
<div id="divListTeachers" class="showingDiv hideDiv">
	{{--
		
		liste professeurs
		possibilté de donner un avertissement, modifier le profil, supprimer(envoi une demande de suppression du user aux admins)

		https://bootsnipp.com/snippets/featured/panel-table-with-filters-per-column
		https://bootsnipp.com/snippets/featured/condensed-table-example
		https://bootsnipp.com/snippets/featured/easy-table-filter

	--}}
	<span class="closeDivIcon" style="float: right; cursor: pointer;font-size: 14pt;" title="Fermer"><i class="fa fa-times" aria-hidden="true"></i></span>
	<legend class="text-center">Liste des professeurs</legend>
	@if(session()->has('message'))
		<div id="successMessage" class="alert alert-success">
			{{ session()->get('message') }}
		</div>
	@endif
	<?php $sqlResult = DB::table('users')->where('type','=','1')->get(); ?>
	<table class="table table-hover table-bordered table-list">
		<tbody>
			@if(count($sqlResult) == 0)
				<tr>
					<td colspan="2" style="font-style: italic; text-align: center;">Aucun professeur trouvé</td>
				</tr>
			@else
				@foreach($sqlResult as $t)
					<tr id="row{{$t->id}}" style="cursor: pointer">
						<td class="clickable-row col-md-9">
							<span>{{$t->first_name}} {{$t->last_name}}</span>
							<div class="hideDiv" style="font-size:11pt;color:#245580;padding-left: 10pt;padding-top: 10pt;">
								Email : {{$t->email}}<br/>
								<?php $localities = DB::table('localities')->where('code', '=', $t->localite)->get()->first(); ?>
								Localité : {{$t->localite}} {{$localities->label_fr}}<br/>
								Téléphone : {{$t->phone}}<br>
								<div style="padding: 10;">
									<span style="display: inline-block;float: left;">
										<?php $user_level = DB::table('user_level')->where('user_id','=',$t->id)->get()->first(); ?>
										@if($user_level->primaire==1) <i class="fa fa-angle-double-right" aria-hidden="true"></i> Primaire<br> @endif
										@if($user_level->sec_inf==1) <i class="fa fa-angle-double-right" aria-hidden="true"></i> Secondaire inférieur<br> @endif
										@if($user_level->sec_sup==1) <i class="fa fa-angle-double-right" aria-hidden="true"></i> Secondaire supérieur<br> @endif
									</span>
									<span style="display: inline-block;padding-left: 100;">
										<?php $user_course = DB::table('user_course')->where('user_id','=',$t->id)->get()->first(); ?>
										@if($user_course->edm==1) <i class="fa fa-angle-right" aria-hidden="true"></i> EDM<br> @endif
										@if($user_course->math==1) <i class="fa fa-angle-right" aria-hidden="true"></i> Math<br> @endif
										@if($user_course->francais==1) <i class="fa fa-angle-right" aria-hidden="true"></i> Français<br> @endif
										@if($user_course->neerlandais==1) <i class="fa fa-angle-right" aria-hidden="true"></i> Néerlandais<br> @endif
										@if($user_course->anglais==1) <i class="fa fa-angle-right" aria-hidden="true"></i> Anglais<br> @endif
										@if($user_course->chimie==1) <i class="fa fa-angle-right" aria-hidden="true"></i> Chimie<br> @endif
										@if($user_course->biologie==1) <i class="fa fa-angle-right" aria-hidden="true"></i> Biologie<br> @endif
										@if($user_course->physique==1) <i class="fa fa-angle-right" aria-hidden="true"></i> Physique<br> @endif
									</span>
								</div>
							</div>
						</td>
						<td class="col-md-3">
							<a href="#" class="label label-primary" data-toggle="modal" data-teacher_id="{{$t->id}}" data-teacher_first_name="{{$t->first_name}}" data-teacher_last_name="{{$t->last_name}}" data-teacher_email="{{$t->email}}" data-teacher_locality="{{$t->localite}}" data-teacher_phone="{{$t->phone}}" data-target="#modalEditTeacher">Modifier</a>&#160;
							<a href="#" class="label label-warning">Avertissement</a>&#160;
							<a href="#" class="label label-danger">Supprimer</a>
						</td>
					</tr>
					@include('./modals/editTeacher')
				@endforeach
			@endif
		</tbody>
	</table>
</div>

<script type="text/javascript">
$(document).ready(function() {
	setTimeout(function() {
	  $('#successMessage').fadeOut('fast');
	}, 3000);
	$('.clickable-row').on('click', function(){
		$(this).children('span').toggleClass('boldText');
		$(".clickable-row").not($(this)).children('span').removeClass('boldText');
		$(".clickable-row").not($(this)).children('div').hide(500);
        $(this).children('div').slideToggle(500);
	});
});
</script>