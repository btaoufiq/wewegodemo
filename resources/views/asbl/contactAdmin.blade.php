<div id="divContactAdmin" class="showingDiv hideDiv">
	<span class="closeDivIcon" style="float: right; cursor: pointer;font-size: 14pt;" title="Fermer"><i class="fa fa-times" aria-hidden="true"></i></span>
	<legend class="text-center">Contacter les administrateurs</legend>
	<form class="form-horizontal" action="" method="post" id="formContactAdmin">
		{{ csrf_field() }}
		<fieldset>
			
			<!-- Name input-->
			<div class="form-group">
				<label class="col-md-3 control-label" for="name">Votre nom</label>
				<div class="col-md-8">
					<input id="name" name="name" type="text" class="form-control" value="{{ $user['first_name'] }} {{ $user['last_name'] }}" required>
				</div>
			</div>

			<!-- Email input-->
			<div class="form-group">
				<label class="col-md-3 control-label" for="mail">Votre adresse email</label>
				<div class="col-md-8">
					<input id="mail" name="mail" type="text" class="form-control" value="{{ $user['email'] }}" required>
				</div>
			</div>

			<!-- Message body -->
			<div class="form-group">
				<label class="col-md-3 control-label" for="message">Votre message</label>
				<div class="col-md-8">
					<textarea class="form-control" id="message" name="message" placeholder="Tapez votre message ici..." rows="5" required></textarea>
				</div>
			</div>

			<!-- Form actions -->
			<div class="form-group">
				<div class="col-md-11 text-right">
					<button type="submit" class="btn btn-primary" id="btnSumbitContactAdminForm">Envoyer</button>
				</div>
			</div>
		</fieldset>
	</form>
</div>

<script>
$(document).ready(function() {
	$("#btnSumbitContactAdminForm").click(function(e){
		e.preventDefault();
		if($('#name').val().length!=0 && $('#mail').val().length!=0 && $('#message').val().length!=0){
			swal({
				title: "Message envoyé",
				text: " ",
				icon: "success",
				timer: 2000,
				button: false,
			});
			//$('#formContactAdmin').submit();
		}
		else{
			swal({
				title: "Attention !",
				text: "Vous devez remplir tous les champs",
				icon: "warning",
			});
		}
	});
});
</script>