<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <link href='https://fonts.googleapis.com/css?family=ABeeZee' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Alef' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Anaheim' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Archivo' rel='stylesheet'>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sweetalert.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive-grid-of-hexagons-master/hexagons.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fullcalendar-3.6.2.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/events.js') }}"></script>
    {{--<script src="https://use.fontawesome.com/9b862c8194.js"></script> FontAwesome 4 --}}
    {{--<script type="text/javascript" src="{{ asset('js/fontawesome-all.js') }}"></script> FontAwesome 5 --}}
    <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet"> <!-- FontAwesome 4 en local -->

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-ui-timepicker-addon@1.6.3/dist/jquery-ui-timepicker-addon.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-ui-timepicker-addon@1.6.3/dist/i18n/jquery-ui-timepicker-fr.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-ui-timepicker-addon@1.6.3/dist/jquery-ui-timepicker-addon.css">
    <script src="{{ asset('js/datepicker-fr.js') }}"></script>
    <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>


    <style type="text/css">
        body {
            font-family: 'Archivo';
            overflow: auto;
            height: 100%;
        }
        html {
            overflow: hidden;
            height: 100%;
        }
        .modal-open {
            overflow: auto; 
        }
        .notificationDateTime{
            font-size: 12px; 
            font-style: italic;
        }
        .hideDiv{
            display: none;
        }
        .weweRowHighlight{
            border-left: 5pt solid #ff5a00;
        }
        .teacherRowHighlight{
           /* border-left: 2.5pt solid #1E7ACE;*/
        }
        .studentRowHighlight{
            /*border-left: 2.5pt solid #FFF50F;*/
        }
        .confirmedRowHighlight{
            background-color: #dff0d8;
            border-left: 5pt solid #4cae4c;
        }
        .notyetconfirmedRowHighlight{
            background-color: #fcf8e3;
            border-left: 5pt solid #eea236;
        }
        .ownerRowHighlight{
            background-color: #f7f7f7;
            border-left: 5pt solid #c5c5c5;
        }
        .addChildLink:hover{
            text-decoration: underline;
            color: #31708f;
        }
        .showInfoCard{
            cursor: pointer;
        }
    </style>
</head>
<body>
<div id="app" style="display: none;">
    <nav class="navbar navbar-default navbar-static-top" style="margin-bottom: 5pt;">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <!--<a class="navbar-brand" href="{{ url('/') }}">
                    <i class="fa fa-calendar" aria-hidden="true"></i>
                    {{ config('app.name') }}
                </a>-->
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>

                <!-- Right Side Of Navbar -->
                @auth
                <ul class="nav navbar-nav">
                    <li style="font-weight: bold;font-size: 1.2vw;cursor: pointer;">
                        <a id="getWewevueLink" href="{{ url('/') }}">WEWEGO</a>
                    </li>
                    @if(Request::is('/') or Request::is('mylist') or Request::is('calendar'))
                        @if(Auth::user()->type==3)
                        <li>
                            <button class="btn btn-default navbar-btn transition" onclick="window.location='{{ url("admin") }}'"><i class="fa fa-cogs" aria-hidden="true"></i> Administration</button>
                        </li>
                        <li>
                            <button class="btn btn-default navbar-btn" data-toggle="modal" data-target="#ModalAddWeweclass" style="margin-left: 15pt"><i class="fa fa-star" aria-hidden="true"></i> Weweclasse</button>
                        </li>
                        @endif
                        <li>
                            @if(Auth::user()->type==1)
                                <button class="btn btn-default navbar-btn" data-toggle="modal" data-target="#ModalAdd"><i class="fa fa-pencil" aria-hidden="true"></i> Proposer un cours</button>
                            @endif
                            @if(Auth::user()->type==0 || Auth::user()->type==4)
                                <button class="btn btn-default navbar-btn" data-toggle="modal" data-target="#ModalAdd"><i class="fa fa-pencil" aria-hidden="true"></i> Demander un cours</button>
                            @endif
                        </li>
                    @endif
                    @if(Auth::user()->type==2)
                        <li>
                            <button class="btn btn-default navbar-btn transition" onclick="window.location='{{ url("asbl") }}'"><i class="fa fa-users" aria-hidden="true"></i> Gérer mon ASBL</button>
                        </li>
                    @endif
                </ul>
                @endauth
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                    <li><a href="{{ route('login') }}">Se connecter</a></li>
                    <li><a href="{{ route('register') }}">S'inscrire</a></li>
                    @else
                        <li style="font-weight: bold;font-size: 1.2vw;cursor: pointer;">
                            <a href="{{ url('calendar') }}" id="getCalendarLink">Mon calendrier</a>
                        </li>
                        @if(Auth::user()->type==0 or Auth::user()->type==1 or Auth::user()->type==4)
                            <li style="font-weight: bold;font-size: 1.2vw;">
                                <a href="{{ url('mylist') }}" id="listNavbarLink">Mes cours
                                    @if($notification_count and !Request::is('mylist'))
                                        <span class="badge" style="font-size:10px;margin-top: -18px;margin-left: -6px;background-color:#ff5a00;">
                                            {{$notification_count}}
                                        </span>
                                    @endif
                                </a>
                            </li>
                        @endif
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">
                                {{ Auth::user()->first_name .' '. Auth::user()->last_name }}
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu" style="text-align: center;">
                                <li>
                                    <a href="{{ url('profil') }}">Mon profil</a>
                                </li>
                                @if(Auth::user()->type==4)
                                <li>
                                    <a href="{{ url('children') }}">Gérer mes enfants</a>
                                </li>
                                @endif
                                <li role="separator" class="divider"></li>
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        Se déconnecter
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        @endguest
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')
    @auth
    @if(!Request::is('profil') && !Request::is('children') && !Request::is('changepassword'))
        @include('./modals/showInfoCardModal')
        @include('./modals/addEventModal')
        @include('./modals/editEventModal')
        @include('./modals/addWeweclassModal')
        @include('./modals/doesNotSuitModal')
        @include('./modals/detailedPricingModal')
    @endif
    @endauth
</div>

<script>
    $(document).ready(function () {
        $('#app').fadeIn(500);
        $('#app').css('display', '');
        $('.dropdown-toggle').dropdown();
        $(".transition").click(function(event){
            event.preventDefault();
            if($(event.target).is("a")) linkLocation = this.href;
            $("body").fadeOut(500, function(){
                document.location.href = linkLocation
            });      
        });
        $('.showInfoCard').click(function(e){
            e.preventDefault();
            $.ajax({
                'url': '/getInfoCard/' + $(this).data('id'),
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'failed') {
                        swal('Oops', res.message, 'error');
                    }
                    else if (res.status == 'success') {
                        showInfoCardModal(res);
                    }
                }
            });
        });
    });
</script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/sweetalert-dev.js') }}"></script>
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/fullcalendar-3.6.2.js') }}"></script>
</body>
</html>
