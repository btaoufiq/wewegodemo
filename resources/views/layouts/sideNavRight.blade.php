<div id="sideNavRight">
	<div style="float:right; width:20%; height:100%; display:inline-block; padding:10px; background-color:#fafafa;font-size:12px;">
        <div style="min-height:25%;max-height:33%">
            <label class="control-label">Prochaines Weweclasses</label> <span class="label label-primary" style="background-color:#ff5a00;float: right;">NEW</span>
            <div>
                @if(count($latestWeweclass['latestWeweclass']) == 0) <i>Aucun cours à afficher</i> @endif
                @foreach($latestWeweclass['latestWeweclass'] as $e)
                    <a title="{{ $e->level_label }}" id="newestEvent{{$e->id}}" class="newestEventClick" style="cursor: pointer;" 
                        data-toggle="modal" 
                        data-target="#ModalEdit" 
                        data-id="{{$e->id}}" 
                        data-owner="{{$e->owner}}" 
                        data-descr="{{$e->descr}}" 
                        data-start="{{ $e->dhstart }}" 
                        data-end="{{ $e->dhend }}" 
                        data-level="{{ $e->level_id }}" 
                        data-level_label="{{ $e->level_label }}" 
                        data-course="{{ $e->course_id }}" 
                        data-course_label="{{ $e->course_label }}" 
                        data-student_home="{{ $e->student_home }}" 
                        data-teacher_home="{{ $e->teacher_home }}" 
                        data-private="{{ $e->private }}" 
                        data-color="{{ $e->color }}" 
                        data-user_type="{{ $e->user_type }}" 
                        data-enrolled="{{ $e->enrolled }}" 
                        data-hours="{{ $e->duration }}" 
                        data-enrolled_user_id="{{ $e->enrolled_user_id }}"
                        data-enrolled_status="{{ $e->enrolled_status }}" 
                        data-teacher="" 
                        data-students="{{ $latestWeweclass['students'][$e->id] }}" 
                        data-type="{{ $e->type }}" 
                        data-address="{{ $e->address }}" 
                        >{{ $e->course_label == null ? $e->level_label : $e->course_label }}</a> : {{ date('d/m/Y', strtotime($e->dhstart)) }}
                    <br/>
                @endforeach
            </div>
        </div>
        {{--<div style="min-height:25%;max-height:33%">
            <label class="control-label">Dernières permanences ASBL</label> <span class="label label-primary">NEW</span>
            <div>
                @if(count($latestAsblEvents) == 0) <i>Aucun cours à afficher</i> @endif
                @foreach($latestAsblEvents as $e)
                    <a title="{{ $e->level_label }}" id="newestEvent{{$e->id}}" class="newestEventClick" style="cursor: pointer;" 
                        data-toggle="modal" 
                        data-target="#ModalEdit" 
                        data-id="{{$e->id}}" 
                        data-owner="{{$e->owner}}" 
                        data-descr="{{$e->descr}}" 
                        data-start="{{ $e->dhstart }}" 
                        data-end="{{ $e->dhend }}" 
                        data-level="{{ $e->level_id }}" 
                        data-level_label="{{ $e->level_label }}" 
                        data-course="{{ $e->course_id }}" 
                        data-course_label="{{ $e->course_label }}" 
                        data-student_home="{{ $e->student_home }}" 
                        data-teacher_home="{{ $e->teacher_home }}" 
                        data-private="{{ $e->private }}" 
                        data-color="{{ $e->color }}" 
                        data-user_type="{{ $e->user_type }}" 
                        data-enrolled="{{ $e->enrolled }}" 
                        data-hours="{{ $e->duration }}" 
                        data-enrolled_user_id="{{ $e->enrolled_user_id }}"
                        data-enrolled_status="{{ $e->enrolled_status }}" 
                        data-teacher="" 
                        data-students="" 
                        data-type="{{ $e->type }}" 
                        data-address="{{ $e->address }}" 
                        >{{ $e->course_label == null ? $e->level_label : $e->course_label }}</a> : {{ date('d/m/Y', strtotime($e->dhstart)) }}
                    <br/>
                @endforeach
            </div>
        </div>--}}
		<div style="min-height:25%;max-height:33%">
			<label class="control-label">Derniers cours proposés</label> <span class="label label-primary" style="float: right;">NEW</span>
			<div>
                @if(count($latestCreatedEvents['latestCreatedEvents']) == 0) <i>Aucun cours à afficher</i> @endif
				@foreach($latestCreatedEvents['latestCreatedEvents'] as $e)
					<a title="{{ $e->level_label }}" id="newestEvent{{$e->id}}" class="newestEventClick" style="cursor: pointer;" 
						data-toggle="modal" 
						data-target="#ModalEdit" 
						data-id="{{$e->id}}" 
						data-owner="{{$e->owner}}" 
						data-descr="{{$e->descr}}" 
						data-start="{{ $e->dhstart }}" 
						data-end="{{ $e->dhend }}" 
						data-level="{{ $e->level_id }}" 
                        data-level_label="{{ $e->level_label }}" 
						data-course="{{ $e->course_id }}" 
                        data-course_label="{{ $e->course_label }}" 
						data-student_home="{{ $e->student_home }}" 
						data-teacher_home="{{ $e->teacher_home }}" 
						data-private="{{ $e->private }}" 
						data-color="{{ $e->color }}" 
						data-user_type="{{ $e->user_type }}" 
						data-enrolled="{{ $e->enrolled }}" 
						data-hours="{{ $e->duration }}" 
                        data-enrolled_user_id="{{ $e->enrolled_user_id }}"
                        data-enrolled_status="{{ $e->enrolled_status }}" 
                        data-teacher="{{ $latestCreatedEvents['teachers'][$e->id] }}" 
                        data-students="{{ $latestCreatedEvents['students'][$e->id] }}" 
                        data-type="{{ $e->type }}" 
                        data-address="{{ $e->address }}" 
						>{{ $e->course_label == null ? $e->level_label : $e->course_label }}</a> : {{ date('d/m/Y', strtotime($e->dhstart)) }}
					@if($e->private == '1') <span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>
					@endif
                    @if($e->type=='1') <span title="Cours certifié Wewego" style="color:#ff5a00"><i class="fa fa-star" aria-hidden="true"></i></span>
                    @endif
					<br/>
				@endforeach
			</div>
		</div>
		<div style="min-height:25%;max-height:33%">
			<label class="control-label">Derniers cours demandés</label> <span class="label label-primary" style="float: right;">NEW</span>
			<div>
				@if(count($latestAskedEvents['latestAskedEvents']) == 0) <i>Aucun cours à afficher</i> @endif
                @foreach($latestAskedEvents['latestAskedEvents'] as $e)
					<a title="{{ $e->level_label }}" id="newestEvent{{$e->id}}" class="newestEventClick" style="cursor: pointer;" 
                        data-toggle="modal" 
                        data-target="#ModalEdit" 
                        data-id="{{$e->id}}" 
                        data-owner="{{$e->owner}}" 
                        data-child="{{$e->child}}" 
                        data-descr="{{$e->descr}}" 
                        data-start="{{ $e->dhstart }}" 
                        data-end="{{ $e->dhend }}" 
                        data-level="{{ $e->level_id }}" 
                        data-level_label="{{ $e->level_label }}" 
                        data-course="{{ $e->course_id }}" 
                        data-course_label="{{ $e->course_label }}" 
                        data-student_home="{{ $e->student_home }}" 
                        data-teacher_home="{{ $e->teacher_home }}" 
                        data-private="{{ $e->private }}" 
                        data-color="{{ $e->color }}" 
                        data-user_type="{{ $e->user_type }}" 
                        data-enrolled="{{ $e->enrolled }}" 
                        data-hours="{{ $e->duration }}" 
                        data-enrolled_user_id="{{ $e->enrolled_user_id }}"
                        data-enrolled_status="{{ $e->enrolled_status }}" 
                        data-teacher="{{ $latestAskedEvents['teachers'][$e->id] }}" 
                        data-students="{{ $latestAskedEvents['students'][$e->id] }}" 
                        data-type="{{ $e->type }}" 
                        data-address="{{ $e->address }}" 
                        >{{ $e->course_label == null ? $e->level_label : $e->course_label }}</a> : {{ date('d/m/Y', strtotime($e->dhstart)) }}
                    @if($e->private == '1') <span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>
                    @endif
                    @if($e->type=='1') <span title="Cours certifié Wewego" style="color:#ff5a00"><i class="fa fa-star" aria-hidden="true"></i></span>
                    @endif
					<br/>
				@endforeach
			</div>
		</div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$(".newestEventClick").on('click', function(){
            $('#ModalEdit #myModalLabel').html('');
            var childId = null;
            if({{Auth::user()->type}}==4 && $(this).attr('data-child')) childId = $(this).attr('data-child');
            editModal_initFooter();
            editModal_initData($(this).attr('data-id'), $(this).attr('data-owner'), $(this).attr('data-user_type'), $(this).attr('data-descr'), $(this).attr('data-hours'), $(this).attr('data-start'), $(this).attr('data-end'), $(this).attr('data-level'), $(this).attr('data-course'), $(this).attr('data-student_home'), $(this).attr('data-teacher_home'), $(this).attr('data-private'), $(this).attr('data-type'), $(this).attr('data-address'), childId, {{Auth::user()->type}}, {{Auth::user()->id}});
            initDoesNotSuitModal($(this).attr('data-owner'), $(this).attr('data-start'), $(this).attr('data-hours'), {{Auth::user()->id}});

        // Buttons display
            var modalTitle = ($(this).attr('data-course_label')=='') ? $(this).attr('data-level_label') : $(this).attr('data-course_label');
            $('#ModalEdit #myModalLabel').html(modalTitle);
            editModal_actionButtonsDisplay($(this).attr('data-id'), $(this).attr('data-owner'), $(this).attr('data-user_type'), $(this).attr('data-enrolled'), $(this).attr('data-enrolled_user_id'), $(this).attr('data-enrolled_status'), $(this).attr('data-private'), $(this).attr('data-teacher'), $(this).attr('data-students'), {{Auth::user()->id}}, {{Auth::user()->type}}, $(this).attr('data-type'), childId);
		});
	});
</script>