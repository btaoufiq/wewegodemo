<style type="text/css">
	span.multiselect-native-select {
		position: relative
	}
	span.multiselect-native-select select {
		border: 0!important;
		clip: rect(0 0 0 0)!important;
		height: 1px!important;
		margin: -1px -1px -1px -3px!important;
		overflow: hidden!important;
		padding: 0!important;
		position: absolute!important;
		width: 1px!important;
		left: 50%;
		top: 30px
	}
	.multiselect-container {
		position: absolute;
		list-style-type: none;
		margin: 0;
		padding: 0
	}
	.multiselect-container .input-group {
		margin: 5px
	}
	.multiselect-container>li {
		padding: 0
	}
	.multiselect-container>li>a.multiselect-all label {
		font-weight: 700
	}
	.multiselect-container>li.multiselect-group label {
		margin: 0;
		padding: 3px 20px 3px 20px;
		height: 100%;
		font-weight: 700
	}
	.multiselect-container>li.multiselect-group-clickable label {
		cursor: pointer
	}
	.multiselect-container>li>a {
		padding: 0
	}
	.multiselect-container>li>a>label {
		margin: 0;
		height: 100%;
		cursor: pointer;
		font-weight: 400;
		padding: 3px 0 3px 30px
	}
	.multiselect-container>li>a>label.radio, .multiselect-container>li>a>label.checkbox {
		margin: 0
	}
	.multiselect-container>li>a>label>input[type=checkbox] {
		margin-bottom: 5px
	}
	.btn-group>.btn-group:nth-child(2)>.multiselect.btn {
		border-top-left-radius: 4px;
		border-bottom-left-radius: 4px
	}
	.form-inline .multiselect-container label.checkbox, .form-inline .multiselect-container label.radio {
		padding: 3px 20px 3px 40px
	}
	.form-inline .multiselect-container li a label.checkbox input[type=checkbox], .form-inline .multiselect-container li a label.radio input[type=radio] {
		margin-left: -20px;
		margin-right: 0
	}
</style>

<nav id="sideNavLeft" style="float:left; width:20%; height:100%; display:inline-block; padding:10px; background-color:#fafafa;font-size:12px;">
	<form id="filterForm" method="POST" action="{{ url('/') }}">
	<div class="table-container">
	<table style="font-size: 12px;height: 80%">
		<tr>
			<td>
				<a id="filterClearLink" href="javascript:clearFilter()" style="font-size: 8pt;float: right;"><i class="fa fa-times" aria-hidden="true"></i> Effacer les filtres</a>
				<legend class="control-label">Filtrer par</legend>
			</td>
		</tr>
		<tr>
			<td>
				<label for="filterCourse">Cours</label><br/>
				<select class="form-control" id="filterCourse" name="filterCourse">
					<option value="All">Tous les cours</option>
					<option value="0">Primaire</option>
					@foreach($courses as $c)
						<option value="{{$c->id}}">{{$c->label_fr}}</option>
					@endforeach
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label for="filterType">Type / Créé par</label><br/>
				<select class="form-control" id="filterType" name="filterType">
					<option value="">Tous</option>
					<option value="0">Élève / Parent</option>
					<option value="1">Professeur</option>
					<!--<option value="asbl">ASBL</option>-->
					<option value="weweclass">Weweclass</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
			    <label for="filterLocalite">Localités</label><br/>
			    <span id="selectedLocalities" style="font-size: 8pt; font-style: italic;"></span>
			    <div>
			        <select id="filterLocalite" name="filterLocalite[]" class="form-control" multiple="multiple">
			            @foreach($localities as $l)
			            	<option value="{{$l->code}}">
								{{$l->code}} &#8211; {{$l->label_fr}}
							</option>
			            @endforeach
			        </select>
			    </div>
			</td>
		</tr>
		<tr>
			<td>
				<span><label for="filterDealChk">Confirmé</label></span>
				<span style="float: right;margin-right: 35%;"><input type="checkbox" name="filterDealChk" id="filterDealChk" value="yes" style="width: 15px;height: 15px;"></span>
			</td>
		</tr>
		<tr>
			<td>
				<span><label for="private">Cours particulier</label></span>
				<span style="float: right;margin-right: 35%;"><input type="checkbox" name="private" id="private" value="1" style="width: 15px;height: 15px;"></span>
			</td>
		</tr>
		<tr>
			<td>
				<span><label for="student_home">Domicile élève</label></span>
				<span style="float: right;margin-right: 35%;"><input type="checkbox" name="student_home" id="student_home" value="1" style="width: 15px;height: 15px;"></span>
			</td>
		</tr>
		<tr>
			<td>
				<span><label for="teacher_home">Domicile professeur</label></span>
				<span style="float: right;margin-right: 35%;"><input type="checkbox" name="teacher_home" id="teacher_home" value="1" style="width: 15px;height: 15px;"></span>
			</td>
		</tr>
		<tr>
			<td>
				<label for="filterDurationBtnGrp">Durée</label>
				<div class="btn-group btn-group-sm btn-duration" data-toggle="buttons" id="filterDurationBtnGrp">
					<label class="btn btn-default">
						<input type="radio" name="duration" id="duration1" value="1">1h
					</label>
					<label class="btn btn-default">
						<input type="radio" name="duration" id="duration2" value="2">2h
					</label>
					<label class="btn btn-default">
						<input type="radio" name="duration" id="duration3" value="3">3h
					</label>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				@if(Request::is('calendar'))
				<div style="text-align: center; top: 10px; position: relative;">
					<button type="submit" class="btn btn-primary btn-block" id="filterForCalendar">
				        Filtrer
				    </button>
			    </div>
			    @endif
			    @if(Request::is('/'))
			    <div style="text-align: center; top: 10px; position: relative;">
					<button type="submit" class="btn btn-primary btn-block" id="filterForWewevue">
				        Filtrer
				    </button>
			    </div>
			    @endif
			</td>
		</tr>
	</table>
	</div>
	</form>
</nav>
	
<script type="text/javascript">
	$('#filterType').on('change',function(e){
		if($(this).val()=='asbl'){
			clearFilter();
			$(this).val('asbl');
			$('#filterDealChk').attr('disabled',true);
			$('#private').attr('disabled',true);
			$('#student_home').attr('disabled',true);
			$('#teacher_home').attr('disabled',true);
			$('#filterDurationBtnGrp').children().css('pointer-events','none');
		}
		else if($(this).val()=='weweclass'){
			clearFilter();
			$(this).val('asbl');
			$('#filterDealChk').attr('disabled',true);
			$('#private').attr('disabled',true);
			$('#student_home').attr('disabled',true);
			$('#teacher_home').attr('disabled',true);
			$('#filterDurationBtnGrp').children().css('pointer-events','none');
			$('#filterLocalite').attr('disabled',true);
			$(this).val('weweclass');
		}
		else{
			$('#filterDealChk').attr('disabled',false);
			$('#private').attr('disabled',false);
			$('#student_home').attr('disabled',false);
			$('#teacher_home').attr('disabled',false);
			$('#filterDurationBtnGrp').children().css('pointer-events','auto');
			$('#filterLocalite').attr('disabled',false);
		}
	});
	$('#filterLocalite option').mousedown(function(e) {
	    e.preventDefault();
	    var originalScrollTop = $(this).parent().scrollTop();
	    $(this).prop('selected', $(this).prop('selected') ? false : true);
	    var self = this;
	    $(this).parent().focus();
	    setTimeout(function() {
	        $(self).parent().scrollTop(originalScrollTop);
	    }, 0);
	   	if($('#selectedLocalities')){
	   		if($('#selectedLocality'+$(this).val()).length > 0) $('#selectedLocality'+$(this).val()).remove();
		    else $("#selectedLocalities").append('<span id="selectedLocality'+$(this).val()+'">'+$(this).val()+' <i class="fa fa-times" aria-hidden="true" onclick="removeSpan('+$(this).val()+')"></i></span>');
		}
	    return false;
	});
	function removeSpan(id)
	{
		$('#selectedLocality'+id).remove();
		$('#filterLocalite option[value='+id+']').prop('selected', false);
	}
	$('#filterLevel[multiple] option').mousedown(function(){
		if ($(this).attr('selected')) $(this).removeAttr('selected');
		else $(this).attr('selected','selected');
		return false;
	});
	function clearFilter()
	{
		$(':input', '#filterForm')
			.val('')
			.removeAttr('checked')
			.removeAttr('selected');
		$('#selectedLocalities').empty();
		$('#filterDurationBtnGrp').children('.btn').removeClass('active');
		$('#filterCourse').val('All');
		$('#tableEventsByCourse tbody tr');
	}
	$('#filterForCalendar').on('click', function(e)
	{
		e.preventDefault();
		var formData = $('#filterForm').serialize();
		$.ajax({
			'url':'/filterCalendar',
			'method':'POST',
			'data':formData,
			'success': function (res) {
				$("#calendar").fullCalendar('removeEvents');
				$("#calendar").fullCalendar('addEventSource', res);
			}
		});
	});
	$('#filterForWewevue').on('click', function(e){
		e.preventDefault();
		$('.hexLink').each(function(){
			var n = ($(this).attr('id')).split('_')[1];
			if($('#filterCourse').val() == n) $(this).trigger('click');
		});
	});
</script>