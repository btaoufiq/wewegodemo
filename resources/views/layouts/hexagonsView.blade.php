<!--*********************************************


DEPRECIATED PAGE => go to ./weweview.blade.php


*********************************************-->

<div id="divHexagonsView" class="col-centered text-center" style="display: none;">

	<ul id="hexGrid" style="padding-top: 50px;">
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homePrimaireLink0">
			<img src="https://images.pexels.com/photos/159823/kids-girl-pencil-drawing-159823.jpeg?w=940&h=650&auto=compress&cs=tinysrgb"/>
			<h1>Primaire</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homeEdmLink1">
			<img src="https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1502131386000/photosp/6ff1c275-4532-44ed-b068-83e649a0cfae/stock-photo-school-backpack-ready-rucksack-college-back-to-school-pencil-case-6ff1c275-4532-44ed-b068-83e649a0cfae.jpg"/>
			<h1>EDM</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homeMathLink2">
			<img src="https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1449758832000/photosp/6f6505b7-6c25-43fe-8050-71cec9bb31a1/stock-photo-education-learning-green-female-chalk-school-person-woman-work-6f6505b7-6c25-43fe-8050-71cec9bb31a1.jpg"/>
			<h1>Math</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homeFrancaisLink3">
			<img src="https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1456744514000/photosp/aabc66eb-8921-4349-97b8-68e0e595e63c/stock-photo-teacher-college-mentor-profesor-alumno-aabc66eb-8921-4349-97b8-68e0e595e63c.jpg"/>
			<h1>Français</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homeNeerlandaisLink4">
			<img src="https://images.pexels.com/photos/247899/pexels-photo-247899.jpeg?w=940&h=650&auto=compress&cs=tinysrgb"/>
			<h1>Néerlandais</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homeAnglaisLink5">
			<img src="https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1462120644000/photosp/eb7f5a50-1a21-4aad-86eb-0ee3459ae637/stock-photo-education-learning-university-hat-school-friends-study-college-homework-eb7f5a50-1a21-4aad-86eb-0ee3459ae637.jpg"/>
			<h1>Anglais</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homeChimieLink6">
			<img src="https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1486893067000/photosp/5f6820e0-d12d-43ac-8c4a-154544180e6d/stock-photo-girl-science-goggles-experiment-conducting-chemicals-chemistry-lab-coat-5f6820e0-d12d-43ac-8c4a-154544180e6d.jpg"/>
			<h1>Chimie</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homeBiologieLink7">
			<img src="https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1500992618000/photosp/3ab865f9-cacd-45c6-9c24-7a3e629298f2/stock-photo-discovery-technology-research-science-biology-microscope-chemistry-laboratory-3ab865f9-cacd-45c6-9c24-7a3e629298f2.jpg"/>
			<h1>Biologie</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	  <li class="hex">
		<div class="hexIn">
		  <a class="hexLink" style="color:white;cursor: pointer;" id="homePhysiqueLink8">
			<img src="https://res.cloudinary.com/twenty20/private_images/t_watermark-criss-cross-10/v1498987060000/photosp/5a95ac16-aec3-4a16-820a-d5fe386ecb49/stock-photo-communication-teamwork-teaching-teacher-brainstorming-teammates-5a95ac16-aec3-4a16-820a-d5fe386ecb49.jpg"/>
			<h1>Physique</h1>
			<p></p>
		  </a>
		</div>
	  </li>
	</ul>

	<div id="divHexagonsViewList" class="hideDiv">
		<span id="fermerIcon" style="float: right; cursor: pointer;font-size: 14pt;" title="Fermer la liste"><i class="fa fa-times" aria-hidden="true"></i></span>
		<legend class="text-center" style="display: inline-block;"></legend>
		<div id="divEventsByCourseList" class="table-container table-responsive scrollable" style="max-height:90%;margin-top: -10pt">
			<div class="btn-group" role="group" id="btnGroupNavigateEventsByCourse1" style="float: left;">
				<button type="button" class="btn btn-default btn-sm" title="Précédent" id="btnWewePrevious"><</button>
				<button type="button" class="btn btn-default btn-sm" title="Suivant" id="btnWeweNext">></button>
			</div>
			<span id="dateShowing"></span>
			<div class="btn-group" role="group" id="btnGroupNavigateEventsByCourse2" style="float: right;margin-bottom: 10px;">
				<button type="button" class="btn btn-default btn-sm weweNavigateGrp2" id="btnWeweMonth">Mois</button>
				<button type="button" class="btn btn-default btn-sm weweNavigateGrp2" id="btnWeweWeek">Semaine</button>
				<button type="button" class="btn btn-default btn-sm weweNavigateGrp2" id="btnWeweDay">Jour</button>
			</div>
			<table id="tableEventsByCourse" class="table table-hover table-bordered table-condensed" style="font-size:1vw;">
				
			</table>
		</div>
		<input type="hidden" name="selectedWeweviewHid" id="selectedWeweviewHid">
		<input type="hidden" name="selectedDateHid" id="selectedDateHid">
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function () {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$('#hexGrid').children('li').children('div').children('a').click(function(e){
			$('#dateShowing').html();
			$('#hexGrid').fadeOut('fast');
			$('#hexGrid').addClass('hideDiv');
			$('#divHexagonsViewList').fadeIn('fast');
			$('#divHexagonsViewList').removeClass('hideDiv');
			$('#divHexagonsViewList').children('legend').html($(this).children('h1').html());
			$('#tableEventsByCourse').empty();

			e.preventDefault();
			$this = $(this);
			var id = $this.attr('id').charAt($this.attr('id').length-1);
			$('#filterCourse').val(id); // sideNavLeft
			var formData = $('#filterForm').serialize(); // sideNavLeft
			$.ajax({
				'url': '/eventByCourse/' + id,
				'type': 'GET',
				'data': formData,
				'success': function(res)
				{
					if(res['events'].length != 0){
						$.each(res['events'], function(index,element){
							if({{Auth::user()->id}} == element.owner) userTypeTitle = ' title="Vous êtes le propriétaire de ce cours"';
							else if(element.type == 1) userTypeTitle = ' title="Cours certifié Wewego !"';
							else if(element.type == 2) userTypeTitle = ' title="Cours proposé par une Asbl"';
							else if(element.user_type == 1 && {{Auth::user()->id}} != element.owner) userTypeTitle = ' title="Ce cours est proposé par un professeur"';
							else if(element.user_type == 0 && {{Auth::user()->id}} != element.owner) userTypeTitle = ' title="Ce cours est demandé par un élève"';
							else userTypeTitle = '';
							var userType = '';
							var arrayStudents = res['students'];
							var arrayTeachers = res['teachers'];
							if(arrayTeachers.hasOwnProperty(element.id) && arrayTeachers[element.id]!=0) userType += '<i class="fa fa-male" aria-hidden="true" title="Professeur confirmé"></i> ';
							if(arrayStudents.hasOwnProperty(element.id) && arrayStudents[element.id]!=0) userType += '<span class="fa-stack" style="font-size:8px" title="'+arrayStudents[element.id]+' élève(s) confirmé(s) "><i class="fa fa-circle-o thin fa-stack-2x"></i><strong class="fa-stack-1x">'+arrayStudents[element.id]+'</strong></span>';
							levelToShow = (element.level_label == 'Primaire') ? '' : ' &#8212; <i>'+element.level_label+'</i>';
							isPrivate = (element.private==1) ? '<span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>' : '';
							isStudent_home = (element.student_home==1) ? ' &emsp; <i class="fa fa-check" aria-hidden="true"></i> Domicile élève' : '';
							isTeacher_home = (element.teacher_home==1) ? ' &emsp; <i class="fa fa-check" aria-hidden="true"></i> Domicile professeur' : '';
							//if(element.type==2) hasDescr = '';
							//else hasDescr = (element.descr==null) ? '<i>Pas de description</i>' : '<i>'+element.descr+'</i>';
							hasDescr = (element.descr==null) ? '<i>Pas de description</i>' : '<i>'+element.descr+'</i>';
							placeToShow = (element.type==1) ? element.address : element.localite;
							var disabledButton = '';
							var disableBothButton = '';
							if(element.enrolled_status == 0) disabledButton = 'disabled';
							else if(element.enrolled_status == 2){
								disabledButton = 'disabled';
								disableBothButton = 'disabled';
							}
							else{
								disabledButton = '';
								disableBothButton = '';
							}
							var weweRowHighlight = '';
							var eventUserType = '';
							if(element.type==1){
								eventUserType = '<span class="label label-basic" style="color:#ff5a00;border:1pt solid #ff5a00;"><i class="fa fa-star" aria-hidden="true"></i> Weweclass <i class="fa fa-star" aria-hidden="true"></i></span>';
								weweRowHighlight = 'weweRowHighlight';
							}
							else{
								if(element.type==2){
									eventUserType = '<span class="label label-basic" style="color:#245580;border:1pt solid #245580;"><i class="fa fa-home" aria-hidden="true"></i> ASBL</span>';
								}
								else{
									if(element.user_type=="1") eventUserType = '<span class="label label-basic" style="color:#245580;border:1pt solid #245580;">Professeur</span>';
									else if(element.user_type=="0") eventUserType = '<span class="label label-basic" style="color:#245580;border:1pt solid #245580;">Élève</span>';
									else if(element.user_type=="4") eventUserType = '<span class="label label-basic" style="color:#245580;border:1pt solid #245580;">Parent</span>';
								}
							}
							var proprietaire = '';
							if(element.user_type!='3') proprietaire = 'Propriétaire : <span class="showInfoCard" id="showInfoCard'+element.id+'" data-toggle="modal" data-target="#ModalShowInfoCard"><u>'+element.first_name+' '+element.last_name+'</u></span>';

							$('#tableEventsByCourse').append('<tr'+userTypeTitle+' data-localite="'+element.localite+'" data-user_type="'+element.user_type+'" data-deal="'+element.deal+'" data-private="'+element.private+'" data-student_home="'+element.student_home+'" data-teacher_home="'+element.teacher_home+'" data-duration="'+element.duration+'" data-type="'+element.type+'" data-dhstart="'+element.dhstart+'" class="'+weweRowHighlight+'"><td class="text-center" style="width:6%;vertical-align:middle;color:#245580;">'+userType+'</td><td style="line-height: 0.2;padding:1vw;border-left:0;"><span class="text-right" style="float:right;"><button id="confirmEditEvent'+element.id+'" data-id="'+element.id+'" class="btn btn-success btn-xs hideDiv" title="">Participer</button><button id="confirmChildEditEvent'+element.id+'" data-id="'+element.id+'" class="btn btn-success btn-xs hideDiv" title="">Inscrire mon enfant</button><button id="proposeEditEvent'+element.id+'" data-id="'+element.id+'" class="btn btn-success btn-xs hideDiv" title="">Se proposer</button><button id="unsubscribeEditEvent'+element.id+'" data-id="'+element.id+'" class="btn btn-warning btn-xs hideDiv" '+disableBothButton+' title="">Se désinscrire</button><button id="withdrawEditEvent'+element.id+'" data-id="'+element.id+'" class="btn btn-warning btn-xs hideDiv" '+disableBothButton+' title="">Se désinscrire</button> <button id="doesNotSuitEditEvent'+element.id+'" data-id="'+element.id+'" class="btn btn-info btn-xs hideDiv" '+disabledButton+' title="">Déplacer le cours</button><button id="editEvent'+element.id+'" data-toggle="modal" data-target="#ModalEdit" data-id="'+element.id+'" class="btn btn-default btn-xs hideDiv" style="border-color:#5bc0de;color:#5bc0de;" title="">Modifier</button> <button id="deleteEvent'+element.id+'" data-id="'+element.id+'" class="btn btn-default btn-xs hideDiv" style="border-color:#d9534f;color:#d9534f;" title="">Supprimer</button><button id="moreInfoEditEvent'+element.id+'" data-toggle="modal" data-target="#ModalInfoAsbl" class="btn btn-primary btn-xs hideDiv" title="">Plus d\'info</button><br/><p id="messagetext'+element.id+'" class="hideDiv small text-info" style="float:right;padding-top:6%"></p></span><span id="spanInfoEvent'+element.id+'"><p>Le <b>'+moment(element.dhstart).format('DD/MM/YYYY')+'</b> de <b>'+moment(element.dhstart).format('HH')+'h</b> à <b>'+moment(element.dhend).format('HH')+'h</b> '+isPrivate+levelToShow+'</p><p>'+proprietaire+'</p><p><i class="fa fa-map-marker" aria-hidden="true" title="Commune du propriétaire du cours"></i> '+placeToShow+isStudent_home+isTeacher_home+'</p><p>'+hasDescr+'</p></span></td><td style="width:10%;vertical-align:middle;text-align:center">'+eventUserType+'</td></tr>');

							$('#showInfoCard'+element.id).click(function(e){
								e.preventDefault();
								$.ajax({
									'url': '/getInfoCard/' + element.owner,
									'type': 'POST',
									'success': function (res) {
										if (res.status == 'failed') {
											swal('Oops', res.message, 'error');
										}
										else if (res.status == 'success') {
								            showInfoCardModal(res);
										}
									}
								});
					            
							});

						// Buttons display
							if(element.owner=={{Auth::user()->id}}) // current user is the owner
							{
								$('#editEvent'+element.id).toggleClass('hideDiv');
								$('#deleteEvent'+element.id).toggleClass('hideDiv');
								$('#ModalEdit #messagetext').css('display','').text('Vous êtes le propriétaire de ce cours');
							}
							else{
								switch({{Auth::user()->type}})
								{
									case 1 : // current user is a teacher
										if(element.user_type==1) // the event belongs to another teacher
										{
											$('#proposeEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true);
											$('#messagetext'+element.id).toggleClass('hideDiv').text('Vous ne pouvez pas vous proposer à ce cours');
										}
										else // the event belongs to a student
										{
											if(element.type==1 || element.type==2){
												$('#proposeEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true);
												$('#messagetext'+element.id).toggleClass('hideDiv').text('Vous ne pouvez pas vous proposer à ce cours');
											}
											else if(element.enrolled!=null && element.enrolled_user_id=={{Auth::user()->id}}){ // current teacher is enrolled
												$('#messagetext'+element.id).toggleClass('hideDiv').text('');
												if(element.enrolled_status == 2){ // current teacher has been declined
													$('#messagetext'+element.id).text('Vous avez été décliné');
													$('#proposeEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
												}
												else{
													$('#withdrawEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
													$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
													switch(element.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
														case 1 : $('#messagetext'+element.id).text('Vous avez été confirmé'); break;
														case 0 : 
															$('#messagetext'+element.id).text('Vous n\'avez pas encore été confirmé');
															$('#doesNotSuitEditEvent'+element.id).attr('disabled',true);
															break;
														default : $('#messagetext'+element.id).text(''); break;
													}
												}
											}
											else{
												if(arrayTeachers[element.id]==1){
													$('#proposeEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true);
													$('#messagetext'+element.id).toggleClass('hideDiv').text('Vous ne pouvez pas vous proposer à ce cours');
												}
												else $('#proposeEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
											}
										}
										break;
									case 0 : // current user is a student
										if(element.type==2){ // asbl event
											$('#moreInfoEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
										}
										else{
											if(element.user_type==1) // the event belongs to a teacher
											{
												switch(element.private)
												{
													case 1 : // private event : student_count = 1
														if(element.enrolled!=null){ // event has enrolled student
															if(element.enrolled_user_id=={{Auth::user()->id}}){ // current student is enrolled
																$('#messagetext'+element.id).toggleClass('hideDiv').text('');
																if(element.enrolled_status == 2){ // current student has been declined
																	$('#messagetext'+element.id).text('Vous avez été décliné');
																	$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																}
																else{
																	$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	switch(element.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
																		case 1 : $('#messagetext'+element.id).text('Vous avez été confirmé'); break;
																		case 0 : 
																			$('#messagetext'+element.id).text('Vous n\'avez pas encore été confirmé');
																			$('#doesNotSuitEditEvent'+element.id).attr('disabled',true);
																			break;
																		default : $('#messagetext'+element.id).text(''); break;
																	}
																}
															}
															else{
																$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
															}
														}
														else{
															if(arrayStudents[element.id]==1){
																$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
															}
															else $('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
														}
														break;
													case null : // not a private event : student_count up to 3
														if(element.enrolled!=null){ // a student is enrolled
															if(element.enrolled_user_id=={{Auth::user()->id}}){
																$('#messagetext'+element.id).toggleClass('hideDiv').text('');
																if(element.enrolled_status == 2){ // current student has been declined
																	$('#messagetext'+element.id).text('Vous avez été décliné');
																	$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																}
																else{
																	$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	switch(element.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
																		case 1 : $('#messagetext'+element.id).text('Vous avez été confirmé'); break;
																		case 0 : 
																			$('#messagetext'+element.id).text('Vous n\'avez pas encore été confirmé');
																			$('#doesNotSuitEditEvent'+element.id).attr('disabled',true);
																			break;
																		default : $('#messagetext'+element.id).text(''); break;
																	}
																}
															}
															else{
																if(arrayStudents[element.id] < 3) $('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
															}
														}
														else{;
															if(arrayStudents[element.id] < 3){
																$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
															}
															else{ // event has 3 students
																$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).text('Ce cours est complet');
															}
														}
														break;
												}
											}
											if(element.user_type==0 || element.user_type==4 || (element.user_type==3 && element.type==1)) // the event belongs to a student, a parent or is a weweclass
											{
												switch(element.private)
												{
													case 1 : // private event : student_count = 1
														$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
														$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
														break;
													case null : // not a private event : student_count up to 3
														if(element.enrolled!=null && element.enrolled_user_id=={{Auth::user()->id}}){ // current student is enrolled
															$('#messagetext'+element.id).toggleClass('hideDiv').text('');
															if(element.enrolled_status == 2){ // current student has been declined
																$('#messagetext'+element.id).text('Vous avez été décliné');
																$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
															}
															else{
																$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																switch(element.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
																	case 1 : $('#messagetext'+element.id).text('Vous avez été confirmé'); break;
																	case 0 : 
																		$('#messagetext'+element.id).text('Vous n\'avez pas encore été confirmé');
																		$('#doesNotSuitEditEvent'+element.id).attr('disabled',true);
																		break;
																	default : $('#messagetext'+element.id).text(''); break;
																}
															}
														}
														else{
															if(arrayStudents[element.id] < 3){
																$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
															}
															else{ // event has 3 students
																$('#confirmEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
															}
														}
														break;
												}
											}
										}
										break;
									case 4 : // current user is a parent
										if(element.type==2){ // asbl event
											$('#moreInfoEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
										}
										else{
											if(element.user_type==1){ // the event belongs to a teacher
												switch(element.private)
												{
													case 1 : // private event : student_count = 1
														if(element.enrolled!=null){ // event has enrolled student
															if(element.enrolled_user_id=={{Auth::user()->id}}){ // current student is enrolled
																$('#messagetext'+element.id).toggleClass('hideDiv').text('');
																if(element.enrolled_status == 2){ // current student has been declined
																	$('#messagetext'+element.id).text('Vous avez été décliné');
																	$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																}
																else{
																	$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	switch(element.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
																		case 1 : $('#messagetext'+element.id).text('Vous avez été confirmé'); break;
																		case 0 : 
																			$('#messagetext'+element.id).text('Vous n\'avez pas encore été confirmé');
																			$('#doesNotSuitEditEvent'+element.id).attr('disabled',true);
																			break;
																		default : $('#messagetext'+element.id).text(''); break;
																	}
																}
															}
															else{
																$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
															}
														}
														else{
															if(arrayStudents[element.id]==1){
																$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
															}
															else $('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
														}
														break;
													case null : // not a private event : student_count up to 3
														if(element.enrolled!=null){ // a student is enrolled
															if(element.enrolled_user_id=={{Auth::user()->id}}){
																$('#messagetext'+element.id).toggleClass('hideDiv').text('');
																if(element.enrolled_status == 2){ // current student has been declined
																	$('#messagetext'+element.id).text('Vous avez été décliné');
																	$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																}
																else{
																	$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																	switch(element.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
																		case 1 : $('#messagetext'+element.id).text('Vous avez été confirmé'); break;
																		case 0 : 
																			$('#messagetext'+element.id).text('Vous n\'avez pas encore été confirmé');
																			$('#doesNotSuitEditEvent'+element.id).attr('disabled',true);
																			break;
																		default : $('#messagetext'+element.id).text(''); break;
																	}
																}
															}
															else{
																if(arrayStudents[element.id] < 3) $('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
															}
														}
														else{;
															if(arrayStudents[element.id] < 3){
																$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
															}
															else{ // event has 3 students
																$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).text('Ce cours est complet');
															}
														}
														break;
												}
											}
											if(element.user_type==0 || element.user_type==4 || (element.user_type==3 && element.type==1)){ // the event belongs to a student, a parent or is weweclass
												switch(element.private)
												{
													case 1 : // private event : student_count = 1
														$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
														$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
														break;
													case null : // not a private event : student_count up to 3
														if(element.enrolled!=null && element.enrolled_user_id=={{Auth::user()->id}}){ // current parent is enrolled
															$('#messagetext'+element.id).toggleClass('hideDiv').text('');
															if(element.enrolled_status == 2){ // current student has been declined
																$('#messagetext'+element.id).text('Vous avez été décliné');
																$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
															}
															else{
																$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('data-id',element.id);
																switch(element.enrolled_status){ // 1=confirmed ; 0=not yet confirmed
																	case 1 : $('#messagetext'+element.id).text('Vous avez été confirmé'); break;
																	case 0 : 
																		$('#messagetext'+element.id).text('Vous n\'avez pas encore été confirmé');
																		$('#doesNotSuitEditEvent'+element.id).attr('disabled',true);
																		break;
																	default : $('#messagetext'+element.id).text(''); break;
																}
															}
														}
														else{
															if(arrayStudents[element.id] < 3){
																$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
															}
															else{ // event has 3 students
																$('#confirmChildEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
																$('#messagetext'+element.id).toggleClass('hideDiv').text('Ce cours est complet');
															}
														}
														break;
												}
											}
										}
										
										break;
								}
							}


							if(!($('#doesNotSuitEditEvent'+element.id).hasClass('hideDiv'))) $('#spanInfoEvent'+element.id).css('width','60%').css('line-height','1');
							else if(!($('#confirmEditEvent'+element.id).hasClass('hideDiv')) || !($('#proposeEditEvent'+element.id).hasClass('hideDiv'))) $('#spanInfoEvent'+element.id).css('width','85%').css('line-height','1');
							else if(!($('#editEvent'+element.id).hasClass('hideDiv'))) $('#spanInfoEvent'+element.id).css('width','75%').css('line-height','1');
							else $('#spanInfoEvent'+element.id).css('width','100%').css('line-height','1');
							
						// Buttons actions
							$('#moreInfoEditEvent'+element.id).on('click', function(e){
								e.preventDefault();
								$('#ModalInfoAsbl #infoAsblTitle').html(element.course_label+'&#160; &#8212; &#160;'+element.level_label);
								$('#ModalInfoAsbl #infoAsblDate').text(moment(element.dhstart).format('DD/MM/YYYY'));
								$('#ModalInfoAsbl #infoAsblTime').text(moment(element.dhstart).format('HH')+'h - '+moment(element.dhend).format('HH')+'h');
								$('#ModalInfoAsbl #infoAsblAddress').text(element.address);
								$('#ModalInfoAsbl #infoAsblDescription').text(element.descr);
								$('#ModalInfoAsbl #event_id').val(element.id);
							});
							$('#editEvent'+element.id).on('click', function(e){
								$('#ModalEdit #id').val(element.id);
								$('#ModalEdit #owner').val(element.owner);
								$('#ModalEdit #editDescr').val(element.descr).attr('disabled',true);
								$('#ModalEdit #editAddress').val(element.address).attr('disabled',true);
								$('#ModalEdit #editLevelSelect').val(element.level_id).attr('disabled',true);
								$('#ModalEdit #editDatepicker').val(moment(element.dhstart).format('DD/MM/YYYY'));
								$('#ModalEdit #editTimepickerStart').val(moment(element.dhstart).format('HH'));
								$('#ModalEdit #editHours option[value="'+element.duration+'"]').prop('selected', true);
								$('#ModalEdit #editHours').attr('disabled',true);
								$('#ModalEdit #editTimepickerEnd').val(moment(element.dhend).format('HH'));
								if(element.course_id == '0') $('#ModalEdit #editDivSelectCourse').hide();
								else{
									$('#ModalEdit #editDivSelectCourse').show();
									$('#ModalEdit #editCourseSelect').val(element.course_id).attr('disabled',true);
								}
								if(element.student_home == '1') $('#ModalEdit #editStudent_home').prop('checked', true).attr('disabled',true);
								else $('#ModalEdit #editStudent_home').prop('checked', false).attr('disabled',true);
								if(element.teacher_home == '1') $('#ModalEdit #editTeacher_home').prop('checked', true).attr('disabled',true);
								else $('#ModalEdit #editTeacher_home').prop('checked', false).attr('disabled',true);
								if(element.private=='1') $('#ModalEdit #editPrivate').prop('checked', true).attr('disabled',true);
								else $('#ModalEdit #editPrivate').prop('checked', false).attr('disabled',true);
								if(element.type=='1'){
		                            $('#ModalEdit #editAddress').val(element.address);
		                            $('#ModalEdit #mobilityNotWeweclass').css('display','none');
		                            $('#ModalEdit #mobilityWeweclass').css('display','');
		                        }
		                        else{
		                            $('#ModalEdit #mobilityNotWeweclass').css('display','');
		                            $('#ModalEdit #mobilityWeweclass').css('display','none');
		                        }
							});
							$('#deleteEvent'+element.id).on('click', function(e)
							{
								e.preventDefault();
								swal({
									title: 'Êtes-vous sûr de vouloir supprimer ce cours ?',
									text: "Vous ne pourrez pas revenir en arrière !",
									type: 'warning',
									showCancelButton: true,
									confirmButtonColor: '#3085d6',
									cancelButtonColor: '#d33',
									confirmButtonText: 'Supprimer',
									cancelButtonText: 'Annuler',
									confirmButtonClass: 'btn btn-success',
									cancelButtonClass: 'btn btn-danger',
									buttonsStyling: false
								},function(confirm) {
									if(confirm)
									{
										$.ajax({
											'url':'/deleteMyEvent/'+element.id,
											'type':'POST',
											'data':   {_method: 'DELETE' },
											'success': function(res)
											{
												$this.closest('tr').fadeOut();
												swal({
													title: 'Supprimé !',
													text: 'Le cours a été supprimé avec succès.',
													type: 'success',
													timer: 3000,
													showConfirmButton: false,
												});
												//setTimeout(function() {window.location.reload();},4000);
												$('#editEvent'+element.id).toggleClass('hideDiv');
												$('#deleteEvent'+element.id).toggleClass('hideDiv');
												$('#messagetext'+element.id).toggleClass('hideDiv').css('font-style','italic').text('Supprimé');
											}
										});
									}

								});
							});
							$('#confirmEditEvent'+element.id).on('click', function(e)
							{
								e.preventDefault();
								var mobility = '';
								if(element.student_home=='1' && element.teacher_home=='1')
								{
									mobility = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Définir le lieu</label><div class="btn-group col-sm-10" data-toggle="buttons"><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
								}
								swal({
									title: 'Demande de participation',
									text: mobility+' <textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
									html: true,
									showCancelButton: true,
									closeOnConfirm: true,
									closeOnCancel: true,
									confirmButtonText:'Participer',
									cancelButtonText:'Annuler'
								}, function(inputValue) {
									if(inputValue) {
										var data = {};
										var arr = [];
										if (inputValue != "") {
											var value = $('#text').val();
											data['message'] = value;

										}
										if (mobility != '') {
											data['mobility'] = $("input[name='mobility']:checked").val();
										}
										arr.push(data);
										$.ajax({
											'url': '/confirmEvent/' + element.id,
											'type': 'POST',
											'data': arr[0],
											'success': function (res) {
												$('#ModalEdit').modal('hide');
												if (res.status == 'failed') {
													swal('Oops', res.message, 'error');
												}
												else if (res.status == 'success') {
													//window.location.reload();
													$('#confirmEditEvent'+element.id).css('display','none');
													$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
													$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
													$('#messagetext'+element.id).toggleClass('hideDiv').text('Vous n\'avez pas encore été confirmé');
													$('#spanInfoEvent'+element.id).css('width','60%').css('line-height','1');
												}
											}
										});
									}
								});
							});
							$('#confirmChildEditEvent'+element.id).on('click', function(e)
						    {
						        e.preventDefault();
						        var mobility = '';
						        if(element.student_home=='1' && element.teacher_home=='1')
						        {
						            mobility = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Définir le lieu</label><div class="btn-group col-sm-10" data-toggle="buttons"><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
						        }
						        var selectChild = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Concerne</label><div class="col-sm-10"><select class="form-control" id="childToConfirm" name="childToConfirm">';
						        <?php $mychildren = DB::table('children')->where('parent','=',Auth::user()->id)->get(); ?>
						        @foreach($mychildren as $child)
						            selectChild += '<option value="{{$child->id}}">{{$child->first_name}}</option>';
						        @endforeach
						        selectChild += '</select></div></div><br/>';
						        swal({
						            title: "Demande de participation",
						            text: selectChild+mobility+' <textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
						            html: true,
						            showCancelButton: true,
						            closeOnConfirm: true,
						            closeOnCancel: true,
						            confirmButtonText:'Participer',
						            cancelButtonText:'Annuler'
						        }, function(inputValue) {
						            if(inputValue) {
						                var data = {};
						                var arr = [];
						                if (inputValue != "") {
						                    var value = $('#text').val();
						                    data['message'] = value;
						                }
						                if (mobility != '') {
						                    data['mobility'] = $("input[name='mobility']:checked").val();
						                }
						                data['childToConfirm'] = $("#childToConfirm").val();
						                arr.push(data);
						                $.ajax({
						                    'url': '/confirmChildEvent/' + element.id,
						                    'type': 'POST',
						                    'data': arr[0],
						                    'success': function (res) {
						                        $('#ModalEdit').modal('hide');
						                        if (res.status == 'failed') {
						                            swal('Oops', res.message, 'error');
						                        }
						                        else if (res.status == 'success') {
						                            $('#confirmChildEditEvent'+element.id).css('display','none');
													$('#unsubscribeEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
													$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
													$('#messagetext'+element.id).toggleClass('hideDiv').text('Vous n\'avez pas encore été confirmé');
													$('#spanInfoEvent'+element.id).css('width','60%').css('line-height','1');
						                        }
						                    }
						                });
						            }
						        });

						    });
							$('#proposeEditEvent'+element.id).on('click', function(e)
							{
								var mobility = '';
								if(element.student_home=='1' && element.teacher_home=='1')
								{
									mobility = '<div class="form-group col-sm-12"><label class="col-sm-5 text-left">Définir le lieu</label><div class="btn-group col-sm-10" data-toggle="buttons"><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default col-sm-6"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
								}
								swal({
									title: 'Proposer de donner le cours',
									text: mobility+' <textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
									html: true,
									showCancelButton: true,
									closeOnConfirm: true,
									closeOnCancel: true,
									confirmButtonText:'Se proposer',
									cancelButtonText:'Annuler'
								}, function(inputValue) {
									if(inputValue) {
										var data = {};
										var arr = [];
										if (inputValue != "") {
											var value = $('#text').val();
											data['message'] = value;

										}
										if (mobility != '') {
											data['mobility'] = $("input[name='mobility']:checked").val();
										}
										arr.push(data);
										$.ajax({
											'url': '/proposeEvent/' + element.id,
											'type': 'POST',
											'data': arr[0],
											'success': function (res) {
												$('#ModalEdit').modal('hide');
												if (res.status == 'failed') {
													swal('Oops', res.message, 'error');
												}
												else if (res.status == 'success') {
													//window.location.reload();
													$('#proposeEditEvent'+element.id).css('display','none');
													$('#withdrawEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',false).attr('data-id',element.id);
													$('#doesNotSuitEditEvent'+element.id).toggleClass('hideDiv').attr('disabled',true).attr('data-id',element.id);
													$('#messagetext'+element.id).toggleClass('hideDiv').text('Vous n\'avez pas encore été confirmé');
													$('#spanInfoEvent'+element.id).css('width','60%').css('line-height','1');
												}
											}
										});
									}
								});
							});
							$('#unsubscribeEditEvent'+element.id).on('click', function(e)
							{
								e.preventDefault();
								$.ajax({
									'url':'/unsubscribeEvent/'+element.id,
									'type':'POST',
									'success': function(res)
									{
										if(res.status == 'success')
										{
											//window.location.reload();
											$('#unsubscribeEditEvent'+element.id).css('display','none');
											$('#doesNotSuitEditEvent'+element.id).css('display','none');
											$('#messagetext'+element.id).css('font-style','italic').text('Vous vous êtes désinscrit');
											if($('#messagetext'+element.id).hasClass('hideDiv')) $('#messagetext'+element.id).toggleClass('hideDiv');
										}
									}
								});
							});
							$('#withdrawEditEvent'+element.id).on('click', function(e)
							{
								e.preventDefault();
								$.ajax({
									'url':'/withdrawEvent/'+element.id,
									'type':'POST',
									'success': function(res)
									{
										$('#ModalEdit').modal('hide');
										if(res.status == 'success')
										{
											//window.location.reload();
											$('#withdrawEditEvent'+element.id).css('display','none');
											$('#doesNotSuitEditEvent'+element.id).css('display','none');
											$('#messagetext'+element.id).toggleClass('hideDiv').css('font-style','italic').text('Vous vous êtes désinscrit');
										}
									}
								});
							});
							$('#doesNotSuitEditEvent'+element.id).on('click', function(e)
							{
								e.preventDefault();
								$('#doesNotSuitModal').find('#suitid').val(element.id);
								$('#doesNotSuitModal').find('#typeSuitMe').val('all');
								$('#doesNotSuitModal').find('#suitDatepicker').val(moment(element.dhstart).format('DD/MM/YYYY'));
								$('#doesNotSuitModal').find('#suitTimepickerStart').val(moment(element.dhstart).format('H'));
								$('#doesNotSuitModal').find('#suitHours').val(element.duration);
								$('#doesNotSuitModal').find('#suitTimepickerEnd').val(moment(element.dhend).format('H'));
								$('#doesNotSuitModal').find('#suitOwner').val(element.owner);
								if(element.owner != {{Auth()->user()->id}}){
									if($('#doesNotSuitModal').find('#messageinfo').length==0) $('<div class="alert alert-danger" id="messageinfo"><strong>Attention !</strong> Vous ne serez plus inscrit(e) pour ce cours.</div>').insertBefore('#firstDiv');
								}
								else $('#doesNotSuitModal').find('#messageinfo').remove();
								$('#doesNotSuitModal').modal('show');
							});
						});
					}
					else{
						$('#tableEventsByCourse').append('<tbody><tr><td class="text-center" style="border:hidden;"><i>Pas de cours</i></td></tr></tbody>');
					}
				}
			});
		});
		moment.locale('fr', {
		    months : 'janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre'.split('_'),
		    monthsShort : 'janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.'.split('_'),
		    monthsParseExact : true,
		    weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
		    weekdaysShort : 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
		    weekdaysMin : 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
		    weekdaysParseExact : true,
		    longDateFormat : {
		        LT : 'HH:mm',
		        LTS : 'HH:mm:ss',
		        L : 'DD/MM/YYYY',
		        LL : 'D MMMM YYYY',
		        LLL : 'D MMMM YYYY HH:mm',
		        LLLL : 'dddd D MMMM YYYY HH:mm'
		    },
		    calendar : {
		        sameDay : '[Aujourd’hui à] LT',
		        nextDay : '[Demain à] LT',
		        nextWeek : 'dddd [à] LT',
		        lastDay : '[Hier à] LT',
		        lastWeek : 'dddd [dernier à] LT',
		        sameElse : 'L'
		    },
		    relativeTime : {
		        future : 'dans %s',
		        past : 'il y a %s',
		        s : 'quelques secondes',
		        m : 'une minute',
		        mm : '%d minutes',
		        h : 'une heure',
		        hh : '%d heures',
		        d : 'un jour',
		        dd : '%d jours',
		        M : 'un mois',
		        MM : '%d mois',
		        y : 'un an',
		        yy : '%d ans'
		    },
		    dayOfMonthOrdinalParse : /\d{1,2}(er|e)/,
		    ordinal : function (number) {
		        return number + (number === 1 ? 'er' : 'e');
		    },
		    meridiemParse : /PD|MD/,
		    isPM : function (input) {
		        return input.charAt(0) === 'M';
		    },
		    // In case the meridiem units are not separated around 12, then implement
		    // this function (look at locale/id.js for an example).
		    // meridiemHour : function (hour, meridiem) {
		    //     return /* 0-23 hour, given meridiem token and hour 1-12 */ ;
		    // },
		    meridiem : function (hours, minutes, isLower) {
		        return hours < 12 ? 'PD' : 'MD';
		    },
		    week : {
		        dow : 1, // Monday is the first day of the week.
		        doy : 4  // The week that contains Jan 4th is the first week of the year.
		    }
		});
		$('#fermerIcon').click(function(){
			$('#divHexagonsViewList').fadeOut('fast');
			$('#divHexagonsViewList').addClass('hideDiv');
			$('#hexGrid').fadeIn('fast');
			$('#hexGrid').removeClass('hideDiv');
			$('.weweNavigateGrp2').removeClass('active');
		});
		$('#btnWewePrevious').on('click', function(e){
			e.preventDefault();
			switch($('#selectedWeweviewHid').val()){
				case 'btnWeweMonth': 
					var currentDisplayingMonth = $('#selectedDateHid').val();
					var previousMonth = moment(moment(currentDisplayingMonth).subtract(1,'M')).format('M');
					$('#selectedDateHid').val(new Date(moment(currentDisplayingMonth).subtract(1,'M')));
					var dateShowing = moment(moment(currentDisplayingMonth).subtract(1,'M')).format('MMMM')
					$('#dateShowing').html(dateShowing.charAt(0).toUpperCase()+dateShowing.substr(1).toLowerCase());
					var eventstart = '';
					$('#tableEventsByCourse tbody tr').fadeOut('fast');
					$('#tableEventsByCourse tbody tr').filter(function(){
						eventstart = moment($(this).data('dhstart')).get('month') +1;
						if(eventstart == previousMonth) $(this).fadeIn('fast').addClass('filtered');
					});
					break;
				case 'btnWeweWeek':
					var currentDisplayingWeek = moment($('#selectedDateHid').val());
					var previousWeek = moment(currentDisplayingWeek).subtract(7, 'd');
					$('#selectedDateHid').val(new Date(previousWeek));
					var next7days = moment(moment(previousWeek).add(7, 'd')).format('DD/MM/YYYY');
					$('#dateShowing').html(moment(new Date(previousWeek)).format('DD/MM/YYYY')+' - '+next7days);
					var eventstart = '';
					$('#tableEventsByCourse tbody tr').fadeOut('fast');
					$('#tableEventsByCourse tbody tr').filter(function(){
						eventstart = moment(moment($(this).data('dhstart'))).format('DD/MM/YYYY');
						if(moment(previousWeek).format('DD/MM/YYYY') <= eventstart && eventstart < next7days) $(this).fadeIn('fast').addClass('filtered');
					});
					break;
				case 'btnWeweDay':
					var currentDisplayingDate = moment($('#selectedDateHid').val());
					var previousDate = moment(currentDisplayingDate).subtract(1, 'd');
					$('#selectedDateHid').val(new Date(previousDate));
					$('#dateShowing').html(moment(new Date(previousDate)).format('DD/MM/YYYY'));
					var eventstart = '';
					$('#tableEventsByCourse tbody tr').fadeOut('fast');
					$('#tableEventsByCourse tbody tr').filter(function(){
						eventstart = moment($(this).data('dhstart')).format('DD/MM/YYYY');
						if(moment(previousDate).format('DD/MM/YYYY') == eventstart) $(this).fadeIn('fast').addClass('filtered');
					});
					break;
			}
		});
		$('#btnWeweNext').on('click', function(e){
			e.preventDefault();
			switch($('#selectedWeweviewHid').val()){
				case 'btnWeweMonth': 
					var currentDisplayingMonth = $('#selectedDateHid').val();
					var nextMonth = moment(moment(currentDisplayingMonth).add(1,'M')).format('M');
					$('#selectedDateHid').val(new Date(moment(currentDisplayingMonth).add(1,'M')));
					var dateShowing = moment(moment(currentDisplayingMonth).add(1,'M')).format('MMMM');
					$('#dateShowing').html(dateShowing.charAt(0).toUpperCase()+dateShowing.substr(1).toLowerCase());
					var eventstart = '';
					$('#tableEventsByCourse tbody tr').fadeOut('fast');
					$('#tableEventsByCourse tbody tr').filter(function(){
						eventstart = moment($(this).data('dhstart')).get('month') +1;
						if(eventstart == nextMonth) $(this).fadeIn('fast').addClass('filtered');
					});
					break;
				case 'btnWeweWeek':
					var currentDisplayingWeek = moment($('#selectedDateHid').val());
					var nextWeek = moment(currentDisplayingWeek).add(7, 'd');
					var next7days = moment(moment(nextWeek).add(7, 'd')).format('DD/MM/YYYY');
					$('#selectedDateHid').val(new Date(nextWeek));
					$('#dateShowing').html(moment(new Date(nextWeek)).format('DD/MM/YYYY')+' - '+next7days);
					var eventstart = '';
					$('#tableEventsByCourse tbody tr').fadeOut('fast');
					$('#tableEventsByCourse tbody tr').filter(function(){
						eventstart = moment(moment($(this).data('dhstart'))).format('DD/MM/YYYY');
						if(moment(nextWeek).format('DD/MM/YYYY') <= eventstart && eventstart < next7days) $(this).fadeIn('fast').addClass('filtered');
					});
					break;
				case 'btnWeweDay':
					var currentDisplayingDate = moment($('#selectedDateHid').val());
					var nextDate = moment(currentDisplayingDate).add(1, 'd');
					$('#selectedDateHid').val(new Date(nextDate));
					$('#dateShowing').html(moment(new Date(nextDate)).format('DD/MM/YYYY'));
					var eventstart = '';
					$('#tableEventsByCourse tbody tr').fadeOut('fast');
					$('#tableEventsByCourse tbody tr').filter(function(){
						eventstart = moment($(this).data('dhstart')).format('DD/MM/YYYY');
						if(moment(nextDate).format('DD/MM/YYYY') == eventstart) $(this).fadeIn('fast').addClass('filtered');
					});
					break;
			}
		});
		$('.weweNavigateGrp2').on('click',function(e){
			e.preventDefault();
			$('.weweNavigateGrp2').not($(this)).removeClass('active');
			$('#selectedWeweviewHid').val($(this).attr('id'));
			$('#selectedDateHid').val(new Date());
			switch($('#selectedWeweviewHid').val()){
				case 'btnWeweMonth': 
					var dateShowing = moment(new Date()).format('MMMM');
					$('#dateShowing').html(dateShowing.charAt(0).toUpperCase()+dateShowing.substr(1).toLowerCase());
					break;
				case 'btnWeweWeek':
					var next7days = moment(moment(new Date()).add(7, 'd')).format('DD/MM/YYYY');
					$('#dateShowing').html(moment(new Date()).format('DD/MM/YYYY')+' - '+next7days);
					break;
				case 'btnWeweDay':
					$('#dateShowing').html(moment(new Date()).format('DD/MM/YYYY'));
					break;
			}
		});
		$('#btnWeweMonth').on('click', function(e){
			e.preventDefault();
			$(this).addClass('active');
			var dhstartMonth = '';
			var currentMonth = moment(new Date()).get('month') +1; //+1 car moment.js commence à compter les mois à 0
			$('#tableEventsByCourse tbody tr').fadeOut('fast');
			$('#tableEventsByCourse tbody tr').filter(function(){
				dhstartMonth = moment($(this).data('dhstart')).get('month') +1;
				if(dhstartMonth == currentMonth) $(this).fadeIn('fast').addClass('filtered');
			});
		});
		$('#btnWeweWeek').on('click', function(e){
			e.preventDefault();
			$(this).addClass('active');
			var today = moment(new Date()).format('DD/MM/YYYY');
			var next7days = moment(moment().add(7, 'd')).format('DD/MM/YYYY');
			var eventstart = '';
			$('#tableEventsByCourse tbody tr').fadeOut('fast');
			$('#tableEventsByCourse tbody tr').filter(function(){
				eventstart = moment($(this).data('dhstart')).format('DD/MM/YYYY');
				if(today <= eventstart && eventstart < next7days) $(this).fadeIn('fast').addClass('filtered');
			});
		});
		$('#btnWeweDay').on('click', function(e){
			e.preventDefault();
			$(this).addClass('active');
			var today = moment(new Date()).format('DD/MM/YYYY');
			var eventstart = '';
			$('#tableEventsByCourse tbody tr').fadeOut('fast');
			$('#tableEventsByCourse tbody tr').filter(function(){
				eventstart = moment($(this).data('dhstart')).format('DD/MM/YYYY');
				if(eventstart == today) $(this).fadeIn('fast').addClass('filtered');
			});
		});
		
	});
</script>