@extends('layouts.app')

@section('content')
<div class="container">
<?php $localities = DB::table('localities')->get(); ?>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Formulaire d'inscription pour un 
                     <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default">
                            <input type="radio" name="registrationType" id="registrationTypeParent" value="registerParent" onchange="showForm(this.value)"> Parent
                        </label>
                        <label class="btn btn-default active">
                            <input type="radio" name="registrationType" id="registrationTypeStudent" value="registerStudent" onchange="showForm(this.value)"> Élève
                        </label>
                    </div>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}" id="simpleRegistrationForm">
                        {{ csrf_field() }}
                        <input type="hidden" name="type" id="type" value="0">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-3 control-label">Nom</label>
                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>
                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-3 control-label">Prénom</label>
                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required>
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                            <label for="sex" class="col-md-3 control-label">Sexe</label>
                            <div class="col-md-6">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-default">
                                        <input type="radio" name="sex" id="sexM" value="M" required> M
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="sex" id="sexF" value="F"> F
                                    </label>
                                </div>
                                @if ($errors->has('sex'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-3 control-label">Addresse email</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="level" class="col-md-3 control-label">Niveau</label>
                            <div class="col-md-6" style="line-height:75%;">
                                <input type="radio" id="primaireRad" name="level" value="1" required>
                                <label for="primaireRad" style="font-weight:normal">Primaire</label><br/>
                                <input type="radio" id="secInfRad" name="level" value="2">
                                <label for="secInfRad" style="font-weight:normal">Secondaire inférieur</label><br/>
                                <input type="radio" id="secSupRad" name="level" value="3">
                                <label for="secSupRad" style="font-weight:normal">Secondaire supérieur</label>
                                @if ($errors->has('level'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('level') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('localite') ? ' has-error' : '' }}">
                            <label for="localite" class="col-md-3 control-label">Localité</label>
                            <div class="col-md-6">
                                <select class="form-control" id="localite" name="localite">
                                    @foreach($localities as $l)
                                        <option value="{{$l->code}}">
                                            {{$l->code}} &#8211; {{$l->label_fr}}
                                            @if(!empty($l->label_nl))
                                                / {{$l->label_nl}}
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('localite'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('localite') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-3 control-label">Téléphone</label>
                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-3 control-label">Mot de passe</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-3 control-label">Confirmer mot de passe</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="checkConditions" class="col-md-3 control-label"></label>
                            <div class="col-md-6">
                                <input id="checkConditions" type="checkbox" name="checkConditions" required /> J'ai lu et j'accepte les <a href="{{route('conditions')}}" onclick="open('{{url('conditions')}}', 'Popup', 'scrollbars=1,resizable=1,height=560,width=770'); return false;" >conditions d'utilisation</a>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    S'inscrire
                                </button>
                            </div>
                        </div>
                    </form>
                    <form class="form-horizontal hideDiv" method="POST" action="{{ url('register/createFamily') }}" id="parentRegistrationForm">
                        {{ csrf_field() }}
                        <div class="col-md-12">
                            <input type="hidden" name="parent_type" id="parent_type" value="4">
                            <div class="col-md-7 form-group{{ $errors->has('parent_last_name') ? ' has-error' : '' }}">
                                <label for="parent_last_name" class="col-md-3 control-label">Nom</label>
                                <div class="col-md-9">
                                    <input id="parent_last_name" type="text" class="form-control" name="parent_last_name" value="{{ old('parent_last_name') }}" required autofocus>
                                    @if ($errors->has('parent_last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5 form-group{{ $errors->has('parent_sex') ? ' has-error' : '' }}">
                                <label for="parent_sex" class="col-md-3 control-label">Sexe</label>
                                <div class="col-md-9">
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default">
                                            <input type="radio" name="parent_sex" id="parent_sexM" value="M" required> M
                                        </label>
                                        <label class="btn btn-default">
                                            <input type="radio" name="parent_sex" id="parent_sexF" value="F"> F
                                        </label>
                                    </div>
                                    @if ($errors->has('parent_sex'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_sex') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-7 form-group{{ $errors->has('parent_first_name') ? ' has-error' : '' }}">
                                <label for="parent_first_name" class="col-md-3 control-label">Prénom</label>
                                <div class="col-md-9">
                                    <input id="parent_first_name" type="text" class="form-control" name="parent_first_name" value="{{ old('parent_first_name') }}" required>
                                    @if ($errors->has('parent_first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5 form-group{{ $errors->has('parent_localite') ? ' has-error' : '' }}">
                                <label for="parent_localite" class="col-md-3 control-label">Localité</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="parent_localite" name="parent_localite">
                                        @foreach($localities as $l)
                                            <option value="{{$l->code}}">
                                                {{$l->code}} &#8211; {{$l->label_fr}}
                                                @if(!empty($l->label_nl))
                                                    / {{$l->label_nl}}
                                                @endif
                                            </option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('parent_localite'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_localite') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-7 form-group{{ $errors->has('parent_email') ? ' has-error' : '' }}">
                                <label for="parent_email" class="col-md-3 control-label">Email</label>
                                <div class="col-md-9">
                                    <input id="parent_email" type="email" class="form-control" name="parent_email" value="{{ old('parent_email') }}" required>
                                    @if ($errors->has('parent_email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-md-5 form-group{{ $errors->has('parent_phone') ? ' has-error' : '' }}">
                                <label for="parent_phone" class="col-md-3 control-label">Téléphone</label>
                                <div class="col-md-9">
                                    <input id="parent_phone" type="text" class="form-control" name="parent_phone" value="{{ old('parent_phone') }}">
                                    @if ($errors->has('parent_phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('parent_phone') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div id="childPanel1" class="panel panel-info childPanel">
                                    <div class="panel-heading" style="cursor: pointer;">
                                        Premier enfant <i id="checkChild1" class="fa fa-check hideDiv" style="font-size: 14pt;"></i>
                                        <i class="fa fa-caret-down" style="float: right;"></i>
                                    </div>
                                    <div class="panel-body form-horizontal childPanel1">
                                        <div class="form-group{{ $errors->has('child1_last_name') ? ' has-error' : '' }}">
                                            <label for="child1_last_name" class="col-md-3 control-label">Nom</label>
                                            <div class="col-md-8">
                                                <input id="child1_last_name" type="text" class="form-control" name="child1_last_name" value="{{ old('child1_last_name') }}">
                                                @if ($errors->has('child1_last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child1_last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child1_first_name') ? ' has-error' : '' }}">
                                            <label for="child1_first_name" class="col-md-3 control-label">Prénom</label>
                                            <div class="col-md-8">
                                                <input id="child1_first_name" type="text" class="form-control" name="child1_first_name" value="{{ old('child1_first_name') }}">
                                                @if ($errors->has('child1_first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child1_first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child1_sex') ? ' has-error' : '' }}">
                                            <label for="child1_sex" class="col-md-3 control-label">Sexe</label>
                                            <div class="col-md-8">
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child1_sex" id="child1_sexM" value="M"> M
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child1_sex" id="child1_sexF" value="F"> F
                                                    </label>
                                                </div>
                                                @if ($errors->has('child1_sex'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child1_sex') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child1_level') ? ' has-error' : '' }}">
                                            <label for="child1_level" class="col-md-3 control-label">Niveau</label>
                                            <div class="col-md-8" style="line-height:50%;">
                                                <input type="radio" id="child1_primaireRad" name="child1_level" value="1">
                                                <label for="child1_primaireRad" style="font-weight:normal">Primaire</label><br/>
                                                <input type="radio" id="child1_secInfRad" name="child1_level" value="2">
                                                <label for="child1_secInfRad" style="font-weight:normal">Secondaire inférieur</label><br/>
                                                <input type="radio" id="child1_secSupRad" name="child1_level" value="3">
                                                <label for="child1_secSupRad" style="font-weight:normal">Secondaire supérieur</label>
                                                @if ($errors->has('child1_level'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child1_level') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="cursor: pointer;" class="col-md-6">
                                <span class="addChildLink" data-id="childPanel2"><i class="fa fa-user-plus" style="font-size:14pt;color:#31708f;"></i> Ajouter un deuxième enfant</span>
                                <div id="childPanel2" class="panel panel-info childPanel hideDiv">
                                    <div class="panel-heading">
                                        Deuxième enfant <i id="checkChild2" class="fa fa-check hideDiv" style="font-size: 14pt;"></i>
                                        <i class="fa fa-caret-down" style="float: right;"></i>
                                    </div>
                                    <div class="panel-body form-horizontal childPanel2 hideDiv">
                                        <div class="form-group{{ $errors->has('child2_last_name') ? ' has-error' : '' }}">
                                            <label for="child2_last_name" class="col-md-3 control-label">Nom</label>
                                            <div class="col-md-8">
                                                <input id="child2_last_name" type="text" class="form-control" name="child2_last_name" value="{{ old('child2_last_name') }}">
                                                @if ($errors->has('child2_last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child2_last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child2_first_name') ? ' has-error' : '' }}">
                                            <label for="child2_first_name" class="col-md-3 control-label">Prénom</label>
                                            <div class="col-md-8">
                                                <input id="child2_first_name" type="text" class="form-control" name="child2_first_name" value="{{ old('child2_first_name') }}">
                                                @if ($errors->has('child2_first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child2_first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child2_sex') ? ' has-error' : '' }}">
                                            <label for="child2_sex" class="col-md-3 control-label">Sexe</label>
                                            <div class="col-md-8">
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child2_sex" id="child2_sexM" value="M"> M
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child2_sex" id="child2_sexF" value="F"> F
                                                    </label>
                                                </div>
                                                @if ($errors->has('child2_sex'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child2_sex') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child2_level') ? ' has-error' : '' }}">
                                            <label for="child2_level" class="col-md-3 control-label">Niveau</label>
                                            <div class="col-md-8" style="line-height:50%;">
                                                <input type="radio" id="child2_primaireRad" name="child2_level" value="1">
                                                <label for="child2_primaireRad" style="font-weight:normal">Primaire</label><br/>
                                                <input type="radio" id="child2_secInfRad" name="child2_level" value="2">
                                                <label for="child2_secInfRad" style="font-weight:normal">Secondaire inférieur</label><br/>
                                                <input type="radio" id="child2_secSupRad" name="child2_level" value="3">
                                                <label for="child2_secSupRad" style="font-weight:normal">Secondaire supérieur</label>
                                                @if ($errors->has('child2_level'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child2_level') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div style="cursor: pointer;" class="col-md-6">
                                <span class="addChildLink" data-id="childPanel3"><i class="fa fa-user-plus" style="font-size:14pt;color:#31708f;"></i> Ajouter un troisième enfant</span>
                                <div id="childPanel3" class="panel panel-info childPanel hideDiv">
                                    <div class="panel-heading">
                                        Troisième enfant <i id="checkChild3" class="fa fa-check hideDiv" style="font-size: 14pt;"></i>
                                        <i class="fa fa-caret-down" style="float: right;"></i>
                                    </div>
                                    <div class="panel-body form-horizontal childPanel3 hideDiv">
                                        <div class="form-group{{ $errors->has('child3_last_name') ? ' has-error' : '' }}">
                                            <label for="child3_last_name" class="col-md-3 control-label">Nom</label>
                                            <div class="col-md-8">
                                                <input id="child3_last_name" type="text" class="form-control" name="child3_last_name" value="{{ old('child3_last_name') }}">
                                                @if ($errors->has('child3_last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child3_last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child3_first_name') ? ' has-error' : '' }}">
                                            <label for="child3_first_name" class="col-md-3 control-label">Prénom</label>
                                            <div class="col-md-8">
                                                <input id="child3_first_name" type="text" class="form-control" name="child3_first_name" value="{{ old('child3_first_name') }}">
                                                @if ($errors->has('child3_first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child3_first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child3_sex') ? ' has-error' : '' }}">
                                            <label for="child3_sex" class="col-md-3 control-label">Sexe</label>
                                            <div class="col-md-8">
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child3_sex" id="child3_sexM" value="M"> M
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child3_sex" id="child3_sexF" value="F"> F
                                                    </label>
                                                </div>
                                                @if ($errors->has('child3_sex'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child3_sex') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child3_level') ? ' has-error' : '' }}">
                                            <label for="child3_level" class="col-md-3 control-label">Niveau</label>
                                            <div class="col-md-8" style="line-height:50%;">
                                                <input type="radio" id="child3_primaireRad" name="child3_level" value="1">
                                                <label for="child3_primaireRad" style="font-weight:normal">Primaire</label><br/>
                                                <input type="radio" id="child3_secInfRad" name="child3_level" value="2">
                                                <label for="child3_secInfRad" style="font-weight:normal">Secondaire inférieur</label><br/>
                                                <input type="radio" id="child3_secSupRad" name="child3_level" value="3">
                                                <label for="child3_secSupRad" style="font-weight:normal">Secondaire supérieur</label>
                                                @if ($errors->has('child3_level'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child3_level') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="cursor: pointer;" class="col-md-6">
                                <span class="addChildLink" data-id="childPanel4"><i class="fa fa-user-plus" style="font-size:14pt;color:#31708f;"></i> Ajouter un quatrième enfant</span>
                                <div id="childPanel4" class="panel panel-info childPanel hideDiv">
                                    <div class="panel-heading">
                                        Quatrième enfant <i id="checkChild4" class="fa fa-check hideDiv" style="font-size: 14pt;"></i>
                                        <i class="fa fa-caret-down" style="float: right;"></i>
                                    </div>
                                    <div class="panel-body form-horizontal childPanel4 hideDiv">
                                        <div class="form-group{{ $errors->has('child4_last_name') ? ' has-error' : '' }}">
                                            <label for="child4_last_name" class="col-md-3 control-label">Nom</label>
                                            <div class="col-md-8">
                                                <input id="child4_last_name" type="text" class="form-control" name="child4_last_name" value="{{ old('child4_last_name') }}">
                                                @if ($errors->has('child4_last_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child4_last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child4_first_name') ? ' has-error' : '' }}">
                                            <label for="child4_first_name" class="col-md-3 control-label">Prénom</label>
                                            <div class="col-md-8">
                                                <input id="child4_first_name" type="text" class="form-control" name="child4_first_name" value="{{ old('child4_first_name') }}">
                                                @if ($errors->has('child4_first_name'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child4_first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child4_sex') ? ' has-error' : '' }}">
                                            <label for="child4_sex" class="col-md-3 control-label">Sexe</label>
                                            <div class="col-md-8">
                                                <div class="btn-group" data-toggle="buttons">
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child4_sex" id="child4_sexM" value="M"> M
                                                    </label>
                                                    <label class="btn btn-default">
                                                        <input type="radio" name="child4_sex" id="child4_sexF" value="F"> F
                                                    </label>
                                                </div>
                                                @if ($errors->has('child4_sex'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child4_sex') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group{{ $errors->has('child4_level') ? ' has-error' : '' }}">
                                            <label for="child4_level" class="col-md-3 control-label">Niveau</label>
                                            <div class="col-md-8" style="line-height:50%;">
                                                <input type="radio" id="child4_primaireRad" name="child4_level" value="1">
                                                <label for="child4_primaireRad" style="font-weight:normal">Primaire</label><br/>
                                                <input type="radio" id="child4_secInfRad" name="child4_level" value="2">
                                                <label for="child4_secInfRad" style="font-weight:normal">Secondaire inférieur</label><br/>
                                                <input type="radio" id="child4_secSupRad" name="child4_level" value="3">
                                                <label for="child4_secSupRad" style="font-weight:normal">Secondaire supérieur</label>
                                                @if ($errors->has('child4_level'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('child4_level') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('parent_password') ? ' has-error' : '' }}">
                            <label for="parent_password" class="col-md-3 control-label">Mot de passe</label>
                            <div class="col-md-8">
                                <input id="parent_password" type="password" class="form-control" name="parent_password" required>
                                @if ($errors->has('parent_password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('parent_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_password-confirm" class="col-md-3 control-label">Confirmer mot de passe</label>
                            <div class="col-md-8">
                                <input id="parent_password-confirm" type="password" class="form-control" name="parent_password_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="parent_checkConditions" class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input id="parent_checkConditions" type="checkbox" name="parent_checkConditions" required /> J'ai lu et j'accepte les <a href="{{route('conditions')}}" onclick="open('{{url('conditions')}}', 'Popup', 'scrollbars=1,resizable=1,height=560,width=770'); return false;" >conditions d'utilisation</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">
                                    S'inscrire
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>

<script type="text/javascript">
    function showForm(radioValue){
        if(radioValue=='registerStudent'){
            $('#simpleRegistrationForm').toggleClass('hideDiv');
            if(!$('#parentRegistrationForm').hasClass('hideDiv')) $('#parentRegistrationForm').toggleClass('hideDiv');
        }
        if(radioValue=='registerParent'){
            $('#parentRegistrationForm').toggleClass('hideDiv');
            if(!$('#simpleRegistrationForm').hasClass('hideDiv')) $('#simpleRegistrationForm').toggleClass('hideDiv');
        }
    }
    function showDiv(t){
        if($("input[name=type]:checked")) $("#divInfotype"+t).show();
        if(t==1) $("#divInfotype0").hide();
        if(t==0) $("#divInfotype1").hide();
    }
    function checkPanel(n){
        $('#child'+n+'_last_name').keypress(function(){
            if($('#child'+n+'_last_name').val()!='' && $('#child'+n+'_first_name').val()!='' && $('input[name="child'+n+'_sex"]:checked').val() && $('input[name="child'+n+'_level"]:checked').val()){
                $('#childPanel'+n).removeClass('panel-info').addClass('panel-success');
                $('#checkChild'+n).removeClass('hideDiv');
            }
        });
        $('#child'+n+'_first_name').keypress(function(){
            if($('#child'+n+'_last_name').val()!='' && $('#child'+n+'_first_name').val()!='' && $('input[name="child'+n+'_sex"]:checked').val() && $('input[name="child'+n+'_level"]:checked').val()){
                $('#childPanel'+n).removeClass('panel-info').addClass('panel-success');
                $('#checkChild'+n).removeClass('hideDiv');
            }
        });
        $('input[name="child'+n+'_sex"]').on('click', function(){
            if($('#child'+n+'_last_name').val()!='' && $('#child'+n+'_first_name').val()!='' && $('input[name="child'+n+'_sex"]:checked').val() && $('input[name="child'+n+'_level"]:checked').val()){
                $('#childPanel'+n).removeClass('panel-info').addClass('panel-success');
                $('#checkChild'+n).removeClass('hideDiv');
            }
        });
        $('input[name="child'+n+'_level"]').on('click', function(){
            if($('#child'+n+'_last_name').val()!='' && $('#child'+n+'_first_name').val()!='' && $('input[name="child'+n+'_sex"]:checked').val() && $('input[name="child'+n+'_level"]:checked').val()){
                $('#childPanel'+n).removeClass('panel-info').addClass('panel-success');
                $('#checkChild'+n).removeClass('hideDiv');
            }
        });
    }
    $(document).ready(function () {
        $('.addChildLink').on('click',function(e){
            $(this).addClass('hideDiv');
            var div = $(this).data('id');
            $('#'+div).toggleClass('hideDiv');
            $('.'+div).toggleClass('hideDiv');
        });
        $('.panel-heading').on('click', function(){
            var id = $(this).parent().attr('id');
            $('.'+id).slideToggle(500);
        });
        $('#childPanel1').focusin(checkPanel('1'));
        $('#childPanel2').focusin(checkPanel('2'));
        $('#childPanel3').focusin(checkPanel('3'));
        $('#childPanel4').focusin(checkPanel('4'));
    });
</script>