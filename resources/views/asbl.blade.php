<style type="text/css">
  nav.sidebar, .main{
	-webkit-transition: margin 200ms ease-out;
	  -moz-transition: margin 200ms ease-out;
	  -o-transition: margin 200ms ease-out;
	  transition: margin 200ms ease-out;
  }

  .main{
	padding: 10px 10px 0 10px;
  }

 @media (min-width: 765px) {

	.main{
	  position: absolute;
	  width: calc(100% - 40px); 
	  margin-left: 40px;
	  float: right;
	}

	nav.sidebar:hover + .main{
	  margin-left: 200px;
	}

	nav.sidebar.navbar.sidebar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
	  margin-left: 0px;
	}

	nav.sidebar .navbar-brand, nav.sidebar .navbar-header{
	  text-align: center;
	  width: 100%;
	  margin-left: 0px;
	}
	
	nav.sidebar a{
	  padding-right: 13px;
	}

	nav.sidebar .navbar-nav > li:first-child{
	  border-top: 1px #e5e5e5 solid;
	}

	nav.sidebar .navbar-nav > li{
	  border-bottom: 1px #e5e5e5 solid;
	}

	nav.sidebar .navbar-nav .open .dropdown-menu {
	  position: static;
	  float: none;
	  width: auto;
	  margin-top: 0;
	  background-color: transparent;
	  border: 0;
	  -webkit-box-shadow: none;
	  box-shadow: none;
	}

	nav.sidebar .navbar-collapse, nav.sidebar .container-fluid{
	  padding: 0 0px 0 0px;
	}

	.navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
	  color: #777;
	}

	nav.sidebar{
	  width: 200px;
	  height: 100%;
	  margin-left: -160px;
	  float: left;
	  margin-bottom: 0px;
	}

	nav.sidebar li {
	  width: 100%;
	}

	nav.sidebar:hover{
	  margin-left: 0px;
	}

	.forAnimate{
	  opacity: 0;
	}
  }
   
  @media (min-width: 1330px) {

	.main{
	  width: calc(100% - 200px);
	  margin-left: 200px;
	}

	nav.sidebar{
	  margin-left: 0px;
	  float: left;
	}

	nav.sidebar .forAnimate{
	  opacity: 1;
	}
  }

  nav.sidebar .navbar-nav .open .dropdown-menu>li>a:hover, nav.sidebar .navbar-nav .open .dropdown-menu>li>a:focus {
	color: #CCC;
	background-color: transparent;
  }

  nav:hover .forAnimate{
	opacity: 1;
  }
  section{
	padding-left: 15px;
  }
  .hideDiv{
	display: none;
  }
  .showingDiv{
	margin-left: 250px; 
	margin-right: 50px; 
	margin-top: 2%;
  }
</style>

@extends('layouts.app')

@section('content')
<nav class="navbar navbar-default sidebar" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>      
		</div>
		<div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
			<ul class="nav navbar-nav" id="asblSideNavId">
				<li><a href="#" id="idCreateTeacher">Inscrire un professeur<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-plus"></span></a></li>
				<li><a href="#" id="idCreateEvent">Permanences<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-pencil"></span></a></li>        
				<li><a href="#" id="idListTeachers">Liste des professeurs<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-user"></span></a></li>
				<li><a href="#" id="idContactStudent">Suivi élèves<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-comment"></span></a></li>
				<li><a href="#" id="idBannedUsers">Utilisateurs bannis<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-ban"></span></a></li>
				<li><a href="#" id="idContactAdmin">Contacter les administrateurs<span style="font-size:16px;" class="pull-right hidden-xs showopacity fa fa-envelope"></span></a></li>
			</ul>
		</div>
	</div>
</nav>

<div id="divAsblContent">
	<div id="divBtnAsbl">
		<div class="col-md-3">
	        <a href="#" class="btn btn-default btn-lg btn-block" id="btnCreateTeacher" style="padding-top: 20pt;padding-bottom: 20pt;margin:20pt;border-color:#337ab7">Inscrire un professeur</a>
	    </div>
	    <div class="col-md-3">
	        <a href="#" class="btn btn-default btn-lg btn-block" id="btnCreateEvent" style="padding-top: 20pt;padding-bottom: 20pt;margin:20pt;border-color:#5cb85c">Permanences</a>
	    </div>
	    <div class="col-md-3">
	        <a href="#" class="btn btn-default btn-lg btn-block" id="btnListTeachers" style="padding-top: 20pt;padding-bottom: 20pt;margin:20pt;border-color:#5bc0de">Liste des professeurs</a>
	    </div>
	    <div class="col-md-3">
	        <a href="#" class="btn btn-default btn-lg btn-block" id="btnContactStudent" style="padding-top: 20pt;padding-bottom: 20pt;margin:20pt;border-color:#f0ad4e">Suivi élèves</a>
	    </div>
	    <div class="col-md-3">
	        <a href="#" class="btn btn-default btn-lg btn-block" id="btnBannedUsers" style="padding-top: 20pt;padding-bottom: 20pt;margin:20pt;border-color:#d43f3a">Utilisateurs bannis</a>
	    </div>
	    <div class="col-md-3">
	        <a href="#" class="btn btn-default btn-lg btn-block" id="btnContactAdmin" style="padding-top: 20pt;padding-bottom: 20pt;margin:20pt;border-color:#000000">Contacter les administrateurs</a>
	    </div>
	</div>
	@include('asbl.createTeacher')
	@include('asbl.createEvent')
	@include('asbl.listTeachers')
	@include('asbl.contactStudent')
	@include('asbl.bannedUsers')
	@include('asbl.contactAdmin')
</div>
@endsection

<script src="{{ asset('js/jquery.js') }}"></script>
<script>
$(document).ready(function() {
	$('#asblSideNavId li a').on('click', function(){
		$('#divAsblContent').children('div').addClass('hideDiv');
		var idLink = $(this).attr('id');
		var idDiv = '#div' + idLink.substring(2);
		$(idDiv).removeClass('hideDiv');
	});
	$('.btn-block').on('click', function(){
		$('#divAsblContent').children('div').addClass('hideDiv');
		var idLink = $(this).attr('id');
		var idDiv = '#div' + idLink.substring(3);
		$(idDiv).removeClass('hideDiv');
	});
	$('.closeDivIcon').on('click', function(){
		var idDiv = $(this).parent().attr('id');
		$('#'+idDiv).addClass('hideDiv');
		$('#divBtnAsbl').removeClass('hideDiv');
	});
});
</script>