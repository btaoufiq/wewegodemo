@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="panel panel-default">
			<div class="panel-heading"><h4>Administration</h4></div>

			<div class="panel-body form-horizontal">
				<div class="panel-body" style="text-align:right">
					<a href="{{ url('/') }}">Retour à l'accueil</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection