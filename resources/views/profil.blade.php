@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading"><h4>Mon profil</h4></div>

				<div class="panel-body form-horizontal">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					<form method="POST" action="{{ url('profil') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<label for="last_name" class="col-md-4 control-label">Nom</label>
							<div class="col-md-6">
								<input id="last_name" type="text" class="form-control" name="last_name" value="{{ $user['last_name'] }}" required>
								@if ($errors->has('last_name'))
									<span class="help-block">
										<strong>{{ $errors->first('last_name') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="first_name" class="col-md-4 control-label">Prénom</label>
							<div class="col-md-6">
								<input id="first_name" type="text" class="form-control" name="first_name" value="{{ $user['first_name'] }}" required>
								@if ($errors->has('first_name'))
									<span class="help-block">
										<strong>{{ $errors->first('first_name') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-md-4 control-label">Addresse email</label>
							<div class="col-md-6">
								<input id="email" type="email" class="form-control" name="email" value="{{ $user['email'] }}" required>
								@if ($errors->has('email'))
									<span class="help-block">
										<strong>{{ $errors->first('email') }}</strong>
									</span>
								@endif
							</div>
						</div>
						<?php $localities = DB::table('localities')->get(); ?>
						<div class="form-group{{ $errors->has('localite') ? ' has-error' : '' }}">
							<label for="localite" class="col-md-4 control-label">Localité</label>
							<div class="col-md-6">
								<select class="form-control" id="localite" name="localite">
									@foreach($localities as $l)
										<option value="{{$l->code}}" <?php if($l->code == $user['localite']) echo "selected"; ?>>
											{{$l->code}} &#8211; {{$l->label_fr}}
											@if(!empty($l->label_nl))
												/ {{$l->label_nl}}
											@endif
										</option>
									@endforeach
								</select>
								@if ($errors->has('localite'))
									<span class="help-block">
										<strong>{{ $errors->first('localite') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="phone" class="col-md-4 control-label">Téléphone</label>
							<div class="col-md-6">
								<input id="phone" type="text" class="form-control" name="phone" value="{{ $user['phone'] }}" required>
								@if ($errors->has('phone'))
									<span class="help-block">
										<strong>{{ $errors->first('phone') }}</strong>
									</span>
								@endif
							</div>
						</div>
						@if($user['type']==1)
							<?php 
								$level = DB::table('user_level')->where('user_id','=',$user['id'])->get();
								$levelToShow = array(0, 0, 0);
								foreach ($level as $l) {
									if($l->primaire == 1) $levelToShow[0] = 1;
									if($l->sec_inf == 1) $levelToShow[1] = 1;
									if($l->sec_sup == 1) $levelToShow[2] = 1;
								}
							?>
							<div class="form-group">
								<label for="level" class="col-md-4 control-label">Niveau</label>
								<div class="col-md-6" style="line-height:75%;">
									<input type="checkbox" id="primaireChk" name="level[]" value="1" <?php if($levelToShow[0]==1) echo "checked"; ?>>
									<label for="primaireChk" style="font-weight:normal">Primaire</label><br/>
									<input type="checkbox" id="secInfChk" name="level[]" value="2" <?php if($levelToShow[1]==1) echo "checked"; ?>>
									<label for="secInfChk" style="font-weight:normal">Secondaire inférieur</label><br/>
									<input type="checkbox" id="secSupChk" name="level[]" value="3" <?php if($levelToShow[2]==1) echo "checked"; ?>>
									<label for="secSupChk" style="font-weight:normal">Secondaire supérieur</label>
								</div>
							</div>
							<?php 
								$course = DB::table('user_course')->where('user_id','=',$user['id'])->get();
								$courseToInsert = array(0, 0, 0, 0, 0, 0, 0, 0);
								foreach ($course as $c) {
									if($c->edm == 1) $courseToInsert[0] = 1;
									if($c->math == 1) $courseToInsert[1] = 1;
									if($c->francais == 1) $courseToInsert[2] = 1;
									if($c->neerlandais == 1) $courseToInsert[3] = 1;
									if($c->anglais == 1) $courseToInsert[4] = 1;
									if($c->chimie == 1) $courseToInsert[5] = 1;
									if($c->biologie == 1) $courseToInsert[6] = 1;
									if($c->physique == 1) $courseToInsert[7] = 1;
								}
							?>
							<div class="form-group">
								<label for="course" class="col-md-4 control-label">Matière(s)</label>
								<div class="col-md-6" style="line-height:75%;">
									<input type="checkbox" id="edmChk" name="course[]" value="1" <?php if($courseToInsert[0]==1) echo "checked"; ?>>
									<label for="emdChk" style="font-weight:normal">EDM</label>
									<br/>
									<input type="checkbox" id="mathChk" name="course[]" value="2" <?php if($courseToInsert[1]==1) echo "checked"; ?>>
									<label for="mathChk" style="font-weight:normal">Math</label>&emsp;
									<input type="checkbox" id="francaisChk" name="course[]" value="3" <?php if($courseToInsert[2]==1) echo "checked"; ?>>
									<label for="francaisChk" style="font-weight:normal">Français</label>
									<br/>
									<input type="checkbox" id="neerlandaisChk" name="course[]" value="4" <?php if($courseToInsert[3]==1) echo "checked"; ?>>
									<label for="neerlandaisChk" style="font-weight:normal">Néerlandais</label>&emsp;
									<input type="checkbox" id="anglaisChk" name="course[]" value="5" <?php if($courseToInsert[4]==1) echo "checked"; ?>>
									<label for="anglaisChk" style="font-weight:normal">Anglais</label>
									<br/>
									<input type="checkbox" id="chimieChk" name="course[]" value="6" <?php if($courseToInsert[5]==1) echo "checked"; ?>>
									<label for="chimieChk" style="font-weight:normal">Chimie</label>&emsp;
									<input type="checkbox" id="biologieChk" name="course[]" value="7" <?php if($courseToInsert[6]==1) echo "checked"; ?>>
									<label for="biologieChk" style="font-weight:normal">Biologie</label>&emsp;
									<input type="checkbox" id="physiqueChk" name="course[]" value="8" <?php if($courseToInsert[7]==1) echo "checked"; ?>>
									<label for="physiqueChk" style="font-weight:normal">Physique</label>
								</div>
							</div>
						@endif
						@if($user['type']==0)
							<?php 
								$level = DB::table('user_level')->where('user_id','=',$user['id'])->get();
								$levelToShow = array(0, 0, 0);
								foreach ($level as $l) {
									if($l->primaire == 1) $levelToShow[0] = 1;
									if($l->sec_inf == 1) $levelToShow[1] = 1;
									if($l->sec_sup == 1) $levelToShow[2] = 1;
								}
							?>
							<div class="form-group">
								<label for="level" class="col-md-4 control-label">Niveau</label>
								<div class="col-md-6" style="line-height:75%;">
									<input type="radio" id="primaireRad" name="level[]" value="1" <?php if($levelToShow[0]==1) echo "checked"; ?>>
									<label for="primaireRad" style="font-weight:normal">Primaire</label><br/>
									<input type="radio" id="secInfRad" name="level[]" value="2" <?php if($levelToShow[1]==1) echo "checked"; ?>>
									<label for="secInfRad" style="font-weight:normal">Secondaire inférieur</label><br/>
									<input type="radio" id="secSupRad" name="level[]" value="3" <?php if($levelToShow[2]==1) echo "checked"; ?>>
									<label for="secSupRad" style="font-weight:normal">Secondaire supérieur</label>
								</div>
							</div>
						@endif
						<div class="panel-body" style="text-align:right">
							<div class="col-md-4">
								<a class="btn btn-link" href="{{ url('changepassword') }}">
									Changer mon mot de passe
								</a>
							</div>
							<button type="submit" class="btn btn-primary">Enregistrer</button>&emsp;
							<a href="{{ url('/') }}">Retour à l'accueil</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
