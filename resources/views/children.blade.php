@extends('layouts.app')

@section('content')

@if(count($children)!=0)
<div class="container">
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-default">
			<div class="panel-heading"><h4>Mes enfants</h4></div>
			<div class="panel-body form-horizontal">
				<form method="POST" action="{{ url('children') }}">
					{{ csrf_field() }}
					<div class="form-group">
						@foreach($children as $pos => $child)
							<div class="form-group col-md-10">
								<div class="col-md-3">
									{{$child->first_name}} {{$child->last_name}}
									<input type="hidden" name="child{{$pos}}" value="{{$child->id}}">
								</div>
								<div class="col-md-6">
									<select class="form-control" name="childLevel_{{$child->id}}">
										@foreach($levels as $l)
											<option value="{{ $l->id }}" @if($child->level==$l->id) selected @endif >{{ $l->label_fr }}</option>
										@endforeach
									</select>
								</div>
							</div>
						@endforeach
					</div>
					<div class="panel-body" style="text-align:right">
						<button type="submit" class="btn btn-primary">Enregistrer</button>&emsp;
						<a href="{{ url('/') }}">Retour à l'accueil</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
@endif

@endsection