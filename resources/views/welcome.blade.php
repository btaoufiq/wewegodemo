<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name') }} - Bienvenue</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('css/sweetalert.min.css') }}" rel="stylesheet">
        <script src="{{ asset('js/jquery.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/events.js') }}"></script>
        <script src="{{ asset('js/sweetalert-dev.js') }}"></script>

        <!-- Styles -->
        <style>
            /*html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }*/

            .full-height {
                height: 100%;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .showInfoCard{
                cursor: pointer;
            }
        </style>
    </head>
    <body style="display: none;">
        <div class="container">
            <div class="row">
                <table class="table">
                    <thead>
                        <th colspan="2" style="text-align: center;">
                            <h1>Donnez votre avis !</h1>
                        </th>
                    </thead>
                    <tbody>
                        @foreach($events as $e)
                        <tr>
                            <td class="col-md-4" style="line-height: 1">
                                <p>
                                    @if($e->course_label == null) <b>{{$e->level_label}}</b>
                                    @else <b>{{$e->course_label}}</b> / {{$e->level_label}}
                                    @endif
                                    @if($e->private == '1') <span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>@endif
                                </p>
                                <p><i class="fa fa-calendar-o" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($e->dhstart)) }} &emsp; <i class="fa fa-clock-o" aria-hidden="true"></i> {{ date('H', strtotime($e->dhstart)) }}h-{{ date('H', strtotime($e->dhend)) }}h</p>
                                <p>Professeur : <span class="showInfoCard" data-toggle="modal" data-target="#ModalShowInfoCard" data-id="{{$myTeachers[$e->id]->id}}"><u>{{$myTeachers[$e->id]->first_name}} {{$myTeachers[$e->id]->last_name}}</u></span></p>
                                <p><i>{{$e->descr}}</i></p>
                            </td>
                            <td class="col-md-8">
                                <div id="ratingForm{{$e->id}}">
                                    <div class="form-group">
                                        <input type="radio" name="opinion{{$e->id}}" value="yes"> Le cours s'est bien déroulé &emsp; 
                                        <input type="radio" name="opinion{{$e->id}}" value="no"> Le cours ne s'est pas bien déroulé
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <select class="form-control" name="rate" id="rate{{$e->id}}">
                                                <option value="0">Degré de satisfaction</option>
                                                <option value="5">5 &#8211; Excellent</option>
                                                <option value="4">4 &#8211; Très bon</option>
                                                <option value="3">3 &#8211; Bon</option>
                                                <option value="2">2 &#8211; Passable</option>
                                                <option value="1">1 &#8211; Médiocre</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6"><input type="text" name="comment" id="comment{{$e->id}}" class="form-control" placeholder="Commentaire (falcutatif)"></div>
                                    </div>
                                    <div class="text-right" style="padding-top: 7%;">
                                        <button class="btn btn-primary btn-xs confirmRating" id="confirmRating{{$e->id}}" data-id="{{$e->id}}">Confirmer</button>
                                    </div>
                                    <input type="hidden" name="ratingForm_idEvent" class="form-control" id="ratingForm_idEvent{{$e->id}}" value="{{$e->id}}">
                                    <input type="hidden" name="ratingForm_idTeacher" class="form-control" id="ratingForm_idTeacher{{$e->id}}" value="{{$myTeachers[$e->id]->id}}">
                                    <input type="hidden" name="ratingForm_idStudent" class="form-control" id="ratingForm_idStudent{{$e->id}}" value="{{Auth::user()->id}}">
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="flex-center position-ref full-height">
                    <div class="content">
                        <a class="btn btn-info btn-lg" href="{{ url('/') }}" id="nextLink">Passer</a>
                    </div>
                </div>
            </div>
        </div>
        @include('modals.showInfoCardModal');
    </body>
</html>

<script>
    $(document).ready(function () {
        $('body').fadeIn(500);
        $('body').css('display', '');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.showInfoCard').click(function(e){
            e.preventDefault();
            $.ajax({
                'url': '/getInfoCard/' + $(this).data('id'),
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'failed') {
                        swal('Oops', res.message, 'error');
                    }
                    else if (res.status == 'success') {
                        showInfoCardModal(res);
                    }
                }
            });
        });
        $('.confirmRating').click(function(e){
            e.preventDefault();
            var event_id = $(this).data('id');
            if ($('input[name="opinion'+event_id+'"]:checked').val() && $('#rate'+event_id).val()!=0) {
                var data = {};
                var arr = [];
                data['event_id'] = event_id;
                data['teacher_id'] = $('#ratingForm_idTeacher'+event_id).val();
                data['student_id'] = $('#ratingForm_idStudent'+event_id).val();
                data['opinion'] = $('input[name="opinion'+event_id+'"]:checked').val();
                data['rate'] = $('#rate'+event_id).val();
                data['comment'] = $('#comment'+event_id).val();
                arr.push(data);
                $.ajax({
                    'url': '/rating',
                    'type': 'POST',
                    'data': arr[0],
                    'success': function (res) {
                        $('#ratingForm'+event_id).html('<h4><i>Merci d\'avoir donné votre avis !</h4></i>').css('text-align','center').css('color','#245580');
                        $('#ratingForm'+event_id).parent().css('vertical-align','middle');
                    }
                });
            }
            else {
                swal('Oops','Il vous faut donner votre avis et votre degré de satisfaction pour l\'enregistrement','error');
            }
        });
    });
</script>