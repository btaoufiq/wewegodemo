@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#myUpcommingEvents" aria-controls="myUpcommingEvents" role="tab" data-toggle="tab">Prochains cours
                @if($countUpcommingEventsNotificationIds != 0)
                    <span class="badge tabBadge" style="font-size:10px;margin-top: -18px;margin-left: -6px;">
                        {{$countUpcommingEventsNotificationIds}}
                    </span>
                @endif
            </a></li>
            <li role="presentation"><a href="#myPastEvents" aria-controls="myPastEvents" role="tab" data-toggle="tab">Historique
                @if($countPastEventsNotificationIds != 0)
                    <span class="badge tabBadge" style="font-size:10px;margin-top: -18px;margin-left: -6px;">
                        {{$countPastEventsNotificationIds}}
                    </span>
                @endif
            </a></li>
            <li role="presentation"><a href="#myDeclinedEvents" aria-controls="myDeclinedEvents" role="tab" data-toggle="tab">Désinscrit / Déclinés
                @if($countDeclinedEventsNotificationIds != 0)
                    <span class="badge tabBadge" style="font-size:10px;margin-top: -18px;margin-left: -6px;">
                        {{$countDeclinedEventsNotificationIds}}
                    </span>
                @endif
            </a></li>
            <li role="presentation"><a href="#invalidEvents" aria-controls="invalidEvents" role="tab" data-toggle="tab">Cours invalidés
                @if($countInvalidEventsNotificationIds != 0)
                    <span class="badge tabBadge" style="font-size:10px;margin-top: -18px;margin-left: -6px;">
                        {{$countInvalidEventsNotificationIds}}
                    </span>
                @endif
            </a></li>
        </ul>
        <div class="col-md-12 tab-content" style="margin-top: 20pt">
            <div id="myUpcommingEvents" role="tabpanel" class="tab-pane active">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-list">
                        @if(count($upcommingEvents)!=0)
                        @foreach($upcommingEvents as $e)
                        <tr>
                            <td>
                                <div class="toggleNotification" style="cursor: pointer">
                                    @if($e->course_label == null) <b>{{$e->level_label}}</b>
                                    @else <b>{{$e->course_label}}</b> / {{$e->level_label}}
                                    @endif
                                    @if($e->private == '1') <span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>@endif
                                    @if($e->type==1) <span class="label label-default" style="font-size: 10px;background-color: #ff5a00;font-weight: normal;">Weweclass</span> @endif
                                    @foreach($notifications as $n)
                                        @if(in_array($e->id,$notificationEventIds) and $n->event_id==$e->id and $n->readit==0)
                                            <span class="pull-right iconToggle" data-id="{{$n->id}}" id="iconToggle{{$n->id}}" data-type="upcomming">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        @endif
                                    @endforeach
                                    <span style="padding-right: 10pt; float: right;font-size: 10pt;"><i class="fa fa-calendar-o" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($e->dhstart)) }} &emsp; <i class="fa fa-clock-o" aria-hidden="true"></i> {{ date('H', strtotime($e->dhstart)) }}h-{{ date('H', strtotime($e->dhend)) }}h</span>
                                </div>
                                <ul class="list-group" style="display: none;margin-top: 5px;box-shadow: 0px 0px 5px {{$e->color}}">
                                    <li class="list-group-item" style="line-height:1;padding: 2%;">
                                        <p>Le <b>{{ date('d/m/Y', strtotime($e->dhstart)) }}</b> de <b>{{ date('H', strtotime($e->dhstart)) }}h</b> à <b>{{ date('H', strtotime($e->dhend)) }}h</b>@if(Auth::user()->type==4), pour <u>{{$e->child_first_name}}</u>@endif</p>
                                        @if(Auth::user()->id!=$e->owner and $e->owner_type!=3)
                                        <p>Propriétaire : <span class="showInfoCard" data-toggle="modal" data-target="#ModalShowInfoCard" data-id="{{$e->owner}}"><u>{{$e->owner_name}}</u></span> à {{$e->owner_locality}}</p>
                                        @endif
                                        @if($e->type==0)
                                            <p>@if($e->student_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile élève
                                            </p>
                                            <p>@if($e->teacher_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile professeur
                                            </p>
                                        @else
                                            <p>
                                                @if($e->address=='')<i>Pas d'adresse définie</i>
                                                @else Adresse : <b>{{$e->address}}</b>
                                                @endif
                                            </p>
                                        @endif
                                        <p>
                                            @if($e->descr=='')<i>Pas de description</i>
                                            @else Description : {{$e->descr}}
                                            @endif
                                        </p>
                                    </li>
                                    @foreach($notifications as $notification)
                                        @if($notification->event_id == $e->id)
                                            <li class="list-group-item">
                                                <div style="display:inline-block;text-align:right;float:right;width: 50%" class="pull-right">
                                                    @if($notification->showoption)
                                                        @if($notification->showoption == 1)
                                                            @if(!in_array($notification->id,$declinedNotificationIds))
                                                                @if(!in_array($notification->id,$acceptedNotificationIds))
                                                                <?php
                                                                    $usertype = DB::table('users')->where('id',$notification->creator_id)->select('type')->first();
                                                                ?>
                                                                    @if($usertype->type==1)
                                                                        @if(!@empty($acceptedTeachersNotificationIds) and in_array($e->id,array_keys($acceptedTeachersNotificationIds)) and !in_array($notification->id,$acceptedTeachersNotificationIds))
                                                                            <span class="small text-info">
                                                                                <i class="fa fa-minus-circle" aria-hidden="true"></i> Un professeur a déja été accepté
                                                                            </span>
                                                                        @else
                                                                            <a class="btn btn-success btn-xs acceptEvent @if($e->invalide==2) disabled @endif" data-id="{{$notification->id}}">Accepter</a>
                                                                        @endif
                                                                    @else
                                                                        <a class="btn btn-success btn-xs acceptEvent @if($e->invalide==2) disabled @endif" data-id="{{$notification->id}}">Accepter</a>
                                                                    @endif
                                                                @elseif(in_array($notification->id,$acceptedNotificationIds))
                                                                    <span class="small text-info">
                                                                        <i class="fa fa-check-circle" aria-hidden="true"></i> Accepté(e)
                                                                    </span>
                                                                @endif
                                                                <a class="btn btn-danger btn-xs declineEvent @if($e->invalide==2) disabled @endif" data-id="{{$notification->id}}">Décliner</a>
                                                            @elseif(in_array($notification->id,$declinedNotificationIds))
                                                                <span class="small text-info">
                                                                    <i class="fa fa-times-circle" aria-hidden="true"></i> Décliné(e)
                                                                </span>
                                                            @endif
                                                        @elseif($notification->showoption == 2)
                                                            @if((!in_array($notification->id,$changedNotificationIds)))
                                                                <span class="small text-info">
                                                                    <a class="btn btn-primary btn-xs duplicateEvent @if($e->invalide==2) disabled @endif" data-id="{{$notification->id}}" title="Dupliquer">Programmer un cours pour ce participant</a>
                                                                    <a class="btn btn-danger btn-xs notPossibleEvent @if($e->invalide==2) disabled @endif" data-id="{{$notification->id}}" title="Refuser">Refuser</a>
                                                                    <p id="infoSuitMe">Cette personne n'est plus inscrite.<br>
                                                                    En programmant un nouveau cours, vous créerez le même cours à la date proposée et ce participant sera inscrit d'office.</p>
                                                                </span>
                                                            @endif
                                                        @elseif($notification->showoption == 3)
                                                            <span class="small text-info">
                                                                <p>Ce cours a été déplacé. Souhaitez-vous {{(Auth()->user()->type == 1) ? 'vous propser' : 'y participer'}} ?</p>
                                                                @if(Auth()->user()->type == 1)
                                                                <a class="btn btn-success btn-xs proposeNewEvent" data-id="{{$notification->id}}" style="margin-right: 10px">Se proposer</a>
                                                                @elseif(Auth()->user()->type == 0)
                                                                <a class="btn btn-success btn-xs confirmNewEvent" data-id="{{$notification->id}}" style="margin-right: 10px">Particper</a>
                                                                @endif
                                                                <a class="btn btn-danger btn-xs declineNewEvent" data-id="{{$notification->id}}" style="margin-right: 10px">Refuser</a>
                                                            </span>
                                                        @elseif($notification->showoption == 4)
                                                            <span class="small text-info">
                                                                <p>Souhaitez-vous reconfirmer votre participation à ce cours ?</p>
                                                                <a class="btn btn-success btn-xs reconfirmNewEvent" data-id="{{$notification->id}}" style="margin-right: 10px">Reconfirmer</a>
                                                                <a class="btn btn-danger btn-xs redeclineNewEvent" data-id="{{$notification->id}}" style="margin-right: 10px">Refuser</a>
                                                            </span>
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="notificationDateTime">{{ date('d/m/Y', strtotime($notification->created_at)) }}, {{ date('H:i', strtotime($notification->created_at)) }}</div>
                                                <div>
                                                    {{trim($notification->description)}}
                                                    @if($e->teacher_home==1 and $e->student_home==1)
                                                    <p>
                                                        @foreach($eventsUsers as $eu)
                                                            @if($e->id==$eu->event_id and $eu->user_id==$notification->creator_id)
                                                                → Mobilité sélectionnée : <b>{{ ($eu->mobility==1) ? 'Domicile professeur' : 'Domicile élève' }}</b>
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                    @endif
                                                </div>
                                                @if(count($notification->messages))
                                                    <ul class="list-group" style="margin-top: 15px;">
                                                        <b><i class="fa fa-comments-o"></i> Messages </b>
                                                        <span>
                                                            @foreach($notification->messages as $message)
                                                                <div style="margin-left: 10pt">{{($message->message_from == Auth()->user()->id) ? 'Vous avez écrit : ' : $message->sender->first_name.' '.$message->sender->last_name.' : ' }}{{$message->description}}</div>
                                                            @endforeach
                                                        </span>
                                                    </ul>
                                                @endif
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                            <td class="col-md-3" style="text-align: center;line-height: 1;">
                                @if(Auth::user()->id == $e->owner)
                                    <button class="btn btn-info btn-sm doesNotSuit @if($e->type==1 or $e->type==2) disabled @endif" data-id="{{$e->id}}" data-suitDatepicker="{{date('d/m/Y', strtotime($e->dhstart))}}" data-suitDhstart="{{date('H', strtotime($e->dhstart))}}" data-suitHours="{{$e->duration}}" data-suitDhend="{{date('H', strtotime($e->dhend))}}" data-suitOwner="{{$e->owner}}">
                                        Déplacer le cours
                                    </button>
                                    <button class="btn btn-danger btn-sm btnDeleteFromMyList" data-id="{{$e->id}}">
                                        Supprimer
                                    </button>
                                @else
                                    @if(Auth()->user()->type == 1)
                                        <button class="btn btn-warning btn-sm withdrawEditEvent @if($e->accepted == 2) disabled @endif " data-id="{{$e->id}}">
                                            Se désinscrire
                                        </button>
                                    @elseif(Auth()->user()->type == 0 || Auth()->user()->type == 4)
                                        <button class="btn btn-warning btn-sm unsubscribeEditEvent @if($e->accepted == 2) disabled @endif " data-id="{{$e->id}}">
                                            Se désinscrire
                                        </button>
                                    @endif
                                    @if($e->type != 1)
                                    <button class="btn btn-info btn-sm doesNotSuit @if($e->accepted != 1 or $e->invalide==2) disabled @endif " data-id="{{$e->id}}" data-suitDatepicker="{{date('d/m/Y', strtotime($e->dhstart))}}" data-suitDhstart="{{date('H', strtotime($e->dhstart))}}" data-suitHours="{{$e->duration}}" data-suitDhend="{{date('H', strtotime($e->dhend))}}" data-suitOwner="{{$e->owner}}">
                                        Déplacer le cours
                                    </button>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td style="font-style: italic;">
                                Pas de cours à afficher
                            </td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div id="myPastEvents" role="tabpanel" class="tab-pane">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-list" id="myPastEventsTable">
                        @if(count($pastEvents)!=0)
                        @foreach($pastEvents as $e)
                        <tr>
                            <td>
                                <div class="toggleNotification" style="cursor: pointer">
                                    @if($e->course_label == null) <b>{{$e->level_label}}</b>
                                    @else <b>{{$e->course_label}}</b> / {{$e->level_label}}
                                    @endif
                                    @if($e->private == '1') <span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>@endif
                                    @if($e->type==1) <span class="label label-default" style="font-size: 10px;background-color: #ff5a00;font-weight: normal;">Weweclass</span> @endif
                                    @foreach($notifications as $n)
                                        @if(in_array($e->id,$notificationEventIds) and $n->event_id==$e->id and $n->readit==0)
                                            <span class="pull-right iconToggle" data-id="{{$n->id}}" id="iconToggle{{$n->id}}" data-type="past">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        @endif
                                    @endforeach
                                    <span style="padding-right: 10pt; float: right;font-size: 10pt;"><i class="fa fa-calendar-o" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($e->dhstart)) }} &emsp; <i class="fa fa-clock-o" aria-hidden="true"></i> {{ date('H', strtotime($e->dhstart)) }}h-{{ date('H', strtotime($e->dhend)) }}h</span>
                                </div>
                                <ul class="list-group" style="display: none;margin-top: 5px;box-shadow: 0px 0px 5px {{$e->color}}">
                                    <li class="list-group-item" style="line-height:1;padding: 2%;">
                                        <p>Le <b>{{ date('d/m/Y', strtotime($e->dhstart)) }}</b> de <b>{{ date('H', strtotime($e->dhstart)) }}h</b> à <b>{{ date('H', strtotime($e->dhend)) }}h</b>@if(Auth::user()->type==4), pour <u>{{$e->child_first_name}}</u>@endif</p>
                                        @if(Auth::user()->id!=$e->owner and $e->owner_type!=3)
                                        <p>Propriétaire : <span class="showInfoCard" data-toggle="modal" data-target="#ModalShowInfoCard" data-id="{{$e->owner}}"><u>{{$e->owner_name}}</u></span> à {{$e->owner_locality}}</p>
                                        @endif
                                        @if($e->type==0)
                                            <p>@if($e->student_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile élève
                                            </p>
                                            <p>@if($e->teacher_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile professeur
                                            </p>
                                        @else
                                            <p>
                                                @if($e->address=='')<i>Pas d'adresse définie</i>
                                                @else Adresse : <b>{{$e->address}}</b>
                                                @endif
                                            </p>
                                        @endif
                                        <p>
                                            @if($e->descr=='')<i>Pas de description</i>
                                            @else Description : {{$e->descr}}
                                            @endif
                                        </p>
                                    </li>
                                    @foreach($notifications as $notification)
                                        @if($notification->event_id == $e->id)
                                            <li class="list-group-item">
                                                <div style="display:inline-block;text-align:right;float:right;width: 40%" class="pull-right">
                                                    @if($notification->showoption)
                                                        @if($notification->showoption == 1)
                                                            @if(!in_array($notification->id,$declinedNotificationIds))
                                                                @if(!in_array($notification->id,$acceptedNotificationIds))
                                                                    <a class="btn btn-success btn-xs acceptEvent disabled" data-id="{{$notification->id}}">Accepter</a>
                                                                @endif
                                                                <a class="btn btn-danger btn-xs declineEvent disabled" data-id="{{$notification->id}}">Décliner</a>
                                                            @endif
                                                        @elseif($notification->showoption == 2)
                                                            @if((!in_array($notification->id,$changedNotificationIds)))
                                                                <span class="small text-info">
                                                                    <a class="btn btn-primary btn-xs duplicateEvent disabled" data-id="{{$notification->id}}" title="Dupliquer">Programmer un cours pour ce participant</a>
                                                                    <a class="btn btn-danger btn-xs notPossibleEvent disabled" data-id="{{$notification->id}}" title="Refuser">Refuser</a>
                                                                    <p id="infoSuitMe">Cette personne n'est plus inscrite.<br>
                                                                    En programmant un nouveau cours, vous créerez le même cours à la date proposée et ce participant sera inscrit d'office.</p>
                                                                </span>
                                                            @endif
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="notificationDateTime">{{ date('d/m/Y', strtotime($notification->created_at)) }}, {{ date('H:i', strtotime($notification->created_at)) }}</div>
                                                <div>
                                                    {{trim($notification->description)}}
                                                    @if($e->teacher_home==1 and $e->student_home==1)
                                                    <p>
                                                        @foreach($eventsUsers as $eu)
                                                            @if($e->id==$eu->event_id and $eu->user_id==$notification->creator_id)
                                                                → Mobilité sélectionnée : <b>{{ ($eu->mobility==1) ? 'Domicile professeur' : 'Domicile élève' }}</b>
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                    @endif
                                                </div>
                                                @if(count($notification->messages))
                                                    <ul class="list-group" style="margin-top: 15px;">
                                                        <b><i class="fa fa-comments-o"></i> Messages </b>
                                                        <span>
                                                            @foreach($notification->messages as $message)
                                                                <div style="margin-left: 10pt">{{($message->message_from == Auth()->user()->id) ? 'Vous avez écrit : ' : $message->sender->first_name.' '.$message->sender->last_name.' : ' }}{{$message->description}}</div>
                                                            @endforeach
                                                        </span>
                                                    </ul>
                                                @endif
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                            <td class="text-center" style="width: 5%">
                                @if(Auth::user()->id==$e->owner)
                                    <i class="fa fa-user" title="Propriétaire"></i>
                                @else
                                    <i class="fa fa-users" title="Participant"></i>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td style="font-style: italic;">
                                Pas de cours à afficher
                            </td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div id="myDeclinedEvents" role="tabpanel" class="tab-pane">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-list" id="myDeclinedEventsTable">
                        @if(count($declinedEvents)!=0)
                        @foreach($declinedEvents as $e)
                        <tr>
                            <td>
                                <div class="toggleNotification" style="cursor: pointer">
                                    @if($e->course_label == null) <b>{{$e->level_label}}</b>
                                    @else <b>{{$e->course_label}}</b> / {{$e->level_label}}
                                    @endif
                                    @if($e->private == '1') <span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>@endif
                                    @if($e->type==1) <span class="label label-default" style="font-size: 10px;background-color: #ff5a00;font-weight: normal;">Weweclass</span> @endif
                                    @foreach($notifications as $n)
                                        @if(in_array($e->id,$notificationEventIds) and $n->event_id==$e->id and $n->readit==0)
                                            <span class="pull-right iconToggle" data-id="{{$n->id}}" id="iconToggle{{$n->id}}" data-type="declined">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        @endif
                                    @endforeach
                                    <span style="padding-right: 10pt; float: right;font-size: 10pt;"><i class="fa fa-calendar-o" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($e->dhstart)) }} &emsp; <i class="fa fa-clock-o" aria-hidden="true"></i> {{ date('H', strtotime($e->dhstart)) }}h-{{ date('H', strtotime($e->dhend)) }}h</span>
                                </div>
                                <ul class="list-group" style="display: none;margin-top: 5px;box-shadow: 0px 0px 5px {{$e->color}}">
                                    <li class="list-group-item" style="line-height:1;padding: 2%;">
                                        <p>Le <b>{{ date('d/m/Y', strtotime($e->dhstart)) }}</b> de <b>{{ date('H', strtotime($e->dhstart)) }}h</b> à <b>{{ date('H', strtotime($e->dhend)) }}h</b>@if(Auth::user()->type==4), pour <u>{{$e->child_first_name}}</u>@endif</p>
                                        @if(Auth::user()->id!=$e->owner and $e->owner_type!=3)
                                        <p>Propriétaire : <span class="showInfoCard" data-toggle="modal" data-target="#ModalShowInfoCard" data-id="{{$e->owner}}"><u>{{$e->owner_name}}</u></span> à {{$e->owner_locality}}</p>
                                        @endif
                                        @if($e->type==0)
                                            <p>@if($e->student_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile élève
                                            </p>
                                            <p>@if($e->teacher_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile professeur
                                            </p>
                                        @else
                                            <p>
                                                @if($e->address=='')<i>Pas d'adresse définie</i>
                                                @else Adresse : <b>{{$e->address}}</b>
                                                @endif
                                            </p>
                                        @endif
                                        <p>
                                            @if($e->descr=='')<i>Pas de description</i>
                                            @else Description : {{$e->descr}}
                                            @endif
                                        </p>
                                    </li>
                                    @foreach($notifications as $notification)
                                        @if($notification->event_id == $e->id)
                                            <li class="list-group-item">
                                                <div style="display:inline-block;text-align:right;float:right;width: 40%" class="pull-right">
                                                    @if($notification->showoption)
                                                        @if($notification->showoption == 1)
                                                            @if(!in_array($notification->id,$declinedNotificationIds))
                                                                @if(!in_array($notification->id,$acceptedNotificationIds))
                                                                    <a class="btn btn-success btn-xs acceptEvent disabled" data-id="{{$notification->id}}">Accepter</a>
                                                                @endif
                                                                <a class="btn btn-danger btn-xs declineEvent disabled" data-id="{{$notification->id}}">Décliner</a>
                                                            @endif
                                                        @elseif($notification->showoption == 2)
                                                            @if((!in_array($notification->id,$changedNotificationIds)))
                                                                <span class="small text-info">
                                                                    <a class="btn btn-primary btn-xs duplicateEvent disabled" data-id="{{$notification->id}}" title="Dupliquer">Programmer un cours pour ce participant</a>
                                                                    <a class="btn btn-danger btn-xs notPossibleEvent disabled" data-id="{{$notification->id}}" title="Refuser">Refuser</a>
                                                                    <p id="infoSuitMe">Cette personne n'est plus inscrite.<br>
                                                                    En programmant un nouveau cours, vous créerez le même cours à la date proposée et ce participant sera inscrit d'office.</p>
                                                                </span>
                                                            @endif
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="notificationDateTime">{{ date('d/m/Y', strtotime($notification->created_at)) }}, {{ date('H:i', strtotime($notification->created_at)) }}</div>
                                                <div>
                                                    {{trim($notification->description)}}
                                                    @if($e->teacher_home==1 and $e->student_home==1)
                                                    <p>
                                                        @foreach($eventsUsers as $eu)
                                                            @if($e->id==$eu->event_id and $eu->user_id==$notification->creator_id)
                                                                → Mobilité sélectionnée : <b>{{ ($eu->mobility==1) ? 'Domicile professeur' : 'Domicile élève' }}</b>
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                    @endif
                                                </div>
                                                @if(count($notification->messages))
                                                    <ul class="list-group" style="margin-top: 15px;">
                                                        <b><i class="fa fa-comments-o"></i> Messages </b>
                                                        <span>
                                                            @foreach($notification->messages as $message)
                                                                <div style="margin-left: 10pt">{{($message->message_from == Auth()->user()->id) ? 'Vous avez écrit : ' : $message->sender->first_name.' '.$message->sender->last_name.' : ' }}{{$message->description}}</div>
                                                            @endforeach
                                                        </span>
                                                    </ul>
                                                @endif
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td style="font-style: italic;">
                                Pas de cours à afficher
                            </td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
            <div id="invalidEvents" role="tabpanel" class="tab-pane">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered table-list" id="invalidtEventsTable">
                        @if(count($invalidEvents)!=0)
                        @foreach($invalidEvents as $e)
                        <tr>
                            <td>
                                <div class="toggleNotification" style="cursor: pointer">
                                    @if($e->course_label == null) <b>{{$e->level_label}}</b>
                                    @else <b>{{$e->course_label}}</b> / {{$e->level_label}}
                                    @endif
                                    @if($e->private == '1') <span title="Cours particulier" style="color: orange"><i class="fa fa-lock" aria-hidden="true"></i></span>@endif
                                    @if($e->type==1) <span class="label label-default" style="font-size: 10px;background-color: #ff5a00;font-weight: normal;">Weweclass</span> @endif
                                    @foreach($notifications as $n)
                                        @if(in_array($e->id,$notificationEventIds) and $n->event_id==$e->id and $n->readit==0)
                                            <span class="pull-right iconToggle" data-id="{{$n->id}}" id="iconToggle{{$n->id}}" data-type="invalid">
                                                <i class="fa fa-plus"></i>
                                            </span>
                                        @endif
                                    @endforeach
                                    <span style="padding-right: 10pt; float: right;font-size: 10pt;"><i class="fa fa-calendar-o" aria-hidden="true"></i> {{ date('d/m/Y', strtotime($e->dhstart)) }} &emsp; <i class="fa fa-clock-o" aria-hidden="true"></i> {{ date('H', strtotime($e->dhstart)) }}h-{{ date('H', strtotime($e->dhend)) }}h</span>
                                </div>
                                <ul class="list-group" style="display: none;margin-top: 5px;box-shadow: 0px 0px 5px {{$e->color}}">
                                    <li class="list-group-item" style="line-height:1;padding: 2%;">
                                        <p>Le <b>{{ date('d/m/Y', strtotime($e->dhstart)) }}</b> de <b>{{ date('H', strtotime($e->dhstart)) }}h</b> à <b>{{ date('H', strtotime($e->dhend)) }}h</b>@if(Auth::user()->type==4), pour <u>{{$e->child_first_name}}</u>@endif</p>
                                        @if(Auth::user()->id!=$e->owner and $e->owner_type!=3)
                                        <p>Propriétaire : <span class="showInfoCard" data-toggle="modal" data-target="#ModalShowInfoCard" data-id="{{$e->owner}}"><u>{{$e->owner_name}}</u></span> à {{$e->owner_locality}}</p>
                                        @endif
                                        @if($e->type==0)
                                            <p>@if($e->student_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile élève
                                            </p>
                                            <p>@if($e->teacher_home==1) <i class="fa fa-check" aria-hidden="true"></i>
                                                @else <i class="fa fa-check" aria-hidden="true" style="color:lightgrey;"></i>
                                                @endif
                                                Domicile professeur
                                            </p>
                                        @else
                                            <p>
                                                @if($e->address=='')<i>Pas d'adresse définie</i>
                                                @else Adresse : <b>{{$e->address}}</b>
                                                @endif
                                            </p>
                                        @endif
                                        <p>
                                            @if($e->descr=='')<i>Pas de description</i>
                                            @else Description : {{$e->descr}}
                                            @endif
                                        </p>
                                    </li>
                                    @foreach($notifications as $notification)
                                        @if($notification->event_id == $e->id)
                                            <li class="list-group-item">
                                                <div style="display:inline-block;text-align:right;float:right;width: 40%" class="pull-right">
                                                    @if($notification->showoption)
                                                        @if($notification->showoption == 1)
                                                            @if(!in_array($notification->id,$declinedNotificationIds))
                                                                @if(!in_array($notification->id,$acceptedNotificationIds))
                                                                    <a class="btn btn-success btn-xs acceptEvent disabled" data-id="{{$notification->id}}">Accepter</a>
                                                                @endif
                                                                <a class="btn btn-danger btn-xs declineEvent disabled" data-id="{{$notification->id}}">Décliner</a>
                                                            @endif
                                                        @elseif($notification->showoption == 2)
                                                            @if((!in_array($notification->id,$changedNotificationIds)))
                                                                <span class="small text-info">
                                                                    <a class="btn btn-primary btn-xs duplicateEvent disabled" data-id="{{$notification->id}}" title="Dupliquer">Programmer un cours pour ce participant</a>
                                                                    <a class="btn btn-danger btn-xs notPossibleEvent disabled" data-id="{{$notification->id}}" title="Refuser">Refuser</a>
                                                                    <p id="infoSuitMe">Cette personne n'est plus inscrite.<br>
                                                                    En programmant un nouveau cours, vous créerez le même cours à la date proposée et ce participant sera inscrit d'office.</p>
                                                                </span>
                                                            @endif
                                                        @endif
                                                    @endif
                                                </div>
                                                <div class="notificationDateTime">{{ date('d/m/Y', strtotime($notification->created_at)) }}, {{ date('H:i', strtotime($notification->created_at)) }}</div>
                                                <div>
                                                    {{trim($notification->description)}}
                                                    @if($e->teacher_home==1 and $e->student_home==1)
                                                    <p>
                                                        @foreach($eventsUsers as $eu)
                                                            @if($e->id==$eu->event_id and $eu->user_id==$notification->creator_id)
                                                                → Mobilité sélectionnée : <b>{{ ($eu->mobility==1) ? 'Domicile professeur' : 'Domicile élève' }}</b>
                                                            @endif
                                                        @endforeach
                                                    </p>
                                                    @endif
                                                </div>
                                                @if(count($notification->messages))
                                                    <ul class="list-group" style="margin-top: 15px;">
                                                        <b><i class="fa fa-comments-o"></i> Messages </b>
                                                        <span>
                                                            @foreach($notification->messages as $message)
                                                                <div style="margin-left: 10pt">{{($message->message_from == Auth()->user()->id) ? 'Vous avez écrit : ' : $message->sender->first_name.' '.$message->sender->last_name.' : ' }}{{$message->description}}</div>
                                                            @endforeach
                                                        </span>
                                                    </ul>
                                                @endif
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </td>
                            <td class="text-center" style="width: 5%">
                                @if(Auth::user()->id==$e->owner)
                                    <i class="fa fa-user" title="Propriétaire"></i>
                                @else
                                    <i class="fa fa-users" title="Participant"></i>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td style="font-style: italic;">
                                Pas de cours à afficher
                            </td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
<script src="{{ asset('js/jquery.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#getWewevueLink').css('color','');
        $('#getCalendarLink').css('color','');
        $('#listNavbarLink').css('color','#ff5a00');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.toggleNotification').on('click',function(e){
            e.preventDefault();
            $(".toggleNotification").not($(this)).next('ul').hide(500);
            $(this).next('ul').slideToggle(500);
            var idNotification = $(this).children('.iconToggle').data('id');
            var tabType = $(this).children('.iconToggle').data('type');
            var cpt = 0;
            if(idNotification){
                $.ajax({
                    'url':'/readNotification/' + idNotification,
                    'type':'POST',
                    'success': function(res)
                    {
                        $('#iconToggle'+idNotification).fadeOut('fast');
                        switch(tabType){
                            case "upcomming" : 
                                cpt = $('a[aria-controls="myUpcommingEvents"]').children('span').html();
                                if(cpt-1 == 0) $('a[aria-controls="myUpcommingEvents"]').children('span').fadeOut('fast');
                                else $('a[aria-controls="myUpcommingEvents"]').children('span').html(cpt-1);
                                break;
                            case "past" :
                                cpt = $('a[aria-controls="myPastEvents"]').children('span').html();
                                if(cpt-1 == 0) $('a[aria-controls="myPastEvents"]').children('span').fadeOut('fast');
                                else $('a[aria-controls="myPastEvents"]').children('span').html(cpt-1);
                                break;
                            case 'declined' :
                                cpt = $('a[aria-controls="myDeclinedEvents"]').children('span').html();
                                if(cpt-1 == 0) $('a[aria-controls="myDeclinedEvents"]').children('span').fadeOut('fast');
                                else $('a[aria-controls="myDeclinedEvents"]').children('span').html(cpt-1);
                                break;
                            case "invalid" :
                                cpt = $('a[aria-controls="invalidEvents"]').children('span').html();
                                if(cpt-1 == 0) $('a[aria-controls="invalidEvents"]').children('span').fadeOut('fast');
                                else $('a[aria-controls="invalidEvents"]').children('span').html(cpt-1);
                                break;
                        }
                    }
                });
            }
            
        });
        $('.expandMessages').on('click', function(e){
            e.preventDefault();
            $(this).next().next('span').slideToggle(500);
        });
        $(".btnDeleteFromMyList").on('click',function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            swal({
                title: 'Êtes-vous sûr de vouloir supprimer ce cours ?',
                text: "Vous ne pourrez pas revenir en arrière !",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Supprimer',
                cancelButtonText: 'Annuler',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false
            },function(confirm) {
                if(confirm)
                {
                    $.ajax({
                        'url':'/deleteMyEvent/'+id,
                        'type':'POST',
                        'data':   {_method: 'DELETE' },
                        'success': function(res)
                        {
                            $this.closest('tr').fadeOut();
                            swal(
                                'Supprimé !',
                                'Le cours a été supprimé avec succès.',
                                'success'
                            )
                        }
                    });
                }

            });
        });
        $('.editMyEvent').on('click', function(e) {
            $('#ModalEdit #id').val($(this).attr('data-id'));
            $('#ModalEdit #owner').val($(this).attr('data-owner'));
            $('#ModalEdit #editDescr').val($(this).attr('data-descr'));
            $('#ModalEdit #editHours option[value="'+$(this).attr('data-hours')+'"]').prop('selected', true);
            $('#ModalEdit #editDatepicker').val(moment($(this).attr('data-start')).format('DD/MM/YYYY'));
            $('#ModalEdit #editTimepickerStart').val(moment($(this).attr('data-start')).format('HH'));
            $('#ModalEdit #editTimepickerEnd').val(moment($(this).attr('data-end')).format('HH'));
            $('#ModalEdit #editLevelSelect').val($(this).attr('data-level'));
            if($(this).attr('data-course') == '0') $('#ModalEdit #editDivSelectCourse').hide();
            else
            {
                $('#ModalEdit #editDivSelectCourse').show();
                $('#ModalEdit #editCourseSelect').val($(this).attr('data-course'));
            }
            if($(this).attr('data-student_home') == '1') $('#ModalEdit #editStudent_home').prop('checked', true);
            else $('#ModalEdit #editStudent_home').prop('checked', false);
            if($(this).attr('data-teacher_home') == '1') $('#ModalEdit #editTeacher_home').prop('checked', true);
            else $('#ModalEdit #editTeacher_home').prop('checked', false);
            if($(this).attr('data-private')=='1') $('#ModalEdit #editPrivate').prop('checked', true);
            else $('#ModalEdit #editPrivate').prop('checked', false);
            //$('#ModalEdit').modal('show');
        });
        $('.acceptEvent').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $.ajax({
                'url': 'acceptEvent/' + id,
                'method': 'POST',
                'success': function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                    else if(res.status == 'failed')
                    {
                        swal('Oops',res.message,'error');
                    }
                }
            });
        });
        $('.declineEvent').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            swal({
                title: "Motif",
                text: '<textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Tapez votre message ici" rows="5" style="resize:none"></textarea>',
                html: true,
                showCancelButton: true,
                closeOnConfirm: true,
                confirmButtonText:'Décliner',
                cancelButtonText:'Annuler'
            }, function(inputValue) {
                if(inputValue)
                {
                    var value = $('#text').val();
                    data = {'message': value};
                    $.ajax({
                        'url': 'declineEvent/' + id,
                        'method': 'POST',
                        'data':data,
                        'success': function (res) {
                            if (res.status == 'success') {
                                $this.parent('span').remove();
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });
        $('.doesNotSuit').on('click', function (e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $('#doesNotSuitModal').find('#suitid').val(id);
            $('#doesNotSuitModal').find('#suitDatepicker').val($this.attr('data-suitDatepicker'));
            $('#doesNotSuitModal').find('#suitTimepickerStart').val($this.attr('data-suitDhstart'));
            $('#doesNotSuitModal').find('#suitHours').val($this.attr('data-suitHours'));
            $('#doesNotSuitModal').find('#suitOwner').val($this.attr('data-suitOwner'));
            $('#doesNotSuitModal').find('#suitTimepickerEnd').val($this.attr('data-suitDhend'));
            if($this.attr('data-suitOwner') != {{Auth()->user()->id}}){
                if($('#doesNotSuitModal').find('#messageinfo').length==0) $('<div class="alert alert-danger" id="messageinfo"><strong>Attention !</strong> Vous ne serez plus inscrit(e) pour ce cours.</div>').insertBefore('#firstDiv');
                $('#doesNotSuitModal #divSuitPrivate').removeClass('hideDiv');
                $('#doesNotSuitModal #divSuitGetConfirmed').addClass('hideDiv');
            }
            else{
                $('#doesNotSuitModal').find('#messageinfo').remove();
                $('#doesNotSuitModal #divSuitPrivate').addClass('hideDiv');
                $('#doesNotSuitModal #divSuitGetConfirmed').removeClass('hideDiv');
            }
            $('#doesNotSuitModal').modal('show');
        });
        $('.unsubscribeEditEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $.ajax({
                'url':'/unsubscribeEvent/'+id,
                'type':'POST',
                'success': function(res)
                {
                    if(res.status == 'success')
                    {
                        window.location.reload();
                    }
                }
            });
        });
        $('.withdrawEditEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $.ajax({
                'url':'/withdrawEvent/'+id,
                'type':'POST',
                'success': function(res)
                {
                    if(res.status == 'success')
                    {
                        window.location.reload();
                    }
                }
            });
        });
        $('.possibleEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $.ajax({
                'url': '/possibleEvent/' + id,
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                    else if(res.status == 'failed')
                    {
                        swal('Oops',res.message,'error');
                    }
                }
            });
        });
        $('.notPossibleEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $.ajax({
                'url': '/notPossibleEvent/' + id,
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                    else if(res.status == 'failed')
                    {
                        swal('Oops',res.message,'error');
                    }
                }
            });
        });
        $('.duplicateEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $.ajax({
                'url': '/duplicateEvent/' + id,
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                    else if(res.status == 'failed')
                    {
                        swal('Oops',res.message,'error');
                    }
                }
            });
        });
        $('.confirmNewEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            var mobility = '';
            if($this.attr('data-student_home')==1 && $this.attr('data-teacher_home')==1)
            {
                mobility = '<div class="form-group col-sm-12">Définir le lieu <div class="btn-group" data-toggle="buttons"><label class="btn btn-default"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
            }
            swal({
                title: 'Demande de participation',
                text: mobility+'<textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
                html: true,
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true,
                confirmButtonText:'Participer',
                cancelButtonText:'Annuler'
            }, function(inputValue) {
                if(inputValue) {
                    var value = $('#text').val();
                    data = {'message': value};
                    $.ajax({
                        'url': '/confirmNewEvent/' + id,
                        'type': 'POST',
                        'data': data,
                        'success': function (res) {
                            $('#ModalEdit').modal('hide');
                            if (res.status == 'failed') {
                                swal('Oops', res.message, 'error');
                            }
                            else if (res.status == 'success') {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });
        $('.proposeNewEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            var mobility = '';
            if($this.attr('data-student_home')==1 && $this.attr('data-teacher_home')==1)
            {
                mobility = '<div class="form-group col-sm-12">Définir le lieu <div class="btn-group" data-toggle="buttons"><label class="btn btn-default"><input type="radio" name="mobility" value="0">Domicile élève</label><label class="btn btn-default"><input type="radio" name="mobility" value="1">Domicile professeur</label></div></div>';
            }
            swal({
                title: 'Proposer de donner le cours',
                text: mobility+'<textarea class="form-control" id="text" name="text" maxlength="350" placeholder="Taper votre message ici" rows="5" style="resize:none"></textarea><h6 class="pull-right" id="count_message">350 caractères restants</h6>',
                html: true,
                showCancelButton: true,
                closeOnConfirm: true,
                closeOnCancel: true,
                confirmButtonText:'Participer',
                cancelButtonText:'Annuler'
            }, function(inputValue) {
                if(inputValue) {
                    var value = $('#text').val();
                    data = {'message': value};
                    $.ajax({
                        'url': '/proposeNewEvent/' + id,
                        'type': 'POST',
                        'data': data,
                        'success': function (res) {
                            $('#ModalEdit').modal('hide');
                            if (res.status == 'failed') {
                                swal('Oops', res.message, 'error');
                            }
                            else if (res.status == 'success') {
                                window.location.reload();
                            }
                        }
                    });
                }
            });
        });
        $('.declineNewEvent').on('click', function(e) {
            e.preventDefault();
            $this = $(this);
            var id = $this.attr('data-id');
            $.ajax({
                'url': '/declineNewEvent/' + id,
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                    else if(res.status == 'failed')
                    {
                        swal('Oops',res.message,'error');
                    }
                }
            });
        });
        $('.reconfirmNewEvent').on('click', function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                'url': '/reconfirmNewEvent/' + id,
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                    else if(res.status == 'failed')
                    {
                        swal('Oops',res.message,'error');
                    }
                }
            });
        });
        $('.redeclineNewEvent').on('click', function(e){
            e.preventDefault();
            var id = $(this).attr('data-id');
            $.ajax({
                'url': '/redeclineNewEvent/' + id,
                'type': 'POST',
                'success': function (res) {
                    if (res.status == 'success') {
                        window.location.reload();
                    }
                    else if(res.status == 'failed')
                    {
                        swal('Oops',res.message,'error');
                    }
                }
            });
        });
    });
</script>